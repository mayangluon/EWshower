#ifndef WEAKLIB_H
#define WEAKLIB_H


#include<iostream> // needed for io
#include<fstream>  // file io
#include<sstream>  // needed for internal io
#include<vector> 
#include<cmath>


// ROOT classes
#include"TROOT.h"
#include"TLorentzVector.h"
#include"TVector3.h"
#include"TRandom3.h"



using namespace std;


/*----------------------------------------------------------------------------------

   weaklib.h    (Brock Tweedie)

   Package for weak shower.

-------------------------------------------------------------------------------------*/


/*



*** Consider consistently allowing ultracollinears to shower. Will be relevant, e.g., for downstream QED and QCD radiation. But need to make sure that the reweighting factors are actually doing something!


  WARNINGS:
  *  Currently using a V(T) -> V(T) V(T) damping function that goes as sqrt(sqrt(1-_th2/4)), to better match MadGraph simulations. Check to what extent this poses a problem for my analytic anzatz "total splitting rate" integral, by inspecting the surviving density matrix for W0(T) subject to W+(T) W-(T) splits, and comparing with the actual splitting rate.

  NOTES:
  *  Understand/fix bug wherein top quark recoiling against dummy, with cos(theta*) generated according to spin-1 resonance -> ff, causes some event (after a few 1000) to hang, even when no showering processes are turned on...related to Breit-Wigner??
  *  Implement running EW couplings, including running cosThetaW and running projection matrices into "photon" and "Z" states
  *  Fix bug wherein gamma->ff and gamma/Z->ff are computed with zLim's set according to massless fermions, but e.g. can later be thrown out-of-bounds for charm pairs
  *  Add full gamma/Z resonance region machinery. Also, add finite Z width to splitting functions? (Some special cases can have splitting zeros, which are smoothed-out by Gamma.)
  *  Figure out a way to interface back into Pythia shower for low-Q? Need to add basic color-connections.
  *  Try to come up with less aggressive over-estimator functions for the gauge splittings, as they tend to occassionally produce to much "fake" splitting probability (O(1) in a single step)...probably due to too-high rate near z boundaries
  *  Consider getting away from the virtuality/approximate-angle based calculation scheme, and define splitting function formulas strictly in virtuality/kT language, for simplicity and closer contact to our paper. Try to consistently enforce sane/suppressed high-angle behavior. (Naive extrapolations to large angle using collinear approximations can lead to spuriously large rates.)

*/



/////////////////////////////////////////////////////////////////////////////////
//
//                             GLOBAL PARAMETERS
//
/////////////////////////////////////////////////////////////////////////////////

namespace weaklib {
  
  // "true" constants
  const double Pi = 3.14159265359;
  const double halfPi = Pi/2.;
  const double twoPi = 2.*Pi;
  const double fourPi = 4.*Pi;
  const double PiSq = Pi*Pi;
  const double sqrt2 = sqrt(2.);
  const double mW = 80.4;
  const double GammaW = 2.08;
  const double mZ = 91.2;
  const double GammaZ = 2.50;
  const double mh = 125.;
  const double Gammah = 0.004;
  const double mtop = 173.;
  const double Gammatop = 1.4;
  const double mbottom = 4.5;
  const double mcharm = 1.3;
  const double mkaon = 0.5;
  const double mpi = 0.14;
  const double mtau = 1.77;
  const double mmu = 0.105;
  const double me = 0.000511;
  const double mMassless = 0.2;  // regulator mass for all "massless" particles
  
  // shower numerical controls
  const double weakCouplingRescaling = 1;  // for testing: rescale dimensionless weak couplings^2 and VEV^2, keeping physical masses fixed (BE CAREFUL WITH THIS!!! And note that lambdaHiggs scales differently.)
  const double alphas = 0.118;   // note all input couplings should be defined at mZ (they will be run to different scales when relevant)
  const double alphaEM = weakCouplingRescaling * 1/132.5;   //  MG default value is 1/132.5,  MS-bar @ mZ is more like 1/129.
  const double alphaW = weakCouplingRescaling * (0.6533)*(0.6533)/fourPi;   // MG default value is 0.6533,  I have usually been using 0.650 such that with alphaEM=1/129, get sin2thetaW = 0.231, which is also MS-bar @ mZ
  const double gW = sqrt(fourPi*alphaW);
  const double sin2ThetaW = alphaEM/alphaW;
  const double cos2ThetaW = 1-sin2ThetaW;
  const double sinThetaW = sqrt(sin2ThetaW);
  const double cosThetaW = sqrt(cos2ThetaW);
  const double gY = gW * sinThetaW/cosThetaW;
  const double gZ = gW / cosThetaW;
  const double alphaZ = gZ*gZ/fourPi;
  const double vev = 2*mW/gW;   // 246 GeV in the SM  (note that if we rescale the weak couplings, will also rescale the VEV given fixed mW/Z, which in turn will affect Yukawas)
  const double ytop = sqrt2*mtop/vev;  // top Yukawa (~1 in SM)
  const double ybottom = sqrt2*mbottom/vev;  // bottom Yukawa
  const double lambdaHiggs = 2*mh*mh/vev/vev;  // Higgs quartic, normalized for V(H) ~ (lambdaHiggs/4)*|H|^4  (~0.5 in the SM)
  const double Qmin = 50.;  // termination point for the shower / handoff to PYTHIA6, etc
  const double dt = 2*0.05;   // maximum step in t:  dt = 2*dQ/Q
  const double dtMin = 2*0.001;  // minimum step in t (to prevent unboundedly-shrinking steps towards very narrow resonance (i.e. Higgs))
  const double CF = 4./3;
  const int    NC = 3;
  const double NF = 4;     // # of *light* quark flavors (excluding top and bottom, which are treated separately)
  const double TR = NF/2.;
  const double NFlep = 3;   // # of charged lepton flavors
  const double NFnu = NFlep;
  const double QW2 = 1/2.; // squared weak charge of a qqW vertex
  const int    Ndoublets = NC*NF/2 + NFlep;  // (3-colors)*(uL,dL) + (3-colors)*(cL,sL) + (3-generations)*(vL,eL)
  const double nGammaCutoff = 10; // # of Gamma's above a resonance (W/Z/h/t) at which to shut off the shower and consider the resonance "on-shell"
  
  // shower basic switches (on/off)
  const bool   applyProductionCorrectionsResonance = 1;  // apply reweightings for setting resonance a on-shell when a's mass would affect its own production rate (esp. important for photon/Z)
  const bool   applyProductionCorrectionsSplitting = 1;  // apply reweightings for a->bc when a's virtuality would affect its own production rate (esp. important for massive bosons)
  const bool   enforceAngularOrdering              = 0;  // enforce angular ordering via veto (probably over-aggressive)
  const bool   coherentPhotonZT   = 1;  // evolve transverse photons and Z bosons coherently **SWITCH NOT CURRENTLY USED  <-- will need to collapse density matrix orientation at production as well as turn off Sudakov rotation...the separate switch neutralBosonSudakovEvolution may then become redundant?
  const bool   coherentHiggsZlong = 1;  // evolve Higgs and longitudindal Z bosons coherently  **SWITCH NOT CURRENTLY USED
  const bool   neutralBosonSudakovEvolution =    1;         // evolve the 2x2 density matrix for neutral bosons at each step (turn off, e.g., for studies of "pure photon" showers)
  const bool   do_ultracollinear  = 1;  // include ultracollinear splittings
  
  
  // individual splitting function switches (on/off)
  //
  // QCD
  bool q_to_g_q              =    1;
  bool g_to_g_g              =  0;
  bool g_to_q_q              =  0;
  bool g_to_t_t              =  0;
  bool g_to_b_b              =  0;
  bool g_to_tRL_tRL          =  0;
  bool g_to_bRL_bRL          =  0;
  //
  // light fermion splittings
  bool f_to_W_f              =    1;
  bool f_to_gamZT_f          =    1;
  bool f_to_Zlong_f          =    1;
  //
  // top quark splittings
  bool tL_to_WT_bL           =  0;
  bool tL_to_Wlong_bL        =  0;
  bool tR_to_WT_bL           =  0;
  bool tRL_to_Wlong_bLR      =  0;
  bool tRL_to_gamZT_tRL      =  0;
  //bool tRL_to_gamZT_tLR    =  0;     // later...(broken spin-flip, needed for dead cone?)
  bool tRL_to_Zlong_tRL      =  0;
  bool tRL_to_h_tRL          =  0;
  bool tRL_to_H0_tLR         =  0;
  bool t_to_g_t              =  0;
  //bool tRL_to_g_tLR        =  0;     // later...("broken" spin-flip, needed for dead cone)
  //
  // bottom quark splittings
  bool bL_to_WT_tL           =  0;
  bool bL_to_Wlong_tL        =  0;
  bool bL_to_WT_tR           =  0;
  bool bLR_to_Wlong_tRL      =  0;
  bool bRL_to_gamZT_bRL      =  0;
  //bool bRL_to_gamZT_bLR    =  0;     // later...(broken spin-flip, needed for dead cone)
  bool bRL_to_Zlong_bRL      =  0;
  bool b_to_g_b              =  0;
  //bool bRL_to_g_bLR        =  0;     // later...("broken" spin-flip, needed for dead cone)
  //
  // transverse gauge boson splittings
  bool WT_to_gamZT_WT        =  0;
  bool WT_to_Wlong_gamT      =  0;
  bool WT_to_Wlong_ZT        =  0;
  bool WT_to_Zlong_WT        =  0;
  bool WT_to_h_WT            =  0;
  bool WT_to_f_f             =  0;
  bool WT_to_t_b             =  0;
  //bool WT_to_tLR_bLR       =  0;   // later...(broken spin-flip, needed for dead cone)
  bool WT_to_H0_Wlong        =  0;
  //
  bool gamZT_to_WT_WT        =  0;
  bool gamZT_to_Wlong_WT     =  0;
  bool gamZT_to_f_f          =  0;
  bool gamZT_to_t_t          =  0;
  //bool gamZT_to_tLR_tLR    =  0;     // later...(broken spin-flip, needed for dead cone)
  bool gamZT_to_b_b          =  0;
  bool gamZT_to_Wlong_Wlong  =  0;
  bool gamZT_to_h_Zlong      =  0;
  //bool gamZT_to_h_ZT       =  0;   // later...(broken gauge vertex)
  //
  bool gamma_to_f_f          =  0;
  bool gamma_to_b_b          =  0;    // not bothering to include here helicity-flip ultracollinears in QED
  //
  // longitudinal/scalar gauge boson splittings
  //
  bool Wlong_to_WT_H0        =  0;
  bool Wlong_to_gamZT_Wlong  =  0;
  bool Wlong_to_tR_bR        =  0;
  bool Wlong_to_Zlong_Wlong  =  0;
  bool Wlong_to_h_Wlong      =  0;
  bool Wlong_to_gamZT_WT     =  0;
  bool Wlong_to_tL_bR        =  0;
  //bool Wlong_to_f_f        =  0;    // later...light fermions, probably only relevant to extend W Breit-Wigner tail
  bool H0_to_WT_Wlong        =  0;
  bool H0_to_gamZT_H0        =  0;
  bool H0_to_tR_tR           =  0;
  bool H0_to_Wlong_Wlong     =  0;
  bool H0_to_Zlong_Zlong     =  0;
  bool H0_to_h_Zlong         =  0;
  bool H0_to_h_h             =  0;
  bool H0_to_WT_WT           =  0;
  // bool H0_to_ZT_ZT        =  0;    // later...very tiny (see H0_to_WT_WT and Wlong_to_gamZT_WT)
  // bool H0_to_tL_tR        =  0;    // later...very tiny (see Wlong_to_tL_bR)
  // bool H0_to_b_b          =  0;    // later...probably only relevant to extend Z Breit-Wigner tail
  // bool_H0_to_f_f          =  0;    /  later...light fermions, probably only relevant to extend Z Breit-Wigner tail
  //
  // test switches for depricated processes...use with caution
  bool f_to_ZT_f             =  0;  // superceded by mixed gamma-ZT emission
  bool tRL_to_ZT_tRL         =  0;  // ditto
  bool WT_to_ZT_WT           =  0;  // ditto


  // decay switches (on/off)
  bool do_top_decay   = 0;
  bool do_W_decay     = 0;
  bool do_Z_decay     = 0;
  bool do_h_decay     = 0;  // ** Perhaps best to leave this to Pythia...
  bool do_gamma_decay = 0;  // *** WE WILL FILL OUT GAMMA SPLITTING SPACE BELOW QMIN BY "DECAYING" IT (MOST OF THE TIME BACK INTO AN UNSPLIT Q=0 PHOTON)


  // ROOT random number generator
  TRandom3 randGen(12345);
  int randPlusMinus1() { return 2*randGen.Integer(2)-1;}
  
  // set of standardized density matrices for "pure" mass eigenstates (also used as projection matrices)
  // (rho[0]==rho11, rho[1]==rho22, rho[2]==Re[rho12], rho[3]==Im[rho12])
  double rhoPhoton_arr[4] = {cos2ThetaW,sin2ThetaW, cosThetaW*sinThetaW,0};  vector<double> rhoPhoton(rhoPhoton_arr, rhoPhoton_arr + sizeof(rhoPhoton_arr) / sizeof(rhoPhoton_arr[0]) );
  double rhoZT_arr[4]     = {sin2ThetaW,cos2ThetaW,-cosThetaW*sinThetaW,0};  vector<double>     rhoZT(    rhoZT_arr,     rhoZT_arr + sizeof(    rhoZT_arr) / sizeof(    rhoZT_arr[0]) );
  double rhoHiggs_arr[4] = {1,0,0,0};  vector<double> rhoHiggs(rhoHiggs_arr, rhoHiggs_arr + sizeof(rhoHiggs_arr) / sizeof(rhoHiggs_arr[0]) );
  double rhoZlong_arr[4] = {0,1,0,0};  vector<double> rhoZlong(rhoZlong_arr, rhoZlong_arr + sizeof(rhoZlong_arr) / sizeof(rhoZlong_arr[0]) );
  double rhoH0_arr[4] = {0.5,0.5,0,-0.5};  vector<double> rhoH0(rhoH0_arr, rhoH0_arr + sizeof(rhoH0_arr) / sizeof(rhoH0_arr[0]) );
  double rhoH0star_arr[4] = {0.5,0.5,0,0.5};  vector<double> rhoH0star(rhoH0star_arr, rhoH0star_arr + sizeof(rhoH0star_arr) / sizeof(rhoH0star_arr[0]) );
  
  // current max color/anticolor line index, which is iterated forward when new color lines are produced (*very* primitive...currently only for quarks, and only in a few processes)
  int current_max_color = 500;

  // debug
  int eventCounter = 0;
  int bugCounter = 0;
  int bugCounter1 = 0;
  int bugCounter2 = 0;
  int bugCounter3 = 0;
  int bugCounter4 = 0;
  const int bugEvent = 9309;
  
  

  ////////////////////////////////////////////////////////////////////////////////
  //
  //                      BASIC OBJECTS AND FUNCTIONS
  //
  ////////////////////////////////////////////////////////////////////////////////
  
  class Particle {
  public:
  Particle() : canShower(true), branched(false), mother(NULL), sister(NULL), aunt(NULL), color(0), anticolor(0), f__a_to_b_c(NULL), a_to_V_c(false), a_to_V_V(false), is_b(false), is_c(false) { 
      densityMatrix.resize(4);
    }
    int id;
    TLorentzVector v;
    int helicity;
    double t;  // evolution variable, log(Q^2/Qmin^2)
    bool canShower;
    bool branched;   // true => has split into daughters
    double cosThetaStar;  // rest-frame "decay angle" of splitting (needed for Jacobian calculations)
    vector<double> densityMatrix;  // neutral boson mixed wavefunction state in hypercharge/T3 gauge space or h^0/phi^0 space; elements are rho11,rho22,Re(rho12),Im(rho12)
    int color, anticolor;
    Particle * mother;
    Particle * sister;
    Particle * unrecoiledSister;  // copy of recoiler before anybody gets a chance to split (be careful: might point to non-persistent object)
    Particle * aunt;
    vector<Particle> daughters;  // from shower or decay. a long shower/decay chain is represented here like a sequence of nested dolls. particles with no daughters are "final state".
    double (*f__a_to_b_c)(double);  // pointer to the splitting function used to split this particle into its daughters (information used in shower reweighting)
    bool a_to_V_c, a_to_V_V;  // whether the used splitting function requires throwing vector polarizations
    bool is_b, is_c;  // whether this particle was the "1st" or "2nd" if it was itself produced from a splitting (information used in shower reweighting)
    int signid() { return abs(id)/id; }
    int absid() { return abs(id); }
    int chirality() { return helicity*signid(); }
    double GetQ() { return Qmin*exp(t/2); }
    double E() { return v.E(); }
    double P() { return v.P(); }
    double Pt() { return v.Pt(); }
    double M() { return v.M(); }
    TVector3 Vect() { return v.Vect(); }
    
    Particle BasicCopy()  {    // make a copy of basic particle info, without all the attendant (and potentially enormously nested) daughter/shower structure (however, keep some important pointers)
      Particle copy;
      copy.id = id;  copy.v = v;  copy.helicity = helicity;  copy.t = t;  copy.densityMatrix = densityMatrix;
      copy.mother = mother;  copy.sister = sister;  copy.unrecoiledSister = unrecoiledSister;  copy.aunt = aunt;
      copy.is_b = is_b;  copy.is_c = is_c;
      copy.color = color;  copy.anticolor = anticolor;
      return copy;
    };
    
    void NewBasicCopy(Particle * original)  {    // make a copy of basic particle info, without all the attendant (and potentially enormously nested) daughter/shower structure (however, keep some important pointers)
      id = original->id;  v = original->v;  helicity = original->helicity;  t = original->t;
      densityMatrix[0] = original->densityMatrix[0];  // somehow faster to set component-by-component instead of the full vector
      densityMatrix[1] = original->densityMatrix[1];
      densityMatrix[2] = original->densityMatrix[2];
      densityMatrix[3] = original->densityMatrix[3];
      mother = original->mother;  sister = original->sister;  unrecoiledSister = original->unrecoiledSister;  aunt = original->aunt;
      is_b = original->is_b;  is_c = original->is_c;
      color = original->color;  anticolor = original->anticolor;
    };
  };
  
  
  bool EvolveParticle(Particle & a, Particle aOriginal, Particle rOriginal, double & z, Particle & b, Particle & c, int & goodEvolve);
  
  bool EvolvePair(Particle & p1, Particle & p2, bool goodPair = true);
  
  bool Split(Particle & a, double mb, double mc, double z, TLorentzVector & b, TLorentzVector & c);
  
  void ResetMasses(Particle & p1, Particle & p2, double m1, double m2);
  
  void BoostDaughters(TLorentzVector va, TLorentzVector vr, Particle & pb, Particle & pc);
  
  void BoostGrandDaughters(const TVector3 & boostVector, Particle & daughter)  // recursively boost all particles in a shower/decay chain
  {
    for (unsigned int d = 0; d < daughter.daughters.size(); d++) {
      daughter.daughters[d].v.Boost(boostVector);
      if (daughter.daughters[d].daughters.size()>0) BoostGrandDaughters(boostVector,daughter.daughters[d]);
    }
  }
  
  pair<double,double> zMinMax(const TLorentzVector &, double, double);
  
  double ComputePtot(double z, double Ea, double mb, double mc);
  
  double ComputeTheta(double z, double Q, double Ptot, double Eb, double Ec, double mb, double mc);
  
  void InitializeMotherVariables(Particle & a, Particle & r);
  
  double ComputeProductionCorrection();
  
  double Jacobian(const Particle & mother, double z, double zPrime, double mOnShell, double mOffShell, double mSister, bool cIsTheOffShell);
  
  
  // unspool the shower/decay chains and gather up final-state particles (those without any daughters)
  void GatherFinalParticles(vector<Particle*> & hard_particles, vector<Particle*> & final_particles)
  {
    for (unsigned int p = 0; p < hard_particles.size(); p++) {
      if (hard_particles[p]->daughters.size()==0) {
	final_particles.push_back(hard_particles[p]);
      }
      else {
	vector<Particle*> daughter_pointers;  // build a list of pointers to the daughters, and recurse
	for (unsigned int d = 0; d < hard_particles[p]->daughters.size(); d++) {
	  daughter_pointers.push_back( &(hard_particles[p]->daughters[d]) );
	}
	GatherFinalParticles(daughter_pointers,final_particles);
      }
    }
  }
  
  // in-house definitions of x*x and x*x*x functions
  double square(double x) { return pow(x,2.0); }
  double   cube(double x) { return pow(x,3.0); }
  
  
  // "kinetic mass"
  double m(int id)
  {
    id = abs(id);
    if (id==  15) return mtau;
    if (id==   5) return mbottom;
    if (id==   6) return mtop;
    if (id==  22) return mMassless;
    if (id==  23) return mZ;
    if (id==  24) return mW;
    if (id==  25) return mh;
    if (id==2223) return min(m(22),m(23));
    if (id==2325) return min(m(23),m(25));
    return mMassless;  // default assumption is "0" mass
  }
  double m2(int id) { return m(id)*m(id); }
  
  // "resonance mass", used to determine boundary between resonance and shower regions (distinction with kinetic mass is only relevant for mixed states)
  double mRes(int id)
  {
    if (id==2223) return max(m(22),m(23));
    if (id==2325) return max(m(23),m(25));
    return m(id);
  }
  
  // resonance natural width
  double Gam(int id)
  {
    id = abs(id);
    if (id==6)  return Gammatop;
    if (id==23) return GammaZ;
    if (id==24) return GammaW;
    if (id==25) return Gammah;
    if (id==2223) return Gam(23);
    return 0.;
  }
  
  // minimum mass boundary for a resonance
  double mMin(int id) {
    id = abs(id);
    if (id==6)  return mW+mbottom+10*GammaW;
    if (id==23) return mZ - 10*GammaZ;
    if (id==24) return 0;
    if (id==25) return 0;
    return 0.;
  }
  
  // maximum mass boundary for a resonance
  double mMax(int id) {
    id = abs(id);
    if (id==6)  return mtop + nGammaCutoff*Gammatop;
    if (id==23) return mZ + nGammaCutoff*GammaZ;  // also used as threshold to match onto QED
    if (id==24) return mW + nGammaCutoff*GammaW;
    if (id==25) return mh + nGammaCutoff*Gammah;
    if (id==2223)  return max(mMax(22),mMax(23));
    if (id==2325)  return max(mMax(23),mMax(25));  
    return 0.;
  }
  
  // a weight for Breit-Wigner distributed events, generalizing Gamma -> Gamma(Q), to improve decay/shower matching
  double BWweight(int id, double Q) {
    if (abs(id)==24 || id==23)  return Q/m(id);
    if (abs(id)==6)  return  cube(Q/mtop) * square(1-square(mW/Q))/square(1-square(mW/mtop)) * (1+2*square(mW/Q))/(1+2*square(mW/mtop));
    return 1;
  }


  // T3-charge of a particle
  double T3(int id, int helicity) {
    int absid = abs(id);
    int chirality = (id/absid)*helicity;
    if (chirality<0 && absid <= 16) {
      if (absid==2 || absid==4 || absid==6 || absid==12 || absid==14 || absid==16)  return  1./2;
      if (absid==1 || absid==3 || absid==5 || absid==11 || absid==13 || absid==15)  return -1./2;
    }
    if (absid==24 && helicity==0)  return 1./2;   // Higgs doublet component
    if (id==2325 || id==25 || (id==23 && helicity==0))  return -1./2;
    return 0.;
  }
  double T3(Particle * p) {
    return T3(p->id,p->helicity);
  }
  
  // hypercharge of a particle
  double Y(int id, int helicity) {
    int absid = abs(id);
    int chirality = (id/absid)*helicity;
    if (chirality<0 && absid<=16) {
      if (absid== 2 || absid== 4 || absid== 6)  return  1./6;
      if (absid== 1 || absid== 3 || absid== 5)  return  1./6;
      if (absid==12 || absid==14 || absid==16)  return -1./2;
      if (absid==11 || absid==13 || absid==15)  return -1./2;
    }
    if (chirality>0 && absid<=16) {
      if (absid== 2 || absid== 4 || absid== 6)  return  2./3;
      if (absid== 1 || absid== 3 || absid== 5)  return -1./3;
      if (absid==12 || absid==14 || absid==16)  return  0;  // "RH neutrinos"
      if (absid==11 || absid==13 || absid==15)  return -1;
    }
    if (absid==24 && helicity==0)  return 1./2;
    if (id==2325 || id==25 || (id==23 && helicity==0))  return 1./2;
    return 0.;
  }
  double Y(Particle * p) {
    return Y(p->id,p->helicity);
  }
  
  // electric charge of a particle
  double QEM(int id, int helicity) {
    return T3(id,helicity)+Y(id,helicity);
  }
  double QEM(Particle * p)
  {
    return QEM(p->id,p->helicity);
  }
  
  // weak charge of a ffZ vertex (multiplies gW/cosThetaW)
  double QZ(int id, int helicity)
  {
    return (T3(id,helicity) - QEM(id,helicity)*sin2ThetaW);
  }
  double QZ(Particle * p)
  {
    return QZ(p->id,p->helicity);
  }
  
  // QED beta-function coefficient as a function of scale
  double betaSumEM(double scale) {
    double betaSum = 0;
    if (scale > me)  betaSum += 1;
    if (scale > mmu)  betaSum += 1;
    if (scale > mtau)  betaSum += 1;
    if (scale > mpi)  betaSum += 3*(2./3)*(2./3) + 3*(-1./3)*(-1./3);  // u/d quarks
    if (scale > mkaon)  betaSum += 3*(-1./3)*(-1./3);  // strange quark
    if (scale > mcharm)  betaSum += 3*(2./3)*(2./3);
    if (scale > mbottom)  betaSum += 3*(-1./3)*(-1./3);
    if (scale > mtop)  betaSum += 3*(2./3)*(2./3);
    return betaSum;
  }

  // density matrix helper functions
  double Trace(const vector<double> & rho) { return (rho[0]+rho[1]); }  // 2x2 density matrix trace (matrix is reprented as an array of four real elements)
  double MultiplyTrace(const vector<double> & rho1, const vector<double> & rho2) { return (rho1[0]*rho2[0] + rho1[1]*rho2[1] + 2*rho1[2]*rho2[2] + 2*rho1[3]*rho2[3]); }   // tr[A*B] of two density matrices
  void AddTo(vector<double> & rho1, const vector<double> & rho2, const double & multiplier = 1) { for (int i=0; i<4; i++) rho1[i]+=multiplier*rho2[i]; }
  void EvolveRho(vector<double> & rho, const vector<double> & Sud) {  // evolve density matrix rho using Sudakov matrix Sud ("sqrt" of normal Sudakov)
    double rho0=rho[0];  double rho1=rho[1];  double rho2=rho[2];  double rho3=rho[3];
    double S0=Sud[0];  double S1=Sud[1];  double S2=Sud[2];  double S3=Sud[3];
    rho[0] = S0*S0*rho0 + (S2*S2+S3*S3)*rho1 + 2*S0*(S2*rho2+S3*rho3);
    rho[1] = S1*S1*rho1 + (S2*S2+S3*S3)*rho0 + 2*S1*(S2*rho2+S3*rho3);
    rho[2] = S0*S2*rho0 + S1*S2*rho1 + (S0*S1+S2*S2-S3*S3)*rho2 + 2*S2*S3*rho3;
    rho[3] = S0*S3*rho0 + S1*S3*rho1 + 2*S2*S3*rho2 + (S0*S1-S2*S2+S3*S3)*rho3;
  }
  void Print(const vector<double> & rho) { cout << "rho = (" << rho[0] << "," << rho[1] << "," << rho[2] << "," << rho[3] << "i)" << endl; }
  void PrintNormalized(const vector<double> & rho) { double tr = Trace(rho);  cout << "rho = (" << rho[0]/tr << "," << rho[1]/tr << "," << rho[2]/tr << "," << rho[3]/tr << "i)" << endl; }


  // effective coupling of a final-state transverse pair of W+W- to a mixed photon/Z state (only nontrivial part here is the Z propagator with nonzero mass)
  void Get_gamZT_to_WT_WT_dPijPrefactor(double Q2, vector<double> & dPij) {
    double DgamZT_BB = cos2ThetaW/Q2 + sin2ThetaW/(Q2-mZ*mZ);  // neutral vector boson propagator matrix
    double DgamZT_WW = sin2ThetaW/Q2 + cos2ThetaW/(Q2-mZ*mZ);   // D_WW
    double DgamZT_BW = -cosThetaW*sinThetaW*mZ*mZ / (Q2*(Q2-mZ*mZ));   // Re[D_BW]
    double g_B =        0;  // coupling to B0
    double g_W = gW/twoPi;  // coupling to W0 (normalized to give alpha_W/pi upon squaring...other factors of 2 sould be absorbed into the ME2 definition below)
    double Dg_B = DgamZT_BB*g_B + DgamZT_BW*g_W;   // convolve with propagator matrix
    double Dg_W = DgamZT_BW*g_B + DgamZT_WW*g_W;
    double naivePropDenom2 =  square(Q2 - square(m(2223)));
    dPij[0] = naivePropDenom2 * Dg_B*Dg_B;
    dPij[1] = naivePropDenom2 * Dg_W*Dg_W;
    dPij[2] = naivePropDenom2 * Dg_B*Dg_W;
    dPij[3] = 0;  // no imaginary part
  }

  // effective coupling of a final-state fermion to a mixed photon/Z state (generalizes, square(gY*Y)/16pi^2 and square(gW*T3)/16pi^2 to case with mixed vector states with non-equal masses)
  void Get_gamZT_to_f_f_dPijPrefactor(int id, int helicity, double Q2, vector<double> & dPij, int multiplicity = 1) {
    double DgamZT_BB = cos2ThetaW/Q2 + sin2ThetaW/(Q2-mZ*mZ);  // neutral vector boson propagator matrix
    double DgamZT_WW = sin2ThetaW/Q2 + cos2ThetaW/(Q2-mZ*mZ);   // D_WW
    double DgamZT_BW = -cosThetaW*sinThetaW*mZ*mZ / (Q2*(Q2-mZ*mZ));   // Re[D_BW]
    double g_B = gY* Y(id,helicity)/fourPi;  // coupling to B0
    double g_W = gW*T3(id,helicity)/fourPi;  // coupling to W0
    double Dg_B = DgamZT_BB*g_B + DgamZT_BW*g_W;   // convolve with propagator matrix
    double Dg_W = DgamZT_BW*g_B + DgamZT_WW*g_W;
    double naivePropDenom2 =  square(Q2 - square(m(2223)));
    dPij[0] = multiplicity * naivePropDenom2 * Dg_B*Dg_B;
    dPij[1] = multiplicity * naivePropDenom2 * Dg_W*Dg_W;
    dPij[2] = multiplicity * naivePropDenom2 * Dg_B*Dg_W;
    dPij[3] = 0;  // no imaginary part
  }

  // effective coupling of a final-state Wlong (~phi+/-) pair to a mixed photon/Z state (generalizes, square(gY*Y)/8pi^2 and square(gW*T3)/8pi^2 to case with mixed vector states with non-equal masses)
  void Get_gamZT_to_Wlong_Wlong_dPijPrefactor(double Q2, vector<double> & dPij) {
    double DgamZT_BB = cos2ThetaW/Q2 + sin2ThetaW/(Q2-mZ*mZ);  // neutral vector boson propagator matrix
    double DgamZT_WW = sin2ThetaW/Q2 + cos2ThetaW/(Q2-mZ*mZ);   // D_WW
    double DgamZT_BW = -cosThetaW*sinThetaW*mZ*mZ / (Q2*(Q2-mZ*mZ));   // Re[D_BW]
    double g_B = sqrt(2.)*gY*(1./2)/fourPi;  // coupling to B0
    double g_W = sqrt(2.)*gW*(1./2)/fourPi;  // coupling to W0
    double Dg_B = DgamZT_BB*g_B + DgamZT_BW*g_W;   // convolve with propagator matrix
    double Dg_W = DgamZT_BW*g_B + DgamZT_WW*g_W;
    double naivePropDenom2 =  square(Q2 - square(m(2223)));
    dPij[0] = naivePropDenom2 * Dg_B*Dg_B;
    dPij[1] = naivePropDenom2 * Dg_W*Dg_W;
    dPij[2] = naivePropDenom2 * Dg_B*Dg_W;
    dPij[3] = 0;  // no imaginary part
  }

  // effective coupling of a final-state hZlong (CP even-odd scalar) pair to a mixed photon/Z state (generalizes, square(gY*Y)/8pi^2 and square(gW*T3)/8pi^2 to case with mixed vector states with non-equal masses)
  void Get_gamZT_to_h_Zlong_dPijPrefactor(double Q2, vector<double> & dPij) {
    double DgamZT_BB = cos2ThetaW/Q2 + sin2ThetaW/(Q2-mZ*mZ);  // neutral vector boson propagator matrix
    double DgamZT_WW = sin2ThetaW/Q2 + cos2ThetaW/(Q2-mZ*mZ);   // D_WW
    double DgamZT_BW = -cosThetaW*sinThetaW*mZ*mZ / (Q2*(Q2-mZ*mZ));   // Re[D_BW]
    double g_B = sqrt(2.)*gY*( 1./2)/fourPi;  // coupling to B0
    double g_W = sqrt(2.)*gW*(-1./2)/fourPi;  // coupling to W0
    double Dg_B = DgamZT_BB*g_B + DgamZT_BW*g_W;   // convolve with propagator matrix
    double Dg_W = DgamZT_BW*g_B + DgamZT_WW*g_W;
    double naivePropDenom2 =  square(Q2 - square(m(2223)));
    dPij[0] = naivePropDenom2 * Dg_B*Dg_B;
    dPij[1] = naivePropDenom2 * Dg_W*Dg_W;
    dPij[2] = naivePropDenom2 * Dg_B*Dg_W;
    dPij[3] = 0;  // no imaginary part
  }



  // Approximate integral of photon/Z -> WW core splitting function, to be used in computing (W0 part of) mixed wavefunction evolution, integrated using Mathematica.
  // In massless limit, this naively reduces to the integral of (1-z*zBar)^2/(z*zBar), which is of course very highly spiked at 0 and 1.
  // The actual analytic formula integrated here includes damping terms at small/large z, where they would take us far outside the collinear
  // region: (1-z*zBar)^2/(z*zBar) * (z-z1)/z * (z2-z)/(1-z). This is meant to model the effect of the angular damping term in the splitting matrix
  // element below: (1-_th2/4) in effective small angle approximation (where kinematic theta=pi gets mapped to "splitting function theta" of 2), or
  // otherwise (and less accurately) (1+cos(_th))/2. This damping becomes relevant when Q > sqrt(2*mW*E), such that the parent is "slow" compared to its daughters,
  // and backwards emission is even possible. For Q < sqrt(2*mW*E), the angular damping terms become irrelevant, but the above splitting function anzatz
  // again works very well. Disagreements occur only within an O(1) region of the critical Q ~ sqrt(2*mW*E), where mother and daughters have
  // the same velocity, and z (defined in terms of momentum fraction) can formally go to 0/1. The anzatz does not turn off fast enough near the boundaries here
  // to model the actual splitting behavior. However, a good match is still obtained by capping the integration boundaries at
  // z = [2*mW/E,1-2*mW/E]. Within this approximation scheme, disagreements with the shower rate integrate to a small single-log "error" on the double-log 
  // integrated running/splitting, wherein single-log effects are not even formally under predictive control anyway.
  // (Note that this is effectively a *prescription* for running the wavefunction, with some assumptions about the structure of formally
  //  subleading soft wide-angle contributions that would contribute to the branch cut for real Q^2. In the future, perhaps consider studying this
  //  structure in more explicit detail.)
  double VtoVVintegral(double z1, double z2) {
    if (z1>z2) return 0;
    return ((1 - z1)*(-24 + square(z1 - z2))*(z1 - z2)*(-1 + z2) + 
	    12*(2 + square(z1) + 3*z1*z2 + square(z2))*log((-1 + z1)/(-1 + z2)) + 
	    6*((2 + 4*z1 + 4*z2)*log((-1 + z2)/(-1 + z1)) + 
	       2*z1*z2*((1 + z1*z2)*log((z1*(-1 + z2))/((-1 + z1)*z2)) + 
			(z1 + z2)*log(z2/z1)) + (-1 + z1)*(-1 + z2)*(z1 + z2 + 2*z1*z2)*
	       log(((-1 + z1)*z2)/(z1*(-1 + z2)))))/(6.*(-1 + z1)*(-1 + z2));
  }


  // check particle identities
  bool isQuark(const Particle & part) { return (abs(part.id)>0 && abs(part.id)<=6); }
  bool isLightQuark(const Particle & part) { return (abs(part.id)>0 && abs(part.id)<=NF); }
  bool isTopQuark(const Particle & part) { return (abs(part.id)==6); }
  bool isBottomQuark(const Particle & part) { return (abs(part.id)==5); }
  bool isGluon(const Particle & part) { return (part.id==21); }
  bool isLightFermion(const Particle & part) { return ( isLightQuark(part)  ||  (abs(part.id)>=11 && abs(part.id)<=16) ); }
  bool isWT(const Particle & part) { return (abs(part.id)==24 && abs(part.helicity) > 0); }
  bool isWlong(const Particle & part) { return (abs(part.id)==24 && abs(part.helicity) == 0); }
  bool isZT(const Particle & part) { return (part.id==23 && abs(part.helicity) > 0); }
  bool isZlong(const Particle & part) { return ((part.id==23 || (part.id==2325 && part.densityMatrix[0]==0 && part.densityMatrix[1]!=0 && part.densityMatrix[2]==0 && part.densityMatrix[3]==0)) && abs(part.helicity) == 0); }
  bool isGamma(const Particle & part) { return (part.id==22); }  // photon implicitly transverse
  bool isGamZT(const Particle & part) { return (part.id==2223); }  // mixed transverse photon/Z state
  bool isHiggsZlong(const Particle & part) { return (part.id==2325); }  // mixed h/Zlong state
  bool isVT(const Particle & part) { return (isWT(part) || isZT(part)); }
  bool isResonance(const Particle & part) { return (abs(part.id)==6 || abs(part.id)==24 || part.id==23 || part.id==25 || part.id==2223 || part.id==2325); }
  bool isVector(const Particle & part) { return (part.id==22 || part.id==23 || part.id==2223 || abs(part.id)==24); }  // not including gluons
  
  
  // declare decay routines
  void DecayTop(Particle & top);
  void DecayW(Particle & W);
  void DecayZ(Particle & Z);
  void Decay(Particle & part) { 
    if (isTopQuark(part))  DecayTop(part);
    if (isWT(part) || isWlong(part))  DecayW(part);
    if (isZT(part) || isZlong(part))  DecayZ(part);
  }

  // (primitive) function for assigning colors to quarks
  void Colorize(const Particle & mother, Particle & daughter1, Particle & daughter2)
  {
    daughter1.color = 0;  daughter1.anticolor = 0;
    daughter2.color = 0;  daughter2.anticolor = 0; 
    //
    if (mother.color == 0 && mother.anticolor == 0 && isQuark(daughter1) && isQuark(daughter2) && daughter1.id*daughter2.id < 0) {  // singlet -> q qbar
      daughter1.color = (daughter1.signid()>0)*current_max_color;  daughter1.anticolor = (daughter1.signid()<0)*current_max_color;
      daughter2.color = (daughter2.signid()>0)*current_max_color;  daughter2.anticolor = (daughter2.signid()<0)*current_max_color;  
      current_max_color++;
    }
  }




  ////////////////////////////////////////////////////////////////////////////////
  //
  //                           SPLITTING FUNCTIONS
  //
  ////////////////////////////////////////////////////////////////////////////////
  
  // splitting functions a->bc, for incoming "a" with fixed virtuality, and splitting to on-shell "b" and "c" with z = |pb|/(|pb|+|pc|)
  //
  // "f" is shorthand for dP/(dz dlogQ^2), with some couplings stripped off (put into CouplingPrefactor functions)
  // "F" is shorthand for its indefinite integral (with some arbitrary starting point...usually not available, except in special cases)
  // "g" is shorthand for an overestimator of f, with a simple and easily-invertible indefinite integral, to facilitate Sudakov evaluation and MC number generation
  // "G" is shorthand for its indefinite integral
  // "Ginv" is the inverse function of G
  //
  // the QCD splittings follow exactly the conventions of PYTHIA6:  alphas/2pi * P(z).  
  // our EW splitting functions are schematically set up as (couplings/16pi^2) * (common kinematic prefactor ~ 1/Q^2) * |ME|^2, possibly with reweighting factors.
  // (PYTHIA6 QCD extracts an extra factor of 2 out of the |ME|^2 and uses it to cancel down 4pi->2pi)
  //
  //   global kinematic variables
  double _Ea,_Q,_Q2,_ma;  // incoming kinematics
  double _mb,_mc;  // fixed according to particle species in splitting function
  Particle *_aVirtual,*_aSisterRecoiled,*_aMother,*_aAunt;  // pointers to the incoming particle and its immediate family after its virtualization
  Particle *_aOnShell,*_aSisterUnrecoiled;
  double _Q2overPropagator,_Q4overPropagator;  // handy combinations of Q^2 and ma^2
  double _z,_zBar;  // momentum splitting variables (zBar == 1-z = |Pc|/(|Pb|+|Pc|))
  double _zReg,_zBarReg;  // momentum splitting regulated by mass, as we'd find in GEG longitudinal pol'n, e.g. zReg = (Eb+z*Ptot)/(2*Ptot)
  double _Ptot,_Eb,_Ec,_th,_th2;  // derived kinematics
  double _kinematicPrefactor;  // common kinematic prefactor, which reduces to 1/Q^2 when all particles are massless
  double _PR_eval,_PL_eval,_Plong_eval;  // individual splitting polarizations, set within the splitting function calculation for later access
  double _PTT_eval,_PTlong_eval,_PlongT_eval,_Plonglong_eval;  // individual splitting polarizations, set within the splitting function calculation for later access
  vector<double> _dPij_total(4,0.), _dPij_WT_WT(4,0.), _dPij_Wlong_Wlong(4,0.), _dPij_h_Zlong(4,0.);
  vector<double> _dPij_sum_f_f(4,0.), _dPij_t_t(4,0.), _dPij_b_b(4,0.);
  vector<double> _dPij_uR(4,0.), _dPij_uL(4,0.), _dPij_dR(4,0.), _dPij_dL(4,0.), _dPij_eR(4,0.), _dPij_eL(4,0.), _dPij_veL(4,0.);
  vector<double> _dPij_tR(4,0.), _dPij_tL(4,0.), _dPij_bR(4,0.), _dPij_bL(4,0.);
  void SetKinematics(double z) { _z = z; _zBar = 1-_z; _Ptot = ComputePtot(_z,_Ea,_mb,_mc); _Eb = sqrt(_z*_z*_Ptot*_Ptot+_mb*_mb); _Ec = sqrt(_zBar*_zBar*_Ptot*_Ptot+_mc*_mc); _zReg = (_Eb+_z*_Ptot)/(2*_Ptot);  _zBarReg = (_Ec+_zBar*_Ptot)/(2*_Ptot);  _th = ComputeTheta(_z,_Q,_Ptot,_Eb,_Ec,_mb,_mc); _th2 = _th*_th; _kinematicPrefactor = (_z*_zBar*_Ea*_Ptot)/(_Eb*_Ec) * _Q2overPropagator; }
  
  ////////   QCD   //////
  //
  //   q -> g q  [Depricated]
  double CouplingPrefactor__q_to_g_q__OLD() { return CF*alphas/twoPi; }
  double f__q_to_g_q__OLD(double z) { return (1+(1-z)*(1-z))/z;}
  double F__q_to_g_q__OLD(double z) { return (2*log(z) - 2*(z-1) + 0.5*(z*z-1)); }  // cumulative function (starting at 1 for convenience)
  double g__q_to_g_q__OLD(double z) { return 2/z; }  // simple overestimated splitting function, for random # generation
  double G__q_to_g_q__OLD(double z) { return 2*log(z); };  // simple overestimated cumulative function, for random # generations
  double Ginv__q_to_g_q__OLD(double G) { return exp(G/2); }  // inverse of overestimated cumulative function, for random # generation
  //
  //   g -> g g  [Depricated]
  double CouplingPrefactor__g_to_g_g__OLD() { return NC*alphas/twoPi; }
  double f__g_to_g_g__OLD(double z) { return (1-z*(1-z))*(1-z*(1-z))/z/(1-z); }
  double F__g_to_g_g__OLD(double z) { return (11./12 - 1./6*z*(12-3*z+2*z*z) - 2*atanh(1-2*z)); }  // (starting at 1/2 for convenience)
  double g__g_to_g_g__OLD(double z) { return 1/z/(1-z); }
  double G__g_to_g_g__OLD(double z) { return (-2)*atanh(1-2*z); };
  double Ginv__g_to_g_g__OLD(double G) { return 0.5*(1-tanh(-G/2)); }
  //
  //   g -> q q~  [Depricated]
  double CouplingPrefactor__g_to_q_q__OLD() { return TR*alphas/twoPi; }
  double f__g_to_q_q__OLD(double z) { return (z*z + (1-z)*(1-z)); }
  double F__g_to_q_q__OLD(double z) { return (-1./3 + z - z*z * 2./3*z*z*z); }  // (starting at 1/2 for convenience)
  double g__g_to_q_q__OLD(double z) { return 1; }
  double G__g_to_q_q__OLD(double z) { return (z - 1./2); };
  double Ginv__g_to_q_q__OLD(double G) { return (G + 1./2); }
  //
  //   q -> g q
  double CouplingPrefactor__q_to_g_q() { return CF*alphas/fourPi; }
  double M2__q_to_g_q()    { return _z*_zBar*_Ptot*_Ea*_th2 * 2*(1+_zBar*_zBar)/_z; }  // currently *not* keeping track of gluon polarizations (probably need coherent)
  double f__q_to_g_q(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  q -> g q" << endl;  return 0; }
    return _kinematicPrefactor*M2__q_to_g_q();
  }
  double g__q_to_g_q(double z) { return 4/z*_Q4overPropagator; } 
  double G__q_to_g_q(double z) { return 4*log(z)*_Q4overPropagator; };
  double Ginv__q_to_g_q(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   g -> g g
  double CouplingPrefactor__g_to_g_g() { return NC*alphas/fourPi; }
  double M2__g_to_g_g() {  return _z*_zBar*_Ptot*_Ptot*_th2 * 2*square(1-_z*_zBar)/(_z*_zBar); } 
  double f__g_to_g_g(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  g -> g g" << endl;  return 0; }
    return _kinematicPrefactor*M2__g_to_g_g();
  }
  double g__g_to_g_g(double z) { return 2/z/(1-z); }
  double G__g_to_g_g(double z) { return (-4)*atanh(1-2*z); };
  double Ginv__g_to_g_g(double G) { return 0.5*(1-tanh(-G/4)); }
  //
  //   g -> q q~
  double CouplingPrefactor__g_to_q_q() { return     TR*alphas/fourPi; }
  double CouplingPrefactor__g_to_t_t() { return (1./2)*alphas/fourPi; }
  double CouplingPrefactor__g_to_b_b() { return (1./2)*alphas/fourPi; }
  double M2__g_to_q_q() {  return _z*_zBar*_Ptot*_Ea*_th2 * 2*(_z*_z+_zBar*_zBar); } 
  double f__g_to_q_q(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  g -> q q" << endl;  return 0; }
    return _kinematicPrefactor*M2__g_to_q_q();
  }
  double g__g_to_q_q(double z) { return 2; }
  double G__g_to_q_q(double z) { return 2*(z - 1./2); };
  double Ginv__g_to_q_q(double G) { return (G/2 + 1./2); }
  //
  //   g -> tRL tRL~   [Ultracollinear, via (QCD gauge-invariant) top mass insertion]
  double CouplingPrefactor__g_to_tRL_tRL() { return do_ultracollinear * mtop*mtop * CouplingPrefactor__g_to_t_t(); }
  double M2__g_to_tRL_tRL() {  return 2/(_z*_zBar); }   // factor of 2 from LL+RR
  double f__g_to_tRL_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  g -> tRL tRL" << endl;  return 0; }
    return _kinematicPrefactor*M2__g_to_tRL_tRL();
  }
  double g__g_to_tRL_tRL(double z) { return 2/z/(1-z) * _Q2overPropagator; }  // note that the "coupling" above is dimensionful
  double G__g_to_tRL_tRL(double z) { return -4*atanh(1-2*z) * _Q2overPropagator; };
  double Ginv__g_to_tRL_tRL(double G) { return 0.5*(1-tanh(-G/4/_Q2overPropagator)); }
  //
  //   g -> bRL bRL~   [Ultracollinear, via (QCD gauge-invariant) bottom mass insertion]
  double CouplingPrefactor__g_to_bRL_bRL() { return do_ultracollinear * mbottom*mbottom * CouplingPrefactor__g_to_b_b(); }
  double M2__g_to_bRL_bRL() {  return 2/(_z*_zBar); }   // factor of 2 from LL+RR
  double f__g_to_bRL_bRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  g -> bRL bRL" << endl;  return 0; }
    return _kinematicPrefactor*M2__g_to_bRL_bRL();
  }
  double g__g_to_bRL_bRL(double z) { return 2/z/(1-z) * _Q2overPropagator; }  // note that the "coupling" above is dimensionful
  double G__g_to_bRL_bRL(double z) { return -4*atanh(1-2*z) * _Q2overPropagator; };
  double Ginv__g_to_bRL_bRL(double G) { return 0.5*(1-tanh(-G/4/_Q2overPropagator)); }

  ///////  Light fermion splittings  //////////
  //
  //   f -> W(T/long) f'
  double CouplingPrefactor__f_to_W_f() { return (_aVirtual->chirality()==-1)*QW2*alphaW/fourPi; } 
  double M2__fL_to_WR_fL()    { return 2*_Ptot*_Ea*_zBar*_zBar*_zBar*_th2; }
  double M2__fL_to_WL_fL()    { return 2*_Ptot*_Ea*_zBar*            _th2; }
  double M2__fL_to_Wlong_fL() { return do_ultracollinear * 16*_Ptot*_Ea*_zBar*mW*mW/(_Eb+_z*_Ptot)/(_Eb+_z*_Ptot) * (1-_zBar*_th2/4)*(1-_zBar*_th2/4); }
  double f__f_to_W_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  f -> W f" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = _kinematicPrefactor*M2__fL_to_Wlong_fL(); }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = _kinematicPrefactor*M2__fL_to_Wlong_fL(); }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__f_to_W_f(double z) { return 4/z; }
  double G__f_to_W_f(double z) { return 4*log(z); }
  double Ginv__f_to_W_f(double G) { return exp(G/4); }
  //
  //   f -> Z(T) f   [superceded by mixed gamma-ZT emission]
  double CouplingPrefactor__f_to_ZT_f() { return square(QZ(_aVirtual)/cosThetaW)*alphaW/fourPi; } 
  double M2__fL_to_ZR_fL()    { return M2__fL_to_WR_fL(); }
  double M2__fL_to_ZL_fL()    { return M2__fL_to_WL_fL(); }
  double f__f_to_ZT_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  f -> Z(T) f" << endl; return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_ZR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_ZL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_ZL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_ZR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__f_to_ZT_f(double z) { return g__f_to_W_f(z); }
  double G__f_to_ZT_f(double z) { return G__f_to_W_f(z); }
  double Ginv__f_to_ZT_f(double G) { return Ginv__f_to_W_f(G); }
  //
  //   f -> photon/Z(T) f
  double CouplingPrefactor__f_to_gamZT_f() { return ( square(QZ(_aVirtual)/cosThetaW)*alphaW + square(QEM(_aVirtual))*alphaEM ) / fourPi; } 
  double f__f_to_gamZT_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  f -> photon/Z(T) f" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__f_to_gamZT_f(double z) { return g__f_to_W_f(z); }
  double G__f_to_gamZT_f(double z) { return G__f_to_W_f(z); }
  double Ginv__f_to_gamZT_f(double G) { return Ginv__f_to_W_f(G); }
  //
  //   f -> Z(long) f
  double CouplingPrefactor__f_to_Zlong_f() { return do_ultracollinear * square(QZ(_aVirtual)/cosThetaW)*alphaW/fourPi; } 
  double M2__fL_to_Zlong_fL() { return M2__fL_to_Wlong_fL(); }
  double f__f_to_Zlong_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  f -> Z(long) f" << endl; return 0; }
    return _kinematicPrefactor*M2__fL_to_Zlong_fL(); }
  double g__f_to_Zlong_f(double z) { return g__f_to_W_f(z); }
  double G__f_to_Zlong_f(double z) { return G__f_to_W_f(z); }
  double Ginv__f_to_Zlong_f(double G) { return Ginv__f_to_W_f(G); }

  ///////////  Top quark splittings  ///////////////
  //
  //   t(L) -> W(T)+ b(L)
  double CouplingPrefactor__tL_to_WT_bL() { return QW2*alphaW/fourPi; } 
  double f__tL_to_WT_bL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(L) -> W(T) b(L)" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__tL_to_WT_bL(double z) { return (4/z)*_Q4overPropagator; }
  double G__tL_to_WT_bL(double z) { return (4*log(z))*_Q4overPropagator; }
  double Ginv__tL_to_WT_bL(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   t(L) -> W(long)+ b(L)  [Ultracollinear, via broken Yukawa+gauge (interfering)]
  double CouplingPrefactor__tL_to_Wlong_bL() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__tL_to_Wlong_bL() { return  square(ytop*mtop - 4*sqrt(QW2)*gW*mW*sqrt(_Ptot*_Ea)/(_Eb+_z*_Ptot)) * _zBar; }  // neglecting here bottom Yukawa
  double f__tL_to_Wlong_bL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(L) -> W(long) b(L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__tL_to_Wlong_bL(); }
  double g__tL_to_Wlong_bL(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * (1/z) * _Q4overPropagator; }
  double G__tL_to_Wlong_bL(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * log(z) * _Q4overPropagator; }
  double Ginv__tL_to_Wlong_bL(double G) { return exp(2*G / (ytop*ytop + 4*QW2*gW*gW) / (_Q4overPropagator)); }
  //
  //   t(R) -> W(T)+ b(L)   [Ultracollinear / soft-finite, via top spin-flip]  (to smoothly match onto top decay)
  double CouplingPrefactor__tR_to_WT_bL() { return do_ultracollinear * QW2*alphaW/twoPi; }
  double M2__tR_to_WT_bL() { return  mtop*mtop * _zBar; }  // neglecting here bottom Yukawa
  double f__tR_to_WT_bL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(L) -> W(T) b(L)" << endl;  return 0; }
    if (_aVirtual->helicity == 1)  { _PR_eval = _kinematicPrefactor*M2__tR_to_WT_bL();  _PL_eval = 0;  _Plong_eval = 0; }
    else                           { _PR_eval = 0;  _PL_eval = _kinematicPrefactor*M2__tR_to_WT_bL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__tR_to_WT_bL(double z) { return (1-z) * _Q4overPropagator; }
  double G__tR_to_WT_bL(double z) { return (z-z*z/2) * _Q4overPropagator; }
  double Ginv__tR_to_WT_bL(double G) { return 1 - sqrt(1-2*G/(_Q4overPropagator)); }
  //
  //   t(R/L) -> W(long)+ b(L/R) via Yukawa   (Strictly speaking, L->R is highly suppressed by mbottom/E, but kept anyway)
  double CouplingPrefactor__tRL_to_Wlong_bLR() { double pre = 1./(16*PiSq);  if (_aVirtual->chirality() > 0) pre*=ytop*ytop; else pre*= ybottom*ybottom; return pre; }
  double M2__tRL_to_Wlong_bLR() { return _Ea*_Ea * _zBar*_z*_z * _th2; }
  double f__tRL_to_Wlong_bLR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> W(long) b(L/R)" << endl;  return 0; }
    return _kinematicPrefactor*M2__tRL_to_Wlong_bLR(); }
  double g__tRL_to_Wlong_bLR(double z) { return z *_Q4overPropagator; }
  double G__tRL_to_Wlong_bLR(double z) { return (z*z/2) *_Q4overPropagator; }
  double Ginv__tRL_to_Wlong_bLR(double G) { return sqrt(2*G/(_Q4overPropagator)); }
  //
  //   t(R/L) -> Z(T) t(R/L)
  double CouplingPrefactor__tRL_to_ZT_tRL() { return square(QZ(_aVirtual)/cosThetaW)*alphaW/fourPi; } 
  double f__tRL_to_ZT_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> Z(T) t(R/L)" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__tRL_to_ZT_tRL(double z) { return (4/z)*_Q4overPropagator; }
  double G__tRL_to_ZT_tRL(double z) { return (4*log(z))*_Q4overPropagator; }
  double Ginv__tRL_to_ZT_tRL(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   t(R/L) -> photon/Z(T) t(R/L)
  double CouplingPrefactor__tRL_to_gamZT_tRL() { return ( square(QZ(_aVirtual)/cosThetaW)*alphaW + square(QEM(_aVirtual))*alphaEM ) / fourPi; } 
  double f__tRL_to_gamZT_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> photon/Z(T) t(R/L)" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__tRL_to_gamZT_tRL(double z) { return (4/z)*_Q4overPropagator; }
  double G__tRL_to_gamZT_tRL(double z) { return (4*log(z))*_Q4overPropagator; }
  double Ginv__tRL_to_gamZT_tRL(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   t(R/L) -> Z(long) t(R/L)  [Ultracollinear, via broken Yukawa+gauge (interfering)]
  double CouplingPrefactor__tRL_to_Zlong_tRL() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__tRL_to_Zlong_tRL() { return  square(_aVirtual->chirality()*ytop/sqrt2*(1/_zBar-1)*mtop - 4*QZ(_aVirtual)*(gW/cosThetaW)*mZ*sqrt(_Ptot*_Ea)/(_Eb+_z*_Ptot)) * _zBar; }
  double f__tRL_to_Zlong_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> Z(long) t(R/L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__tRL_to_Zlong_tRL(); }
  double g__tRL_to_Zlong_tRL(double z) { return (ytop*ytop + 4*square(QZ(_aVirtual)*gW/cosThetaW)) * (1/z) * _Q4overPropagator; }  //// need a slightly better over-estimator
  double G__tRL_to_Zlong_tRL(double z) { return (ytop*ytop + 4*square(QZ(_aVirtual)*gW/cosThetaW)) * log(z) * _Q4overPropagator; }
  double Ginv__tRL_to_Zlong_tRL(double G) { return exp(G / (ytop*ytop + 4*square(QZ(_aVirtual)*gW/cosThetaW)) / (_Q4overPropagator)); }
  //
  //   t(R/L) -> h t(R/L)  [Ultracollinear, via broken Yukawa]
  double CouplingPrefactor__tRL_to_h_tRL() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__tRL_to_h_tRL() { return  square(ytop/sqrt2*(1/_zBar+1)*mtop) * _zBar; }
  double f__tRL_to_h_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> Z(long) t(R/L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__tRL_to_h_tRL(); }
  double g__tRL_to_h_tRL(double z) { return (ytop*ytop) * (1/z) * _Q4overPropagator; }  //// need a slightly better over-estimator
  double G__tRL_to_h_tRL(double z) { return (ytop*ytop) * log(z) * _Q4overPropagator; }
  double Ginv__tRL_to_h_tRL(double G) { return exp(G / (ytop*ytop) / (_Q4overPropagator)); }
  //
  //   t(R/L) -> h/Z(long) t(L/R) via Yukawa 
  double CouplingPrefactor__tRL_to_H0_tLR() { return ytop*ytop/(16*PiSq); }
  double M2__tRL_to_H0_tLR() { return _Ea*_Ea * _zBar*_z*_z * _th2; }
  double f__tRL_to_H0_tLR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  t(R/L) -> h/Z(long) t(L/R)" << endl;  return 0; }  // in case of NaN
    return _kinematicPrefactor*M2__tRL_to_H0_tLR(); }
  double g__tRL_to_H0_tLR(double z) { return z *_Q4overPropagator; }
  double G__tRL_to_H0_tLR(double z) { return (z*z/2) *_Q4overPropagator; }
  double Ginv__tRL_to_H0_tLR(double G) { return sqrt(2*G/(_Q4overPropagator)); }

  //////////////  Bottom quark splittings  /////////////////
  //
  //   b(L) -> W(T)- t(L)
  double CouplingPrefactor__bL_to_WT_tL() { return QW2*alphaW/fourPi; } 
  double f__bL_to_WT_tL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(L) -> W(T) t(L)" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__bL_to_WT_tL(double z) { return (4/z)*_Q4overPropagator; }
  double G__bL_to_WT_tL(double z) { return (4*log(z))*_Q4overPropagator; }
  double Ginv__bL_to_WT_tL(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   b(L) -> W(long)- t(L)  [Ultracollinear, via broken Yukawa+gauge (interfering)]
  double CouplingPrefactor__bL_to_Wlong_tL() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__bL_to_Wlong_tL() { return  square(-ytop*mtop/_zBar - 4*sqrt(QW2)*gW*mW*sqrt(_Ptot*_Ea)/(_Eb+_z*_Ptot)) * _zBar; }  // neglecting here bottom Yukawa...note nontrivial relative sign-flip between Yukawa and gauge contributions compared to t->Wb
  double f__bL_to_Wlong_tL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(L) -> W(long) t(L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__bL_to_Wlong_tL(); }
  double g__bL_to_Wlong_tL(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * (1/z) * _Q4overPropagator; }
  double G__bL_to_Wlong_tL(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * log(z) * _Q4overPropagator; }
  double Ginv__bL_to_Wlong_tL(double G) { return exp(2*G / (ytop*ytop + 4*QW2*gW*gW) / (_Q4overPropagator)); }
  //
  //   b(L) -> W(T)- t(R)   [Ultracollinear / soft-finite, via top spin-flip]
  double CouplingPrefactor__bL_to_WT_tR() { return do_ultracollinear * QW2*alphaW/twoPi; }
  double M2__bL_to_WT_tR() { return  mtop*mtop / _zBar; }  // neglecting here bottom Yukawa
  double f__bL_to_WT_tR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(L) -> W(T) t(R)" << endl;  return 0; }
    if (_aVirtual->helicity == 1)  { _PR_eval = _kinematicPrefactor*M2__bL_to_WT_tR();  _PL_eval = 0;  _Plong_eval = 0; }
    else                           { _PR_eval = 0;  _PL_eval = _kinematicPrefactor*M2__bL_to_WT_tR();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__bL_to_WT_tR(double z) { return (1-z) * _Q4overPropagator; }
  double G__bL_to_WT_tR(double z) { return (z-z*z/2) * _Q4overPropagator; }
  double Ginv__bL_to_WT_tR(double G) { return 1 - sqrt(1-2*G/(_Q4overPropagator)); }
  //
  //   b(L/R) -> W(long)- t(R/L) via Yukawa   (Strictly speaking, R->L is highly suppressed by mbottom/E, but kept anyway)
  double CouplingPrefactor__bLR_to_Wlong_tRL() { double pre = 1./(16*PiSq);  if (_aVirtual->chirality() < 0) pre*=ytop*ytop; else pre*= ybottom*ybottom; return pre; }
  double M2__bLR_to_Wlong_tRL() { return _Ea*_Ea * _zBar*_z*_z * _th2; }
  double f__bLR_to_Wlong_tRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(L/R) -> W(long) t(R/L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__bLR_to_Wlong_tRL(); }
  double g__bLR_to_Wlong_tRL(double z) { return z *_Q4overPropagator; }
  double G__bLR_to_Wlong_tRL(double z) { return (z*z/2) *_Q4overPropagator; }
  double Ginv__bLR_to_Wlong_tRL(double G) { return sqrt(2*G/(_Q4overPropagator)); }
  //
  //   b(R/L) -> photon/Z(T) b(R/L)
  double CouplingPrefactor__bRL_to_gamZT_bRL() { return ( square(QZ(_aVirtual)/cosThetaW)*alphaW + square(QEM(_aVirtual))*alphaEM ) / fourPi; } 
  double f__bRL_to_gamZT_bRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(R/L) -> photon/Z(T) b(R/L)" << endl;  return 0; }
    if (_aVirtual->helicity == -1)  { _PR_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _Plong_eval = 0; }
    else                            { _PR_eval = _kinematicPrefactor*M2__fL_to_WL_fL();  _PL_eval = _kinematicPrefactor*M2__fL_to_WR_fL();  _Plong_eval = 0; }
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__bRL_to_gamZT_bRL(double z) { return (4/z)*_Q4overPropagator; }
  double G__bRL_to_gamZT_bRL(double z) { return (4*log(z))*_Q4overPropagator; }
  double Ginv__bRL_to_gamZT_bRL(double G) { return exp(G/4/(_Q4overPropagator)); }
  //
  //   b(R/L) -> Z(long) b(R/L)  [Ultracollinear, via broken gauge]
  double CouplingPrefactor__bRL_to_Zlong_bRL() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__bRL_to_Zlong_bRL() { return  square( - 4*QZ(_aVirtual)*(gW/cosThetaW)*mZ*sqrt(_Ptot*_Ea)/(_Eb+_z*_Ptot)) * _zBar; }
  double f__bRL_to_Zlong_bRL(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  b(R/L) -> Z(long) b(R/L)" << endl;  return 0; }
    return _kinematicPrefactor*M2__bRL_to_Zlong_bRL(); }
  double g__bRL_to_Zlong_bRL(double z) { return (4*square(QZ(_aVirtual)*gW/cosThetaW)) * (1/z) * _Q4overPropagator; }  //// need a slightly better over-estimator
  double G__bRL_to_Zlong_bRL(double z) { return (4*square(QZ(_aVirtual)*gW/cosThetaW)) * log(z) * _Q4overPropagator; }
  double Ginv__bRL_to_Zlong_bRL(double G) { return exp(G / (4*square(QZ(_aVirtual)*gW/cosThetaW)) / (_Q4overPropagator)); }

  /////////  Transverse gauge splittings  ////////////
  //
  //   W(T) -> photon/Z(T) W(T)
  double CouplingPrefactor__WT_to_gamZT_WT() { return alphaW/Pi; } 
  double M2__WT_to_gamZT_WT() { return square((1-_z*_zBar)*_th*_Ptot) * sqrt(sqrt(fabs(1-0*_th2/4))); }  // summed over final +/-1 helicities, averaged over initial (equal anyway given final-sum); including a "damping" function in effective _th angle, assuming its maximum allowed value is 2 (see ComputeTheta()), to damp unpredictable IR divergences from large-angle emission and simplify the approximate analytic integration for photon/Z mixed wavefunction running
  //double M2__WT_to_gamZT_WT      () { return square((1-_z*_zBar)*_th*_Ptot)*(1+cos(_th)/2; }  // summed over final +/-1 helicities, averaged over initial (equal anyway given final-sum); including a "damping" function in effective _th angle, assuming its maximum allowed value is Pi (see ComputeTheta()), to damp unpredictable IR divergences from large-angle emission and simplify the approximate analytic integration for photon/Z mixed wavefunction running
  double f__WT_to_gamZT_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << eventCounter << "  WARNING: BAD THETA IN  W(T) -> photon/Z(T) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_gamZT_WT(); }
  double g__WT_to_gamZT_WT(double z) { return 3/z/(1-z); }
  double G__WT_to_gamZT_WT(double z) { return (-6)*atanh(1-2*z); }
  double Ginv__WT_to_gamZT_WT(double G) { return 0.5*(1-tanh(-G/6)); }
  //
  //   W(T) -> Z(T) W(T)  [Depricated: Superceded by mixed gamma/Z production, but left here for testing purposes.]
  double CouplingPrefactor__WT_to_ZT_WT() { return alphaW/Pi*cos2ThetaW; } 
  double M2__WT_to_ZT_WT() { return square((1-_z*_zBar)*_th*_Ptot) * sqrt(sqrt(fabs(1-0*_th2/4))); }  // summed over final +/-1 helicities, averaged over initial (equal anyway given final-sum); including a "damping" function in effective _th angle, assuming its maximum allowed value is 2 (see ComputeTheta()), to damp unpredictable IR divergences from large-angle emission and simplify the approximate analytic integration for photon/Z mixed wavefunction running
  //double M2__WT_to_ZT_WT() { return square((1-_z*_zBar)*_th*_Ptot); }  ///////
  double f__WT_to_ZT_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << eventCounter << "  WARNING: BAD THETA IN  W(T) -> Z(T) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_ZT_WT(); }
  double g__WT_to_ZT_WT(double z) { return 3/z/(1-z); }
  double G__WT_to_ZT_WT(double z) { return (-6)*atanh(1-2*z); }
  double Ginv__WT_to_ZT_WT(double G) { return 0.5*(1-tanh(-G/6)); }
  //
  //   W(T) -> W(long) photon(T)   [Ultracollinear via gauge-Goldstone mixing and SU(2)/U(1) VEV/Goldstone cross-term]
  double CouplingPrefactor__WT_to_Wlong_gamT() { return do_ultracollinear * alphaEM/(4*Pi); } 
  double M2__WT_to_Wlong_gamT() { return square(mW*(2*_zBar/_zReg)); }
  double f__WT_to_Wlong_gamT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << eventCounter << "  WARNING: BAD THETA IN  W(T) -> W(long) photon(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_Wlong_gamT(); }
  double g__WT_to_Wlong_gamT(double z) { return 4/z; }
  double G__WT_to_Wlong_gamT(double z) { return 4*log(z); }
  double Ginv__WT_to_Wlong_gamT(double G) { return exp(G/4); }
  //
  //   W(T) -> W(long) Z(T)   [Ultracollinear via gauge-Goldstone mixing and SU(2)/U(1) VEV/Goldstone cross-term]
  double CouplingPrefactor__WT_to_Wlong_ZT() { return do_ultracollinear * alphaW*cos2ThetaW/(4*Pi); } 
  double M2__WT_to_Wlong_ZT() { return square(mW*(2*_zBar/_zReg + 1/cos2ThetaW)); }
  double f__WT_to_Wlong_ZT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << eventCounter << "  WARNING: BAD THETA IN  W(T) -> W(long) Z(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_Wlong_ZT(); }
  double g__WT_to_Wlong_ZT(double z) { return 4/z; }
  double G__WT_to_Wlong_ZT(double z) { return 4*log(z); }
  double Ginv__WT_to_Wlong_ZT(double G) { return exp(G/4); }
  //
  //   W(T) -> Z(long) W(T)   [Ultracollinear via gauge-Goldstone mixing]
  double CouplingPrefactor__WT_to_Zlong_WT() { return do_ultracollinear * cos2ThetaW*alphaW/(4*Pi); } 
  double M2__WT_to_Zlong_WT() { return square(mZ*(1+_zBar)/_zReg); }  
  double f__WT_to_Zlong_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << eventCounter << "  WARNING: BAD THETA IN  W(T) -> Z(long) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_Zlong_WT(); }
  double g__WT_to_Zlong_WT(double z) { return 4/z; }
  double G__WT_to_Zlong_WT(double z) { return 4*log(z); }
  double Ginv__WT_to_Zlong_WT(double G) { return exp(G/4); }
  //
  //   W(T) -> h W(T)   [Ultracollinear via hWW coupling]
  double CouplingPrefactor__WT_to_h_WT() { return do_ultracollinear * alphaW*mW*mW/fourPi; } 
  double M2__WT_to_h_WT() { return 1; }  // just the dot product of polarizations (=1 for same-helicity, =0 for opposite-helicity, in strict collinear limit)
  double f__WT_to_h_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(T) -> h W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_h_WT(); }
  double g__WT_to_h_WT(double z) { return 2*_Q2overPropagator; }  // note that the "coupling" above is dimensionful
  double G__WT_to_h_WT(double z) { return 2*_Q2overPropagator*z; }
  double Ginv__WT_to_h_WT(double G) { return G/(2*_Q2overPropagator); }
  //
  //   W(T) -> f f'  light fermions
  double CouplingPrefactor__WT_to_f_f() { return Ndoublets*alphaW*QW2/fourPi; } 
  double M2__WT_to_f_f() { return _z*_zBar*(_z*_z+_zBar*_zBar)*_th2*_Ptot*_Ptot; } 
  double f__WT_to_f_f(double z) { 
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  W(T) -> f f'" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_f_f(); }
  double g__WT_to_f_f(double z) { return _Q4overPropagator; }
  double G__WT_to_f_f(double z) { return _Q4overPropagator*z; }
  double Ginv__WT_to_f_f(double G) { return G/(_Q4overPropagator); }
  //
  //   W(T) -> t b  (chirality-preserving)
  double CouplingPrefactor__WT_to_t_b() { return NC*alphaW*QW2/fourPi; } 
  double M2__WT_to_t_b() { return M2__WT_to_f_f(); } 
  double f__WT_to_t_b(double z) { 
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  W(T) -> t b (chirality-preserving)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_t_b(); }
  double g__WT_to_t_b(double z) { return g__WT_to_f_f(z); }
  double G__WT_to_t_b(double z) { return G__WT_to_f_f(z); }
  double Ginv__WT_to_t_b(double G) { return Ginv__WT_to_f_f(G); }
  //
  //   W(T) -> h/Z(long) W(long)  via scalar gauge coupling
  double CouplingPrefactor__WT_to_H0_Wlong() { return 2*alphaW*QW2/fourPi; }
  double M2__WT_to_H0_Wlong() { return square(_z*_zBar*_th*_Ptot); }
  double f__WT_to_H0_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(T) -> h/Z(long) W(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__WT_to_H0_Wlong(); }
  double g__WT_to_H0_Wlong(double z) { return 1.; }
  double G__WT_to_H0_Wlong(double z) { return z; }
  double Ginv__WT_to_H0_Wlong(double G) { return G; }
  //
  //   photon/Z(T) -> W(T) W(T)
  double CouplingPrefactor__gamZT_to_WT_WT() {
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    Get_gamZT_to_WT_WT_dPijPrefactor(_Q2,_dPij_WT_WT);
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_WT_WT)/Trace(_aVirtual->densityMatrix);
  } 
  double M2__gamZT_to_WT_WT()  { return M2__WT_to_gamZT_WT(); }  // summed over final +/-1 helicities, averaged over initial (equal anyway given final-sum)
  double f__gamZT_to_WT_WT(double z) { 
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> W(T) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__gamZT_to_WT_WT(); }
  double g__gamZT_to_WT_WT(double z) { return g__WT_to_gamZT_WT(z); }
  double G__gamZT_to_WT_WT(double z) { return G__WT_to_gamZT_WT(z); };
  double Ginv__gamZT_to_WT_WT(double G) { return Ginv__WT_to_gamZT_WT(G); }
  //
  //   photon/Z(T) -> W(long) W(T)  [Ultracollinear]   (both charge channels...M^2 is doubled and charge is chosen randomly below)
  double CouplingPrefactor__gamZT_to_Wlong_WT() {  return do_ultracollinear * 1./(16*PiSq);  } 
  double M2__gamZT_to_Wlong_WT() { 
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    double DgamZT_BB = cos2ThetaW/_Q2 + sin2ThetaW/(_Q2-mZ*mZ);  // neutral vector boson propagator matrix
    double DgamZT_WW = sin2ThetaW/_Q2 + cos2ThetaW/(_Q2-mZ*mZ);   // D_WW
    double DgamZT_BW = -cosThetaW*sinThetaW*mZ*mZ / (_Q2*(_Q2-mZ*mZ));   // Re[D_BW]
    double M_B = gY*mW;  // B0 1->2 matrix element
    double M_W = gW*mW*(1+_zBar)*(2*_Ptot)/(_Eb+_z*_Ptot);  // W0 1->2 matrix element
    double DM_B = DgamZT_BB*M_B + DgamZT_BW*M_W;   // convolve with propagator matrix
    double DM_W = DgamZT_BW*M_B + DgamZT_WW*M_W;
    double naivePropDenom2 =  square(_Q2 - square(m(2223)));
    vector<double> dPij(4,0.);
    dPij[0] = naivePropDenom2 * DM_B*DM_B;
    dPij[1] = naivePropDenom2 * DM_W*DM_W;
    dPij[2] = naivePropDenom2 * DM_B*DM_W;
    dPij[3] = 0;  // no imaginary part
    return 2 * MultiplyTrace(_aVirtual->densityMatrix,dPij)/Trace(_aVirtual->densityMatrix); }  // extra factor of 2 to count both W+(long)W-(T) and W-(long)W+(T)
  double f__gamZT_to_Wlong_WT(double z) { 
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> W(long) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__gamZT_to_Wlong_WT(); }
  double g__gamZT_to_Wlong_WT(double z) { return 8*(gY*gY+gW*gW)/z; }   //// need to multiply by intelligent charge factors
  double G__gamZT_to_Wlong_WT(double z) { return 8*(gY*gY+gW*gW)*log(z); };
  double Ginv__gamZT_to_Wlong_WT(double G) { return exp(G/8/(gY*gY+gW*gW)); }
  //
  //   photon/Z(T) -> f f  (coherent density matrix evolution)
  double CouplingPrefactor__gamZT_to_f_f() {   // summing "massless" fermions (everything except bottom & top)
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    fill(_dPij_sum_f_f.begin(),_dPij_sum_f_f.end(),0.);  // initialize splitting matrix to zero
    Get_gamZT_to_f_f_dPijPrefactor( 2, 1,_Q2,_dPij_uR,NC);
    Get_gamZT_to_f_f_dPijPrefactor( 2,-1,_Q2,_dPij_uL,NC);
    Get_gamZT_to_f_f_dPijPrefactor( 1, 1,_Q2,_dPij_dR,NC);
    Get_gamZT_to_f_f_dPijPrefactor( 1,-1,_Q2,_dPij_dL,NC);
    Get_gamZT_to_f_f_dPijPrefactor(11, 1,_Q2,_dPij_eR, 1);
    Get_gamZT_to_f_f_dPijPrefactor(11,-1,_Q2,_dPij_eL, 1);
    Get_gamZT_to_f_f_dPijPrefactor(12,-1,_Q2,_dPij_veL,1);
    AddTo( _dPij_sum_f_f, _dPij_uR , 2 );  // uR+cR
    AddTo( _dPij_sum_f_f, _dPij_uL , 2 );  // uL+cL
    AddTo( _dPij_sum_f_f, _dPij_dR , 2 );  // dR+sR
    AddTo( _dPij_sum_f_f, _dPij_dL , 2 );  // dL+dL
    AddTo( _dPij_sum_f_f, _dPij_eR , 3 );  // eR+muR+tauR
    AddTo( _dPij_sum_f_f, _dPij_eL , 3 );  // eL+muL+tauL
    AddTo( _dPij_sum_f_f, _dPij_veL, 3 );  // veL+vmuL+vtauL
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_sum_f_f)/Trace(_aVirtual->densityMatrix);
  }
  double M2__gamZT_to_f_f() { return _z*_zBar*(_z*_z+_zBar*_zBar)*_th2*_Ptot*_Ptot; } 
  double f__gamZT_to_f_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> f f" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamZT_to_f_f()); }
  double g__gamZT_to_f_f(double z) { return 1; }
  double G__gamZT_to_f_f(double z) { return z; }
  double Ginv__gamZT_to_f_f(double G) { return G; }
  //
  //   photon/Z(T) -> t t  (coherent density matrix evolution)
  double CouplingPrefactor__gamZT_to_t_t() {
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    fill(_dPij_t_t.begin(),_dPij_t_t.end(),0.);  // initialize splitting matrix to zero
    Get_gamZT_to_f_f_dPijPrefactor( 2, 1,_Q2,_dPij_tR,NC);
    Get_gamZT_to_f_f_dPijPrefactor( 2,-1,_Q2,_dPij_tL,NC);
    AddTo( _dPij_t_t, _dPij_tR);
    AddTo( _dPij_t_t, _dPij_tL);
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_t_t)/Trace(_aVirtual->densityMatrix);
  }
  double M2__gamZT_to_t_t() { return M2__gamZT_to_f_f(); } 
  double f__gamZT_to_t_t(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> f f" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamZT_to_t_t()); }
  double g__gamZT_to_t_t(double z) { return g__gamZT_to_f_f(z); }
  double G__gamZT_to_t_t(double z) { return G__gamZT_to_f_f(z); }
  double Ginv__gamZT_to_t_t(double G) { return Ginv__gamZT_to_f_f(G); }
  //
  //   photon/Z(T) -> b b  (coherent density matrix evolution)
  double CouplingPrefactor__gamZT_to_b_b() {
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    fill(_dPij_b_b.begin(),_dPij_b_b.end(),0.);  // initialize splitting matrix to zero
    Get_gamZT_to_f_f_dPijPrefactor( 1, 1,_Q2,_dPij_bR,NC);
    Get_gamZT_to_f_f_dPijPrefactor( 1,-1,_Q2,_dPij_bL,NC);
    AddTo( _dPij_b_b, _dPij_bR);
    AddTo( _dPij_b_b, _dPij_bL);
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_b_b)/Trace(_aVirtual->densityMatrix);
  }
  double M2__gamZT_to_b_b() { return M2__gamZT_to_f_f(); } 
  double f__gamZT_to_b_b(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> f f" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamZT_to_b_b()); }
  double g__gamZT_to_b_b(double z) { return g__gamZT_to_f_f(z); }
  double G__gamZT_to_b_b(double z) { return G__gamZT_to_f_f(z); }
  double Ginv__gamZT_to_b_b(double G) { return Ginv__gamZT_to_f_f(G); }
  //
  //   photon/Z(T) -> Wlong Wlong  (coherent density matrix evolution)
  double CouplingPrefactor__gamZT_to_Wlong_Wlong() {
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    Get_gamZT_to_Wlong_Wlong_dPijPrefactor(_Q2,_dPij_Wlong_Wlong);
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_Wlong_Wlong)/Trace(_aVirtual->densityMatrix);
  }
  double M2__gamZT_to_Wlong_Wlong() { return M2__WT_to_H0_Wlong(); }
  double f__gamZT_to_Wlong_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> W(long) W(long)" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamZT_to_Wlong_Wlong()); }
  double g__gamZT_to_Wlong_Wlong(double z) { return g__WT_to_H0_Wlong(z); }
  double G__gamZT_to_Wlong_Wlong(double z) { return G__WT_to_H0_Wlong(z); }
  double Ginv__gamZT_to_Wlong_Wlong(double G) { return Ginv__WT_to_H0_Wlong(G); }
  //
  //   photon/Z(T) -> h Zlong  (coherent density matrix evolution)
  double CouplingPrefactor__gamZT_to_h_Zlong() {
    if (Trace(_aVirtual->densityMatrix)<=0)  return 0;
    Get_gamZT_to_h_Zlong_dPijPrefactor(_Q2,_dPij_h_Zlong);
    return MultiplyTrace(_aVirtual->densityMatrix,_dPij_h_Zlong)/Trace(_aVirtual->densityMatrix);
  }
  double M2__gamZT_to_h_Zlong() { return M2__WT_to_H0_Wlong(); }
  double f__gamZT_to_h_Zlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon/Z(T) -> W(long) W(long)" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamZT_to_h_Zlong()); }
  double g__gamZT_to_h_Zlong(double z) { return g__WT_to_H0_Wlong(z); }
  double G__gamZT_to_h_Zlong(double z) { return G__WT_to_H0_Wlong(z); }
  double Ginv__gamZT_to_h_Zlong(double G) { return Ginv__WT_to_H0_Wlong(G); }
  //
  //   photon -> f f   (Only valid for Q < mZ. Photon/Z coherent evolution switches over to this if we project out a photon.)
  double CouplingPrefactor__gamma_to_f_f() { return (3 + 2*NC*(4./9) + 2*NC*(1./9))*alphaEM/twoPi; }  // summing "massless" fermions (everything except bottom & top)
  double M2__gamma_to_f_f() { return _z*_zBar*(_z*_z+_zBar*_zBar)*_th2*_Ptot*_Ptot; } 
  double f__gamma_to_f_f(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon -> f f" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamma_to_f_f()); }
  double g__gamma_to_f_f(double z) { return 1; }
  double G__gamma_to_f_f(double z) { return z; }
  double Ginv__gamma_to_f_f(double G) { return G; }
  //
  //   photon -> b b   (Only valid for Q < mZ. Photon/Z convolved evolution switches over to this if we project out a photon.)
  double CouplingPrefactor__gamma_to_b_b() { return (NC*(1./9))*alphaEM/twoPi; }
  double M2__gamma_to_b_b() { return M2__gamma_to_f_f(); } 
  double f__gamma_to_b_b(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  photon -> b b" << endl; return 0; }
    return (_kinematicPrefactor*M2__gamma_to_b_b()); }
  double g__gamma_to_b_b(double z) { return g__gamma_to_f_f(z); }
  double G__gamma_to_b_b(double z) { return G__gamma_to_f_f(z); }
  double Ginv__gamma_to_b_b(double G) { return Ginv__gamma_to_f_f(G); }

  /////////  Scalar/longitudinal splittings  /////////
  //
  //   W(long) -> W(T) h/Z(long)
  double CouplingPrefactor__Wlong_to_WT_H0() { return QW2*alphaW/fourPi; } 
  double M2__Wlong_to_WT_H0() { return 2*_Ptot*_Ea*_zBar*(2*_zBar)*_th2; }  // summed over final +/-1 helicity
  double f__Wlong_to_WT_H0(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> W(T) h/Z(long)" << endl; return 0; }
    _PR_eval = 0.5 * _kinematicPrefactor*M2__Wlong_to_WT_H0();  _PL_eval = _PR_eval;  _Plong_eval = 0;
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__Wlong_to_WT_H0(double z) { return 4/z; }
  double G__Wlong_to_WT_H0(double z) { return 4*log(z); }
  double Ginv__Wlong_to_WT_H0(double G) { return exp(G/4); }
  //
  //   W(long) -> photon/Z(T) W(long)
  double CouplingPrefactor__Wlong_to_gamZT_Wlong() { return square(T3(_aVirtual)*gW/fourPi) + square(Y(_aVirtual)*gY/fourPi); } 
  double M2__Wlong_to_gamZT_Wlong() { return 2*_Ptot*_Ea*_zBar*(2*_zBar)*_th2; }  // summed over final +/-1 helicity
  double f__Wlong_to_gamZT_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> photon/Z(T) W(long)" << endl; return 0; }
    _PR_eval = 0.5 * _kinematicPrefactor*M2__Wlong_to_gamZT_Wlong();  _PL_eval = _PR_eval;  _Plong_eval = 0;
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__Wlong_to_gamZT_Wlong(double z) { return 4/z*_Q4overPropagator; }
  double G__Wlong_to_gamZT_Wlong(double z) { return 4*log(z)*_Q4overPropagator; }
  double Ginv__Wlong_to_gamZT_Wlong(double G) { return exp(G/4/_Q4overPropagator); }
  //
  //   W(long) -> t(R) b~(R)
  double CouplingPrefactor__Wlong_to_tR_bR() { return NC*square(ytop/fourPi); } 
  double M2__Wlong_to_tR_bR() { return _Ptot*_Ptot*_z*_zBar*(1)*_th2; }
  double f__Wlong_to_tR_bR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> t(R) b(R)" << endl; return 0; }
    return _kinematicPrefactor*M2__Wlong_to_tR_bR(); }
  double g__Wlong_to_tR_bR(double z) { return 2; }
  double G__Wlong_to_tR_bR(double z) { return 2*z; }
  double Ginv__Wlong_to_tR_bR(double G) { return G/2; }
  //
  //   W(long) -> Z(long) W(long)   [ultracollinear triple-longitudinal via gauge-Goldstone mixing individually on each leg]
  double CouplingPrefactor__Wlong_to_Zlong_Wlong() { return do_ultracollinear * alphaW/fourPi; } 
  double M2__Wlong_to_Zlong_Wlong() { return square( -QZ(_aVirtual)/cosThetaW*((1+_zBar)*2*_Ptot/(_Eb+_z*_Ptot))*mZ + sqrt(QW2/2)*((1+_z)*2*_Ptot/(_Ec+_zBar*_Ptot)-(_zBar-_z))*mW ); }
  double f__Wlong_to_Zlong_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> Z(long) W(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__Wlong_to_Zlong_Wlong(); }
  double g__Wlong_to_Zlong_Wlong(double z) {  return 4*max(square(QZ(_aVirtual)/cosThetaW),QW2/2) * 1./z/(1-z); }
  double G__Wlong_to_Zlong_Wlong(double z) {  return 4*max(square(QZ(_aVirtual)/cosThetaW),QW2/2) * (-2)*atanh(1-2*z); }
  double Ginv__Wlong_to_Zlong_Wlong(double G) { G /= 4*max(square(QZ(_aVirtual)/cosThetaW),QW2/2); return 0.5*(1-tanh(-G/2)); }
  //
  //   W(long) -> h W(long)   [ultracollinear via gauge-Goldstone mixing and Higgs cubic]
  double CouplingPrefactor__Wlong_to_h_Wlong() { return do_ultracollinear * alphaW/fourPi; } 
  double M2__Wlong_to_h_Wlong() { return square(lambdaHiggs*vev/2/gW + sqrt(QW2/2)*((1+_z)*2*_Ptot/(_Ec+_zBar*_Ptot) + (_zBar-_z))*mW ); }
  double f__Wlong_to_h_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> h W(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__Wlong_to_h_Wlong(); }
  double g__Wlong_to_h_Wlong(double z) {  return 4*QW2/2 * 1./z/(1-z); }
  double G__Wlong_to_h_Wlong(double z) {  return 4*QW2/2 * (-2)*atanh(1-2*z); }
  double Ginv__Wlong_to_h_Wlong(double G) { G /= 4*QW2/2; return 0.5*(1-tanh(-G/2)); }
  //
  //   W(long) -> photon/Z(T) W(T)  [ultracollinear via gauge-Goldstone mixing in initial state]
  double CouplingPrefactor__Wlong_to_gamZT_WT() { return do_ultracollinear * alphaW/fourPi; } 
  double M2__Wlong_to_gamZT_WT() { return 2*square(mW*(_z-_zBar)); }
  double f__Wlong_to_gamZT_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN W(long) -> photon/Z(T) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__Wlong_to_gamZT_WT(); }
  double g__Wlong_to_gamZT_WT(double z) { return 4*_Q4overPropagator; }
  double G__Wlong_to_gamZT_WT(double z) { return 4*z*_Q4overPropagator; }
  double Ginv__Wlong_to_gamZT_WT(double G) { return (G/4/_Q4overPropagator); }
  //
  //   W(long) -> t(L) b~(R)  [Ultracollinear, via broken Yukawa+gauge (interfering)]
  double CouplingPrefactor__Wlong_to_tL_bR() { return do_ultracollinear * 1./(16*PiSq); } 
  double M2__Wlong_to_tL_bR() { return  square(ytop*mtop/_z - 2*sqrt(QW2)*gW*mW) * _z*_zBar; }  // neglecting here bottom Yukawa
  double f__Wlong_to_tL_bR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN  W(long) -> t(L) b~(R)" << endl;  return 0; }
    return _kinematicPrefactor*M2__Wlong_to_tL_bR(); }
  double g__Wlong_to_tL_bR(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * (1/z) * _Q4overPropagator; }
  double G__Wlong_to_tL_bR(double z) { return 0.5*(ytop*ytop + 4*QW2*gW*gW) * log(z) * _Q4overPropagator; }
  double Ginv__Wlong_to_tL_bR(double G) { return exp(2*G / (ytop*ytop + 4*QW2*gW*gW) / (_Q4overPropagator)); }
  //
  //   h/Z(long) -> W(T) W(long)
  double CouplingPrefactor__H0_to_WT_Wlong() { return CouplingPrefactor__Wlong_to_WT_H0(); }
  double M2__H0_to_WT_Wlong() { return M2__Wlong_to_WT_H0(); }  // summed over final +/-1 helicity
  double f__H0_to_WT_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> W(T) W(long)" << endl; return 0; }
    _PR_eval = 0.5 * _kinematicPrefactor*M2__H0_to_WT_Wlong();  _PL_eval = _PR_eval;  _Plong_eval = 0;
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__H0_to_WT_Wlong(double z) { return g__Wlong_to_WT_H0(z); }
  double G__H0_to_WT_Wlong(double z) { return G__Wlong_to_WT_H0(z); }
  double Ginv__H0_to_WT_Wlong(double G) { return Ginv__Wlong_to_WT_H0(G); }
  //
  //   h/Z(long) -> photon/Z(T) h/Z(long)
  double CouplingPrefactor__H0_to_gamZT_H0() { return CouplingPrefactor__Wlong_to_gamZT_Wlong(); }
  double M2__H0_to_gamZT_H0() { return M2__Wlong_to_gamZT_Wlong(); }  // summed over final +/-1 helicity
  double f__H0_to_gamZT_H0(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> photon/Z(T) h/Z(long)" << endl; return 0; }
    _PR_eval = 0.5 * _kinematicPrefactor*M2__H0_to_gamZT_H0();  _PL_eval = _PR_eval;  _Plong_eval = 0;
    return _PR_eval+_PL_eval+_Plong_eval; }
  double g__H0_to_gamZT_H0(double z) { return 4/z*square(_Q2/(_Q2-max(m2(23),m2(25)))); }
  double G__H0_to_gamZT_H0(double z) { return 4*log(z)*square(_Q2/(_Q2-max(m2(23),m2(25)))); }
  double Ginv__H0_to_gamZT_H0(double G) { return exp(G/4/square(_Q2/(_Q2-max(m2(23),m2(25))))); }
  //
  //   h/Z(long) -> t(R) t~(R)
  double CouplingPrefactor__H0_to_tR_tR() { return CouplingPrefactor__Wlong_to_tR_bR(); }
  double M2__H0_to_tR_tR() { return M2__Wlong_to_tR_bR(); }
  double f__H0_to_tR_tR(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> t(R) t(R)" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_tR_tR(); }
  double g__H0_to_tR_tR(double z) { return g__Wlong_to_tR_bR(z); }
  double G__H0_to_tR_tR(double z) { return G__Wlong_to_tR_bR(z); }
  double Ginv__H0_to_tR_tR(double G) { return Ginv__Wlong_to_tR_bR(G); }
  //
  //   h/Z(long) -> W(long) W(long)
  double CouplingPrefactor__H0_to_Wlong_Wlong() { return do_ultracollinear * 1./(16*PiSq); }
  double M2__H0_to_Wlong_Wlong() {
    double amp_h =  sqrt(QW2/2)*gW*mW*(2*_Ptot/(_Eb+_z*_Ptot))*(1+_zBar) + sqrt(QW2/2)*gW*mW*(2*_Ptot/(_Ec+_zBar*_Ptot))*(1+_z) - lambdaHiggs*vev/2;
    double amp_Z =  sqrt(QW2/2)*gW*mW*(2*_Ptot/(_Eb+_z*_Ptot))*(1+_zBar) - sqrt(QW2/2)*gW*mW*(2*_Ptot/(_Ec+_zBar*_Ptot))*(1+_z) - QZ(_aVirtual)*gZ*mZ*(_z-_zBar);  // amp_phi, the pseuodscalar amp, is i times this in our conventions
    double tr = Trace(_aVirtual->densityMatrix);
    if (tr==0) return 0;
    return amp_h*amp_h*_aVirtual->densityMatrix[0]*square((_Q2-m2(2325))/(_Q2-m2(25)))/tr +
      amp_Z*amp_Z*_aVirtual->densityMatrix[1]*square((_Q2-m2(2325))/(_Q2-m2(23)))/tr +
      2*amp_h*amp_Z*_aVirtual->densityMatrix[3]*square((_Q2-m2(2325)))/(_Q2-m2(23))/(_Q2-m2(25))/tr; }
  double f__H0_to_Wlong_Wlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> W(long) W(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_Wlong_Wlong(); }
  double g__H0_to_Wlong_Wlong(double z) {  return 4*QW2*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))) * 1./z/(1-z); }
  double G__H0_to_Wlong_Wlong(double z) {  return 4*QW2*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))) * (-2)*atanh(1-2*z); }
  double Ginv__H0_to_Wlong_Wlong(double G) { G /= 4*QW2*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))); return 0.5*(1-tanh(-G/2)); }
  //
  //   h/Z(long) -> W(T) W(T)
  double CouplingPrefactor__H0_to_WT_WT() { return do_ultracollinear * 2./(16*PiSq); }  // factor of 2 here since we sum over W(+1) W(-1) and W(-1) W(+1) channels
  double M2__H0_to_WT_WT() {
    double amp_h =  (1./2)*gW*gW*vev;
    double amp_Z =  (1./2)*gW*gW*vev*(_z-_zBar);  // amp_phi, the pseuodscalar amp, is i times this in our conventions
    double tr = Trace(_aVirtual->densityMatrix);
    if (tr==0) return 0;
    return amp_h*amp_h*_aVirtual->densityMatrix[0]*square((_Q2-m2(2325))/(_Q2-m2(25)))/tr +
      amp_Z*amp_Z*_aVirtual->densityMatrix[1]*square((_Q2-m2(2325))/(_Q2-m2(23)))/tr +
      2*amp_h*amp_Z*_aVirtual->densityMatrix[3]*square((_Q2-m2(2325)))/(_Q2-m2(23))/(_Q2-m2(25))/tr; }
  double f__H0_to_WT_WT(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> W(T) W(T)" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_WT_WT(); }
  double g__H0_to_WT_WT(double z) {  return 4*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))); }
  double G__H0_to_WT_WT(double z) {  return 4*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))) * z; }
  double Ginv__H0_to_WT_WT(double G) { G /= 4*gW*gW*square((_Q2-m2(2325))/(_Q2-max(m2(23),m2(25)))); return G; }
  //
  //   h/Z(long) -> Z(long) Z(long)
  double CouplingPrefactor__H0_to_Zlong_Zlong() { return do_ultracollinear * 1./(16*PiSq) * (1./2); }  // Bose factor of 1/2
  double M2__H0_to_Zlong_Zlong() { return square( -QZ(_aVirtual)*gW/cosThetaW*mZ*((1+_zBar)*2*_Ptot/(_Eb+_z*_Ptot) + (1+_z)*2*_Ptot/(_Ec+_zBar*_Ptot))  - lambdaHiggs*vev/2 ); }     // only sourced by the h component (projection is done in EvolveParticle())
  double f__H0_to_Zlong_Zlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> Z(long) Z(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_Zlong_Zlong(); }
  double g__H0_to_Zlong_Zlong(double z) {  return 4*square(gW/cosThetaW) * 1./z/(1-z); }
  double G__H0_to_Zlong_Zlong(double z) {  return 4*square(gW/cosThetaW) * (-2)*atanh(1-2*z); }
  double Ginv__H0_to_Zlong_Zlong(double G) { G /= 4*square(gW/cosThetaW); return 0.5*(1-tanh(-G/2)); }
  //
  //   h/Z(long) -> h Z(long)
  double CouplingPrefactor__H0_to_h_Zlong() { return do_ultracollinear * 1./(16*PiSq); }
  double M2__H0_to_h_Zlong() { return square( -QZ(_aVirtual)*gW/cosThetaW*mZ*((1+_z)*2*_Ptot/(_Ec+_zBar*_Ptot) + (_zBar-_z)) + lambdaHiggs*vev/2 ); }     // only sourced by the Zlong component (projection is done in EvolveParticle())
  double f__H0_to_h_Zlong(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> h Z(long)" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_h_Zlong(); }
  double g__H0_to_h_Zlong(double z) {  return 4*square(gW/cosThetaW) * 1./z/(1-z); }
  double G__H0_to_h_Zlong(double z) {  return 4*square(gW/cosThetaW) * (-2)*atanh(1-2*z); }
  double Ginv__H0_to_h_Zlong(double G) { G /= 4*square(gW/cosThetaW); return 0.5*(1-tanh(-G/2)); }
  //
  //   h/Z(long) -> h h
  double CouplingPrefactor__H0_to_h_h() { return do_ultracollinear * (9./2*lambdaHiggs)/(16*PiSq) * (1./2); }  // Bose factor of 1/2
  double M2__H0_to_h_h() { return mh*mh; }   // only sourced by the h component
  double f__H0_to_h_h(double z) {
    SetKinematics(z);  if (_th != _th)  { cout << "WARNING: BAD THETA IN h/Z(long) -> h h" << endl; return 0; }
    return _kinematicPrefactor*M2__H0_to_h_h(); }
  double g__H0_to_h_h(double z) { return 2; }
  double G__H0_to_h_h(double z) { return 2*z; }
  double Ginv__H0_to_h_h(double G) { return G/2; }



////////////////////////////////////////////////////////////////////////////////
//
//                      EVOLUTION FUNCTIONS AND HELPERS
//
////////////////////////////////////////////////////////////////////////////////

// Evolve down a particle "a" in virtuality, with recoiler "r".  Return branching information.
//
Particle aTemp, rTemp;   // temp copies of "a" and "r" for tentative kinematic adjustments, defined globally to save processing time
Particle b_,c_;  // tentative daughters, also defined globally
vector<double> dPij_traceless(4,0.);  // traceless part of splitting matrix
vector<double> SudMatrix(4,0.);  // exponentiated splitting matrix, analog of sqrt(P_noBranch) for ordinary 1-component Sudakov/wavefunction evolution (keeping overall exp factor for validation purposes)
bool EvolveParticle(Particle & a, Particle aOriginal, Particle rOriginal, double & z, Particle & b, Particle & c, int & goodEvolve)
{
  if (a.id == 0) { return false; }  // id==0 => non-evolving spectator
  a.daughters.clear();  a.f__a_to_b_c = NULL;  a.a_to_V_c = false;  a.a_to_V_V = false;  // clear splitting info within this particle
  _aOnShell = &aOriginal;  _aSisterUnrecoiled = &rOriginal;  // the showering pair, as they were at their production
  double tMass = -1000;  double tMassPlusGammas = -1000;   if (m(a.id) > 0) { tMass = 2*log(m(a.id)/Qmin);  tMassPlusGammas = 2*log(mMax(a.id)/Qmin); }
    while (1) {
    aTemp.NewBasicCopy(&a);  rTemp.NewBasicCopy(&rOriginal);  // copies that will be used for tentative kinematic readjustments (keeping the originals kinematically unchanged)
    double this_dt = dt;
    if (a.GetQ() > 0 && m(a.id) > 0 && a.GetQ() > m(a.id))  this_dt *= square(1 - mRes(a.id)*mRes(a.id)/(a.GetQ()*a.GetQ()));  // step size becomes smaller as we approach a resonance
    if (this_dt < dtMin)  this_dt = dtMin;
    if (a.id==21 && g_to_g_g)  this_dt /= 4;  // for nonabelian gluon splittings, reduce step size (big rate!)
    double tNew = a.t - this_dt*randGen.Uniform();  // step size is quasi-random to prevent spurious discretizations
    //
    if (tNew <= 0 || tNew <= tMassPlusGammas || !a.canShower) {    // evolved below the virtuality cutoff or particle mass (+ a few natural widths), or is non-showering...set virtuality on-shell or prep for decay
      if (isResonance(a)) {
	if (a.id==2223) { double Pphoton = MultiplyTrace(a.densityMatrix,rhoPhoton)/Trace(a.densityMatrix);  if (randGen.Uniform() < Pphoton) a.id=22; else a.id=23; }  // project out from mixed photon/Z ~ B0/W0 state
	if (a.id==22) { a.t = 2*log(mMax(2223)/Qmin);  goodEvolve = 2;  return false; }  // Give photon virtuality just above Z resonance and start QED shower, subsequently allow the photon to possibly develop some small mass at the end of the shower, so that it can be "decayed"
	if (a.id==2325) { double PHiggs = a.densityMatrix[0]/Trace(a.densityMatrix);  if (randGen.Uniform() < PHiggs) a.id=25; else a.id=23; }  // project out from mixed Higgs/Zlong state
	double mNew2 = randGen.BreitWigner(square(m(a.id)),2*Gam(a.id)*m(a.id));  // ROOT Breit-Wigner is ~ 1/((x-mean)*(x-mean) + gamma*gamma/4)
	if (mNew2 < square(mMin(a.id)) || mNew2 > square(mMax(a.id)))  { goodEvolve = false;  return false;  }  // veto if outside Breit-Wigner window (region above will be filled by shower)
	if (mNew2 > min(square(a.v.E()),square(a.GetQ())))  { goodEvolve = false;  return false;  }  // veto if kinematically inaccessible, or unreachable at this Q scale
	double BWweightMax = BWweight(a.id,mMax(a.id));
	for (int it=0; it<1001; it++) {  // improve BW model by weighting with Gamma(Q)/Gamma(m)
	  if (it==1000) { cout << "WARNING: failed to throw weighted Breit-Wigner mass after 1000 tries" << endl;  break; }
	  double mNewNew2 = randGen.BreitWigner(square(m(a.id)),2*Gam(a.id)*m(a.id));
	  if (mNewNew2 < square(mMin(a.id)) || mNewNew2 > square(mMax(a.id)) || mNewNew2 > min(square(a.v.E()),square(a.GetQ()))) continue;  // we're redistributing, not vetoing, so just try again
	  double r = randGen.Uniform();
	  if (r < BWweight(a.id,sqrt(mNewNew2))/BWweightMax)  { mNew2 = mNewNew2;  break; }
	}
	a.t = log(mNew2/Qmin/Qmin);
	aTemp.id = a.id;  aTemp.t = a.t;  // update copy
	ResetMasses(aTemp,rTemp,a.GetQ(),rTemp.M());  // set to tentative virtual mass (recoiler mass is kept fixed...it may later be evolved in a separate EvolveParticle call with a's mass fixed)
	if (aTemp.P() != aTemp.P() || rTemp.P() != rTemp.P()) { goodEvolve = false; return false; }  // avoid roundoff errors at borders of allowed phase space
	InitializeMotherVariables(aTemp,rTemp);  // set up mother/recoiler kinematics and global variables
	double productionCorrection = 1;  if (applyProductionCorrectionsResonance)  productionCorrection = ComputeProductionCorrection();  // account for changes to this particle's production probability now that it's gone off-shell
	if (randGen.Uniform() > productionCorrection) { goodEvolve = false;  return false; }   // veto due to reduced production probability in mother splitting
      }
      else {
	a.t = tMass;
      }
      return false;
    }
    //
    double tStep = a.t-tNew;
    a.t = tNew;  aTemp.t = tNew;
    if (a.GetQ()+rOriginal.M() > (a.v+rOriginal.v).M())  continue; // a would have more mass than the entire a+r system...evolve down
    ResetMasses(aTemp,rTemp,a.GetQ(),rTemp.M());  // set to tentative virtual mass (recoiler mass is kept fixed...it may later be evolved in a separate EvolveParticle call with a's mass fixed)
    if (aTemp.P() != aTemp.P() || rTemp.P() != rTemp.P())  continue;  // avoid roundoff errors at borders of allowed phase space
    InitializeMotherVariables(aTemp,rTemp);  // set up mother/recoiler kinematics and global variables
    double I = 0;  // integral of splitting probabilities over z at this virtuality (to be exponentiated, times -tStep)
    pair<double,double> zLim;
    double (*f__a_to_b_c)(double);  double (*g__a_to_b_c)(double);  double (*G__a_to_b_c)(double);  double (*Ginv__a_to_b_c)(double);  // pointers to splitting function and over-estimators
    bool a_to_V_c = false;  // flag to allow us to throw a final vector polarization after a->Vc branching, with c polarization fixed or random
    bool a_to_V_V = false;  // ditto, with c also a vector
    //
    // Gluon branchings
    //
    if (isGluon(a)) {
      pair<double,double> zLim_00(-1,-1);  if(g_to_g_g || g_to_q_q) zLim_00 = zMinMax(aTemp.v,mMassless,mMassless);
      pair<double,double> zLim_tt(-1,-1);  if(g_to_t_t || g_to_tRL_tRL) zLim_tt = zMinMax(aTemp.v,mtop,mtop);
      pair<double,double> zLim_bb(-1,-1);  if(g_to_b_b || g_to_bRL_bRL) zLim_bb = zMinMax(aTemp.v,mbottom,mbottom);
      double I__g_to_g_g = 0;  if (g_to_g_g && zLim_00.first >= 0 && zLim_00.second <= 1) { I__g_to_g_g = CouplingPrefactor__g_to_g_g() * (G__g_to_g_g(zLim_00.second) - G__g_to_g_g(zLim_00.first));  I += I__g_to_g_g; }
      double I__g_to_q_q = 0;  if (g_to_q_q && zLim_00.first >= 0 && zLim_00.second <= 1) { I__g_to_q_q = CouplingPrefactor__g_to_q_q() * (G__g_to_q_q(zLim_00.second) - G__g_to_q_q(zLim_00.first));  I += I__g_to_q_q; }
      double I__g_to_t_t = 0;  if (g_to_t_t && zLim_tt.first >= 0 && zLim_tt.second <= 1) { I__g_to_t_t = CouplingPrefactor__g_to_t_t() * (G__g_to_q_q(zLim_tt.second) - G__g_to_q_q(zLim_tt.first));  I += I__g_to_t_t; }
      double I__g_to_b_b = 0;  if (g_to_b_b && zLim_bb.first >= 0 && zLim_bb.second <= 1) { I__g_to_b_b = CouplingPrefactor__g_to_b_b() * (G__g_to_q_q(zLim_bb.second) - G__g_to_q_q(zLim_bb.first));  I += I__g_to_b_b; }
      double I__g_to_tRL_tRL = 0;  if (g_to_tRL_tRL && zLim_tt.first >= 0 && zLim_tt.second <= 1) { I__g_to_tRL_tRL = CouplingPrefactor__g_to_tRL_tRL() * (G__g_to_tRL_tRL(zLim_tt.second) - G__g_to_tRL_tRL(zLim_tt.first));  I += I__g_to_tRL_tRL; }
      double I__g_to_bRL_bRL = 0;  if (g_to_bRL_bRL && zLim_bb.first >= 0 && zLim_bb.second <= 1) { I__g_to_bRL_bRL = CouplingPrefactor__g_to_bRL_bRL() * (G__g_to_bRL_bRL(zLim_bb.second) - G__g_to_bRL_bRL(zLim_bb.first));  I += I__g_to_bRL_bRL; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__g_to_g_g)  && rnd < IrunningSum) {    //  g -> g g   QCD
	b_.id = 21;  c_.id = 21;
	b_.helicity = randPlusMinus1();  c_.helicity = randPlusMinus1();
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__g_to_g_g;  g__a_to_b_c = &g__g_to_g_g;  G__a_to_b_c = &G__g_to_g_g;  Ginv__a_to_b_c = &Ginv__g_to_g_g;  
      }
      else if ((IrunningSum += I__g_to_q_q)  && rnd < IrunningSum) {  // g -> q q~   QCD (light flavors)
	b_.id = randGen.Integer(NF)+1;  c_.id = -b_.id;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__g_to_q_q;  g__a_to_b_c = &g__g_to_q_q;  G__a_to_b_c = &G__g_to_q_q;  Ginv__a_to_b_c = &Ginv__g_to_q_q; 
      }
      else if ((IrunningSum += I__g_to_t_t)  && rnd < IrunningSum) {  // g -> t t~   QCD
	b_.id = 6;  c_.id = -b_.id;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	zLim.first = zLim_tt.first;  zLim.second = zLim_tt.second;
	f__a_to_b_c = &f__g_to_q_q;  g__a_to_b_c = &g__g_to_q_q;  G__a_to_b_c = &G__g_to_q_q;  Ginv__a_to_b_c = &Ginv__g_to_q_q; 
      }
      else if ((IrunningSum += I__g_to_b_b)  && rnd < IrunningSum) {  // g -> b b~   QCD
	b_.id = 5;  c_.id = -b_.id;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	zLim.first = zLim_bb.first;  zLim.second = zLim_bb.second;
	f__a_to_b_c = &f__g_to_q_q;  g__a_to_b_c = &g__g_to_q_q;  G__a_to_b_c = &G__g_to_q_q;  Ginv__a_to_b_c = &Ginv__g_to_q_q; 
      }
      else if ((IrunningSum += I__g_to_tRL_tRL)  && rnd < IrunningSum) {  // g -> tRL tRL~   QCD ultracollinear
	b_.id = 6;  c_.id = -b_.id;
	b_.helicity = randPlusMinus1();  c_.helicity = b_.helicity;
	zLim.first = zLim_tt.first;  zLim.second = zLim_tt.second;
	f__a_to_b_c = &f__g_to_tRL_tRL;  g__a_to_b_c = &g__g_to_tRL_tRL;  G__a_to_b_c = &G__g_to_tRL_tRL;  Ginv__a_to_b_c = &Ginv__g_to_tRL_tRL; 
      }
      else if ((IrunningSum += I__g_to_bRL_bRL)  && rnd < IrunningSum) {  // g -> bRL bRL~   QCD ultracollinear
	b_.id = 5;  c_.id = -b_.id;
	b_.helicity = randPlusMinus1();  c_.helicity = b_.helicity;
	zLim.first = zLim_bb.first;  zLim.second = zLim_bb.second;
	f__a_to_b_c = &f__g_to_bRL_bRL;  g__a_to_b_c = &g__g_to_bRL_bRL;  G__a_to_b_c = &G__g_to_bRL_bRL;  Ginv__a_to_b_c = &Ginv__g_to_bRL_bRL; 
      }
    }
    //
    // Light fermion branchings
    //
    if (isLightFermion(a)) {
      pair<double,double> zLim_00(-1,-1);  if(q_to_g_q || f_to_gamZT_f) zLim_00 = zMinMax(aTemp.v,mMassless,mMassless);
      pair<double,double> zLim_W0(-1,-1);  if(f_to_W_f) zLim_W0 = zMinMax(aTemp.v,mW,mMassless);
      pair<double,double> zLim_Z0(-1,-1);  if(f_to_ZT_f || f_to_Zlong_f) zLim_Z0 = zMinMax(aTemp.v,mZ,mMassless);
      bool downType = abs(a.id)%2;
      bool upType = !downType;
      double I__q_to_g_q = 0;  if (q_to_g_q && isLightQuark(a)) { I__q_to_g_q = CouplingPrefactor__q_to_g_q() * (G__q_to_g_q(zLim_00.second) - G__q_to_g_q(zLim_00.first));  I += I__q_to_g_q; }
      double I__f_to_W_f = 0;  if (f_to_W_f && a.chirality() < 0 && zLim_W0.first >= 0 && zLim_W0.second <= 1) { I__f_to_W_f = CouplingPrefactor__f_to_W_f() * (G__f_to_W_f(zLim_W0.second) - G__f_to_W_f(zLim_W0.first));  I += I__f_to_W_f; }
      double I__f_to_gamZT_f = 0;  if (f_to_gamZT_f && zLim_00.first >= 0 && zLim_00.second <= 1) { I__f_to_gamZT_f = CouplingPrefactor__f_to_gamZT_f() * (G__f_to_gamZT_f(zLim_00.second) - G__f_to_gamZT_f(zLim_00.first));  I += I__f_to_gamZT_f; }
      double I__f_to_ZT_f = 0;  if (f_to_ZT_f && zLim_Z0.first >= 0 && zLim_Z0.second <= 1) { I__f_to_ZT_f = CouplingPrefactor__f_to_ZT_f() * (G__f_to_ZT_f(zLim_Z0.second) - G__f_to_ZT_f(zLim_Z0.first));  I += I__f_to_ZT_f; }
      double I__f_to_Zlong_f = 0;  if (f_to_Zlong_f && zLim_Z0.first >= 0 && zLim_Z0.second <= 1) { I__f_to_Zlong_f = CouplingPrefactor__f_to_Zlong_f() * (G__f_to_Zlong_f(zLim_Z0.second) - G__f_to_Zlong_f(zLim_Z0.first));  I += I__f_to_Zlong_f; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__q_to_g_q)  && rnd < IrunningSum) {  //  q -> g q  (QCD)
	b_.id = 21;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__q_to_g_q;  g__a_to_b_c = &g__q_to_g_q;  G__a_to_b_c = &G__q_to_g_q;  Ginv__a_to_b_c = &Ginv__q_to_g_q;  
      }
      else if ((IrunningSum += I__f_to_W_f)  && rnd < IrunningSum) {  // f -> W(T/long) f'
	a_to_V_c = true;
	b_.id = a.signid()*(upType-downType)*24;  c_.id = upType*a.signid()*(abs(a.id)-1) + downType*a.signid()*(abs(a.id)+1);
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the W helicity +/-1
	zLim.first = zLim_W0.first;  zLim.second = zLim_W0.second;
	f__a_to_b_c = &f__f_to_W_f;  g__a_to_b_c = &g__f_to_W_f;  G__a_to_b_c = &G__f_to_W_f;  Ginv__a_to_b_c = &Ginv__f_to_W_f;
      }
      else if ((IrunningSum += I__f_to_gamZT_f)  && rnd < IrunningSum) {  // f -> photon/Z(T) f
	a_to_V_c = true;
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the Z helicity +/-1
	b_.densityMatrix[0] = square(gY*Y(&a));  b_.densityMatrix[1] = square(gW*T3(&a));  b_.densityMatrix[2] = gY*gW*Y(&a)*T3(&a);  b_.densityMatrix[3] = 0; 
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__f_to_gamZT_f;  g__a_to_b_c = &g__f_to_gamZT_f;  G__a_to_b_c = &G__f_to_gamZT_f;  Ginv__a_to_b_c = &Ginv__f_to_gamZT_f;
      }
      else if ((IrunningSum += I__f_to_ZT_f)  && rnd < IrunningSum) {  // f -> Z(T) f
	a_to_V_c = true;
	b_.id = 23;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the Z helicity +/-1
	zLim.first = zLim_Z0.first;  zLim.second = zLim_Z0.second;
	f__a_to_b_c = &f__f_to_ZT_f;  g__a_to_b_c = &g__f_to_ZT_f;  G__a_to_b_c = &G__f_to_ZT_f;  Ginv__a_to_b_c = &Ginv__f_to_ZT_f;
      }
      else if ((IrunningSum += I__f_to_Zlong_f)  && rnd < IrunningSum) {  // f -> Z(long) f  ultracollinear
	b_.id = 23;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Z0.first;  zLim.second = zLim_Z0.second;
	f__a_to_b_c = &f__f_to_Zlong_f;  g__a_to_b_c = &g__f_to_Zlong_f;  G__a_to_b_c = &G__f_to_Zlong_f;  Ginv__a_to_b_c = &Ginv__f_to_Zlong_f;
      }
    }
    //
    // Top quark branchings
    //
    if (isTopQuark(a)) {
      pair<double,double> zLim_Wb(-1,-1);  if(tL_to_WT_bL || tL_to_Wlong_bL || tR_to_WT_bL || tRL_to_Wlong_bLR) zLim_Wb = zMinMax(aTemp.v,mW,mbottom);
      pair<double,double> zLim_Zt(-1,-1);  if(tRL_to_ZT_tRL || tRL_to_Zlong_tRL || tRL_to_H0_tLR) zLim_Zt = zMinMax(aTemp.v,mZ,mtop);
      pair<double,double> zLim_ht(-1,-1);  if(tRL_to_h_tRL) zLim_ht = zMinMax(aTemp.v,mh,mtop);
      pair<double,double> zLim_0t(-1,-1);  if(tRL_to_gamZT_tRL || t_to_g_t) zLim_0t = zMinMax(aTemp.v,mMassless,mtop);
      double I__tL_to_WT_bL = 0;  if (tL_to_WT_bL && a.chirality() < 0 && zLim_Wb.first >= 0 && zLim_Wb.second <= 1) { I__tL_to_WT_bL = CouplingPrefactor__tL_to_WT_bL() * (G__tL_to_WT_bL(zLim_Wb.second) - G__tL_to_WT_bL(zLim_Wb.first));  I += I__tL_to_WT_bL; }
      double I__tL_to_Wlong_bL = 0;  if (tL_to_Wlong_bL && a.chirality() < 0 && zLim_Wb.first >= 0 && zLim_Wb.second <= 1) { I__tL_to_Wlong_bL = CouplingPrefactor__tL_to_Wlong_bL() * (G__tL_to_Wlong_bL(zLim_Wb.second) - G__tL_to_Wlong_bL(zLim_Wb.first));  I += I__tL_to_Wlong_bL; }
      double I__tR_to_WT_bL = 0;  if (tR_to_WT_bL && a.chirality() > 0 && zLim_Wb.first >= 0 && zLim_Wb.second <= 1) { I__tR_to_WT_bL = CouplingPrefactor__tR_to_WT_bL() * (G__tR_to_WT_bL(zLim_Wb.second) - G__tR_to_WT_bL(zLim_Wb.first));  I += I__tR_to_WT_bL; }
      double I__tRL_to_Wlong_bLR = 0;  if (tRL_to_Wlong_bLR && zLim_Wb.first >= 0 && zLim_Wb.second <= 1) { I__tRL_to_Wlong_bLR = CouplingPrefactor__tRL_to_Wlong_bLR() * (G__tRL_to_Wlong_bLR(zLim_Wb.second) - G__tRL_to_Wlong_bLR(zLim_Wb.first));  I += I__tRL_to_Wlong_bLR; }
      double I__tRL_to_gamZT_tRL = 0;  if (tRL_to_gamZT_tRL && zLim_0t.first >= 0 && zLim_0t.second <= 1) { I__tRL_to_gamZT_tRL = CouplingPrefactor__tRL_to_gamZT_tRL() * (G__tRL_to_gamZT_tRL(zLim_0t.second) - G__tRL_to_gamZT_tRL(zLim_0t.first));  I += I__tRL_to_gamZT_tRL; }
      double I__tRL_to_ZT_tRL = 0;  if (tRL_to_ZT_tRL && zLim_Zt.first >= 0 && zLim_Zt.second <= 1) { I__tRL_to_ZT_tRL = CouplingPrefactor__tRL_to_ZT_tRL() * (G__tRL_to_ZT_tRL(zLim_Zt.second) - G__tRL_to_ZT_tRL(zLim_Zt.first));  I += I__tRL_to_ZT_tRL; }
      double I__tRL_to_Zlong_tRL = 0;  if (tRL_to_Zlong_tRL && zLim_Zt.first >= 0 && zLim_Zt.second <= 1) { I__tRL_to_Zlong_tRL = CouplingPrefactor__tRL_to_Zlong_tRL() * (G__tRL_to_Zlong_tRL(zLim_Zt.second) - G__tRL_to_Zlong_tRL(zLim_Zt.first));  I += I__tRL_to_Zlong_tRL; }
      double I__tRL_to_h_tRL = 0;  if (tRL_to_h_tRL && zLim_ht.first >= 0 && zLim_ht.second <= 1) { I__tRL_to_h_tRL = CouplingPrefactor__tRL_to_h_tRL() * (G__tRL_to_h_tRL(zLim_ht.second) - G__tRL_to_h_tRL(zLim_ht.first));  I += I__tRL_to_h_tRL; }
      double I__tRL_to_H0_tLR = 0;  if (tRL_to_H0_tLR && zLim_Zt.first >= 0 && zLim_Zt.second <= 1) { I__tRL_to_H0_tLR = CouplingPrefactor__tRL_to_H0_tLR() * (G__tRL_to_H0_tLR(zLim_Zt.second) - G__tRL_to_H0_tLR(zLim_Zt.first));  I += I__tRL_to_H0_tLR; }
      double I__t_to_g_t = 0;  if (t_to_g_t && zLim_0t.first >= 0 && zLim_0t.second <= 1) { I__t_to_g_t = CouplingPrefactor__q_to_g_q() * (G__q_to_g_q(zLim_0t.second) - G__q_to_g_q(zLim_0t.first));  I += I__t_to_g_t; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__tL_to_WT_bL)  && rnd < IrunningSum) {  // t(L) -> W(T) b(L) via gauge
	a_to_V_c = true;
	b_.id = a.signid()*24;  c_.id = a.signid()*5;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the W helicity +/-1
	zLim.first = zLim_Wb.first;  zLim.second = zLim_Wb.second;
	f__a_to_b_c = &f__tL_to_WT_bL;  g__a_to_b_c = &g__tL_to_WT_bL;  G__a_to_b_c = &G__tL_to_WT_bL;  Ginv__a_to_b_c = &Ginv__tL_to_WT_bL;
      }
      else if ((IrunningSum += I__tL_to_Wlong_bL)  && rnd < IrunningSum) {   // t(L) -> W(long) b(L) via broken guage+Yukawa
	b_.id = a.signid()*24;  c_.id = a.signid()*5;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Wb.first;  zLim.second = zLim_Wb.second;
	f__a_to_b_c = &f__tL_to_Wlong_bL;  g__a_to_b_c = &g__tL_to_Wlong_bL;  G__a_to_b_c = &G__tL_to_Wlong_bL;  Ginv__a_to_b_c = &Ginv__tL_to_Wlong_bL;
      }
      else if ((IrunningSum += I__tR_to_WT_bL)  && rnd < IrunningSum) {  //  t(R) -> W(T) b(L) via broken top spin-flip
	a_to_V_c = true;
	b_.id = a.signid()*24;  c_.id = a.signid()*5;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the W same helicity orientation as top
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Wb.first;  zLim.second = zLim_Wb.second;
	f__a_to_b_c = &f__tR_to_WT_bL;  g__a_to_b_c = &g__tR_to_WT_bL;  G__a_to_b_c = &G__tR_to_WT_bL;  Ginv__a_to_b_c = &Ginv__tR_to_WT_bL;
      }
      else if ((IrunningSum += I__tRL_to_Wlong_bLR)  && rnd < IrunningSum) {  // t(R/L) -> W(long) b(L/R) via Yukawa (unbroken helicity-flipping vertex)
	b_.id = a.signid()*24;  c_.id = a.signid()*5;
	b_.helicity = 0;  c_.helicity = a.helicity*(-1);
	zLim.first = zLim_Wb.first;  zLim.second = zLim_Wb.second;
	f__a_to_b_c = &f__tRL_to_Wlong_bLR;  g__a_to_b_c = &g__tRL_to_Wlong_bLR;  G__a_to_b_c = &G__tRL_to_Wlong_bLR;  Ginv__a_to_b_c = &Ginv__tRL_to_Wlong_bLR;
      }
      else if ((IrunningSum += I__tRL_to_gamZT_tRL)  && rnd < IrunningSum) {  // t(R/L) -> photon/Z(T) t(R/L) via gauge
	a_to_V_c = true;
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the photon/Z helicity +/-1	
	b_.densityMatrix[0] = square(gY*Y(&a));  b_.densityMatrix[1] = square(gW*T3(&a));  b_.densityMatrix[2] = gY*gW*Y(&a)*T3(&a);  b_.densityMatrix[3] = 0; 
	zLim.first = zLim_0t.first;  zLim.second = zLim_0t.second;
	f__a_to_b_c = &f__tRL_to_gamZT_tRL;  g__a_to_b_c = &g__tRL_to_gamZT_tRL;  G__a_to_b_c = &G__tRL_to_gamZT_tRL;  Ginv__a_to_b_c = &Ginv__tRL_to_gamZT_tRL;
      }
      else if ((IrunningSum += I__tRL_to_ZT_tRL)  && rnd < IrunningSum) {  // t(R/L) -> Z(T) t(R/L) via gauge
	a_to_V_c = true;
	b_.id = 23;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the Z helicity +/-1
	zLim.first = zLim_Zt.first;  zLim.second = zLim_Zt.second;
	f__a_to_b_c = &f__tRL_to_ZT_tRL;  g__a_to_b_c = &g__tRL_to_ZT_tRL;  G__a_to_b_c = &G__tRL_to_ZT_tRL;  Ginv__a_to_b_c = &Ginv__tRL_to_ZT_tRL;
      }
      else if ((IrunningSum += I__tRL_to_Zlong_tRL)  && rnd < IrunningSum) {  // t(R/L) -> Z(long) t(R/L) via broken gauge+Yukawa
	b_.id = 23;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Zt.first;  zLim.second = zLim_Zt.second;
	f__a_to_b_c = &f__tRL_to_Zlong_tRL;  g__a_to_b_c = &g__tRL_to_Zlong_tRL;  G__a_to_b_c = &G__tRL_to_Zlong_tRL;  Ginv__a_to_b_c = &Ginv__tRL_to_Zlong_tRL;
      }
      else if ((IrunningSum += I__tRL_to_h_tRL)  && rnd < IrunningSum) {  // t(R/L) -> h t(R/L) via broken Yukawa
	b_.id = 25;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_ht.first;  zLim.second = zLim_ht.second;
	f__a_to_b_c = &f__tRL_to_h_tRL;  g__a_to_b_c = &g__tRL_to_h_tRL;  G__a_to_b_c = &G__tRL_to_h_tRL;  Ginv__a_to_b_c = &Ginv__tRL_to_h_tRL;
      }
      else if ((IrunningSum += I__tRL_to_H0_tLR)  && rnd < IrunningSum) {  // t(R/L) -> h/Z(long) t(L/R) via Yukawa (unbroken helicity-flipping vertex)
	b_.id = 2325;  c_.id = a.signid()*6;
	b_.helicity = 0;  c_.helicity = a.helicity*(-1);
	if (a.chirality()>0)  b_.densityMatrix = rhoH0;  else  b_.densityMatrix = rhoH0star;
	zLim.first = zLim_Zt.first;  zLim.second = zLim_Zt.second;
	f__a_to_b_c = &f__tRL_to_H0_tLR;  g__a_to_b_c = &g__tRL_to_H0_tLR;  G__a_to_b_c = &G__tRL_to_H0_tLR;  Ginv__a_to_b_c = &Ginv__tRL_to_H0_tLR;
      }
      else if ((IrunningSum += I__t_to_g_t)  && rnd < IrunningSum) {  //  t -> g t  (QCD)
	b_.id = 21;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;
	zLim.first = zLim_0t.first;  zLim.second = zLim_0t.second;
	f__a_to_b_c = &f__q_to_g_q;  g__a_to_b_c = &g__q_to_g_q;  G__a_to_b_c = &G__q_to_g_q;  Ginv__a_to_b_c = &Ginv__q_to_g_q;  
      }
    }
    //
    // Bottom quark branchings
    //
    if (isBottomQuark(a)) {
      pair<double,double> zLim_Wt(-1,-1);  if(bL_to_WT_tL || bL_to_Wlong_tL || bL_to_WT_tR || bLR_to_Wlong_tRL) zLim_Wt = zMinMax(aTemp.v,mW,mtop);
      pair<double,double> zLim_Zb(-1,-1);  if(bRL_to_Zlong_bRL) zLim_Zb = zMinMax(aTemp.v,mZ,mbottom);
      pair<double,double> zLim_0b(-1,-1);  if(bRL_to_gamZT_bRL || b_to_g_b) zLim_0b = zMinMax(aTemp.v,mMassless,mbottom);
      double I__bL_to_WT_tL = 0;  if (bL_to_WT_tL && a.chirality() < 0 && zLim_Wt.first >= 0 && zLim_Wt.second <= 1) { I__bL_to_WT_tL = CouplingPrefactor__bL_to_WT_tL() * (G__bL_to_WT_tL(zLim_Wt.second) - G__bL_to_WT_tL(zLim_Wt.first));  I += I__bL_to_WT_tL; }
      double I__bL_to_Wlong_tL = 0;  if (bL_to_Wlong_tL && a.chirality() < 0 && zLim_Wt.first >= 0 && zLim_Wt.second <= 1) { I__bL_to_Wlong_tL = CouplingPrefactor__bL_to_Wlong_tL() * (G__bL_to_Wlong_tL(zLim_Wt.second) - G__bL_to_Wlong_tL(zLim_Wt.first));  I += I__bL_to_Wlong_tL; }
      double I__bL_to_WT_tR = 0;  if (bL_to_WT_tR && a.chirality() > 0 && zLim_Wt.first >= 0 && zLim_Wt.second <= 1) { I__bL_to_WT_tR = CouplingPrefactor__bL_to_WT_tR() * (G__bL_to_WT_tR(zLim_Wt.second) - G__bL_to_WT_tR(zLim_Wt.first));  I += I__bL_to_WT_tR; }
      double I__bLR_to_Wlong_tRL = 0;  if (bLR_to_Wlong_tRL && zLim_Wt.first >= 0 && zLim_Wt.second <= 1) { I__bLR_to_Wlong_tRL = CouplingPrefactor__bLR_to_Wlong_tRL() * (G__bLR_to_Wlong_tRL(zLim_Wt.second) - G__bLR_to_Wlong_tRL(zLim_Wt.first));  I += I__bLR_to_Wlong_tRL; }
      double I__bRL_to_gamZT_bRL = 0;  if (bRL_to_gamZT_bRL && zLim_0b.first >= 0 && zLim_0b.second <= 1) { I__bRL_to_gamZT_bRL = CouplingPrefactor__bRL_to_gamZT_bRL() * (G__bRL_to_gamZT_bRL(zLim_0b.second) - G__bRL_to_gamZT_bRL(zLim_0b.first));  I += I__bRL_to_gamZT_bRL; }
      double I__bRL_to_Zlong_bRL = 0;  if (bRL_to_Zlong_bRL && zLim_Zb.first >= 0 && zLim_Zb.second <= 1) { I__bRL_to_Zlong_bRL = CouplingPrefactor__bRL_to_Zlong_bRL() * (G__bRL_to_Zlong_bRL(zLim_Zb.second) - G__bRL_to_Zlong_bRL(zLim_Zb.first));  I += I__bRL_to_Zlong_bRL; }
      double I__b_to_g_b = 0;  if (b_to_g_b && zLim_0b.first >= 0 && zLim_0b.second <= 1) { I__b_to_g_b = CouplingPrefactor__q_to_g_q() * (G__q_to_g_q(zLim_0b.second) - G__q_to_g_q(zLim_0b.first));  I += I__b_to_g_b; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__bL_to_WT_tL)  && rnd < IrunningSum) {  // t(L) -> W(T) b(L) via gauge
	a_to_V_c = true;
	b_.id = a.signid()*(-24);  c_.id = a.signid()*6;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the W helicity +/-1
	zLim.first = zLim_Wt.first;  zLim.second = zLim_Wt.second;
	f__a_to_b_c = &f__bL_to_WT_tL;  g__a_to_b_c = &g__bL_to_WT_tL;  G__a_to_b_c = &G__bL_to_WT_tL;  Ginv__a_to_b_c = &Ginv__bL_to_WT_tL;
      }
      else if ((IrunningSum += I__bL_to_Wlong_tL)  && rnd < IrunningSum) {   // t(L) -> W(long) b(L) via broken guage+Yukawa
	b_.id = a.signid()*(-24);  c_.id = a.signid()*6;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Wt.first;  zLim.second = zLim_Wt.second;
	f__a_to_b_c = &f__bL_to_Wlong_tL;  g__a_to_b_c = &g__bL_to_Wlong_tL;  G__a_to_b_c = &G__bL_to_Wlong_tL;  Ginv__a_to_b_c = &Ginv__bL_to_Wlong_tL;
      }
      else if ((IrunningSum += I__bL_to_WT_tR)  && rnd < IrunningSum) {  //  t(R) -> W(T) b(L) via broken top spin-flip
	a_to_V_c = true;
	b_.id = a.signid()*(-24);  c_.id = a.signid()*6;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the W random helicity
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Wt.first;  zLim.second = zLim_Wt.second;
	f__a_to_b_c = &f__bL_to_WT_tR;  g__a_to_b_c = &g__bL_to_WT_tR;  G__a_to_b_c = &G__bL_to_WT_tR;  Ginv__a_to_b_c = &Ginv__bL_to_WT_tR;
      }
      else if ((IrunningSum += I__bLR_to_Wlong_tRL)  && rnd < IrunningSum) {  // b(L/R) -> W(long) t(R/L) via Yukawa (unbroken helicity-flipping vertex)
	b_.id = a.signid()*(-24);  c_.id = a.signid()*6;
	b_.helicity = 0;  c_.helicity = a.helicity*(-1);
	zLim.first = zLim_Wt.first;  zLim.second = zLim_Wt.second;
	f__a_to_b_c = &f__bLR_to_Wlong_tRL;  g__a_to_b_c = &g__bLR_to_Wlong_tRL;  G__a_to_b_c = &G__bLR_to_Wlong_tRL;  Ginv__a_to_b_c = &Ginv__bLR_to_Wlong_tRL;
      }
      else if ((IrunningSum += I__bRL_to_gamZT_bRL)  && rnd < IrunningSum) {  // b(R/L) -> photon/Z(T) b(R/L) via gauge
	a_to_V_c = true;
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;   // provisionally assign the photon/Z helicity +/-1	
	b_.densityMatrix[0] = square(gY*Y(&a));  b_.densityMatrix[1] = square(gW*T3(&a));  b_.densityMatrix[2] = gY*gW*Y(&a)*T3(&a);  b_.densityMatrix[3] = 0; 
	zLim.first = zLim_0b.first;  zLim.second = zLim_0b.second;
	f__a_to_b_c = &f__bRL_to_gamZT_bRL;  g__a_to_b_c = &g__bRL_to_gamZT_bRL;  G__a_to_b_c = &G__bRL_to_gamZT_bRL;  Ginv__a_to_b_c = &Ginv__bRL_to_gamZT_bRL;
      }
      else if ((IrunningSum += I__bRL_to_Zlong_bRL)  && rnd < IrunningSum) {  // b(R/L) -> Z(long) b(R/L) via broken gauge
	b_.id = 23;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_Zb.first;  zLim.second = zLim_Zb.second;
	f__a_to_b_c = &f__bRL_to_Zlong_bRL;  g__a_to_b_c = &g__bRL_to_Zlong_bRL;  G__a_to_b_c = &G__bRL_to_Zlong_bRL;  Ginv__a_to_b_c = &Ginv__bRL_to_Zlong_bRL;
      }
      else if ((IrunningSum += I__b_to_g_b)  && rnd < IrunningSum) {  //  b -> g b  (QCD)
	b_.id = 21;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = a.helicity;
	zLim.first = zLim_0b.first;  zLim.second = zLim_0b.second;
	f__a_to_b_c = &f__q_to_g_q;  g__a_to_b_c = &g__q_to_g_q;  G__a_to_b_c = &G__q_to_g_q;  Ginv__a_to_b_c = &Ginv__q_to_g_q;  
      }
    }
    //
    // Transverse W boson branchings
    //
    if (isWT(a)) {
      pair<double,double> zLim_00(-1,-1);  if (WT_to_f_f) zLim_00 = zMinMax(aTemp.v,mMassless,mMassless);
      pair<double,double> zLim_W0(-1,-1);  if (WT_to_Wlong_gamT) zLim_W0 = zMinMax(aTemp.v,mW,mMassless);
      pair<double,double> zLim_WZ(-1,-1);  if (WT_to_Wlong_ZT) zLim_WZ = zMinMax(aTemp.v,mW,mZ);
      pair<double,double> zLim_hW(-1,-1);  if (WT_to_h_WT) zLim_hW = zMinMax(aTemp.v,mh,mW);
      pair<double,double> zLim_ZW(-1,-1);  if (WT_to_ZT_WT || WT_to_Zlong_WT || WT_to_H0_Wlong) zLim_ZW = zMinMax(aTemp.v,mZ,mW);
      pair<double,double> zLim_gamZW(-1,-1);  if (WT_to_gamZT_WT) zLim_gamZW = zMinMax(aTemp.v,m(2223),mW);
      pair<double,double> zLim_tb(-1,-1);  if (WT_to_t_b) zLim_tb = zMinMax(aTemp.v,mtop,mbottom);
      double I__WT_to_gamZT_WT = 0;  if (WT_to_gamZT_WT && zLim_gamZW.first >= 0 && zLim_gamZW.second <= 1) { I__WT_to_gamZT_WT = CouplingPrefactor__WT_to_gamZT_WT() * (G__WT_to_gamZT_WT(zLim_gamZW.second) - G__WT_to_gamZT_WT(zLim_gamZW.first));  I += I__WT_to_gamZT_WT; }
      double I__WT_to_ZT_WT = 0;  if (WT_to_ZT_WT && zLim_ZW.first >= 0 && zLim_ZW.second <= 1) { I__WT_to_ZT_WT = CouplingPrefactor__WT_to_ZT_WT() * (G__WT_to_ZT_WT(zLim_ZW.second) - G__WT_to_ZT_WT(zLim_ZW.first));  I += I__WT_to_ZT_WT; }
      double I__WT_to_Wlong_gamT = 0;  if (WT_to_Wlong_gamT && zLim_W0.first >= 0 && zLim_W0.second <= 1) { I__WT_to_Wlong_gamT = CouplingPrefactor__WT_to_Wlong_gamT() * (G__WT_to_Wlong_gamT(zLim_W0.second) - G__WT_to_Wlong_gamT(zLim_W0.first));  I += I__WT_to_Wlong_gamT; }
      double I__WT_to_Wlong_ZT = 0;  if (WT_to_Wlong_ZT && zLim_WZ.first >= 0 && zLim_WZ.second <= 1) { I__WT_to_Wlong_ZT = CouplingPrefactor__WT_to_Wlong_ZT() * (G__WT_to_Wlong_ZT(zLim_WZ.second) - G__WT_to_Wlong_ZT(zLim_WZ.first));  I += I__WT_to_Wlong_ZT; }
      double I__WT_to_Zlong_WT = 0;  if (WT_to_Zlong_WT && zLim_ZW.first >= 0 && zLim_ZW.second <= 1) { I__WT_to_Zlong_WT = CouplingPrefactor__WT_to_Zlong_WT() * (G__WT_to_Zlong_WT(zLim_ZW.second) - G__WT_to_Zlong_WT(zLim_ZW.first));  I += I__WT_to_Zlong_WT; }
      double I__WT_to_h_WT = 0;  if (WT_to_h_WT && zLim_hW.first >= 0 && zLim_hW.second <= 1) { I__WT_to_h_WT = CouplingPrefactor__WT_to_h_WT() * (G__WT_to_h_WT(zLim_hW.second) - G__WT_to_h_WT(zLim_hW.first));  I += I__WT_to_h_WT; }
      double I__WT_to_f_f = 0;  if (WT_to_f_f && zLim_00.first >= 0 && zLim_00.second <= 1) { I__WT_to_f_f = CouplingPrefactor__WT_to_f_f() * (G__WT_to_f_f(zLim_00.second) - G__WT_to_f_f(zLim_00.first));  I += I__WT_to_f_f; }
      double I__WT_to_t_b = 0;  if (WT_to_t_b && zLim_tb.first >= 0 && zLim_tb.second <= 1) { I__WT_to_t_b = CouplingPrefactor__WT_to_t_b() * (G__WT_to_t_b(zLim_tb.second) - G__WT_to_t_b(zLim_tb.first));  I += I__WT_to_t_b; }
      double I__WT_to_H0_Wlong = 0;  if (WT_to_H0_Wlong && zLim_ZW.first >= 0 && zLim_ZW.second <= 1) { I__WT_to_H0_Wlong = CouplingPrefactor__WT_to_H0_Wlong() * (G__WT_to_H0_Wlong(zLim_ZW.second) - G__WT_to_H0_Wlong(zLim_ZW.first));  I += I__WT_to_H0_Wlong; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__WT_to_gamZT_WT)  && rnd < IrunningSum) { //  W(T) -> photon/Z(T) W(T)
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = randPlusMinus1();
	b_.densityMatrix[0] = 0;  b_.densityMatrix[1] = 1;  b_.densityMatrix[2] = 0;  b_.densityMatrix[3] = 0;  // pure W0
	zLim.first = zLim_gamZW.first;  zLim.second = zLim_gamZW.second;
	f__a_to_b_c = &f__WT_to_gamZT_WT;  g__a_to_b_c = &g__WT_to_gamZT_WT;  G__a_to_b_c = &G__WT_to_gamZT_WT;  Ginv__a_to_b_c = &Ginv__WT_to_gamZT_WT;  
      }
      else if ((IrunningSum += I__WT_to_ZT_WT)  && rnd < IrunningSum) { //  W(T) -> Z(T) W(T)   [Depricated. For testing purposes only.]
	b_.id = 23;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = randPlusMinus1();
	zLim.first = zLim_ZW.first;  zLim.second = zLim_ZW.second;
	f__a_to_b_c = &f__WT_to_ZT_WT;  g__a_to_b_c = &g__WT_to_ZT_WT;  G__a_to_b_c = &G__WT_to_ZT_WT;  Ginv__a_to_b_c = &Ginv__WT_to_ZT_WT;  
      }
      else if ((IrunningSum += I__WT_to_Wlong_gamT)  && rnd < IrunningSum) { //  W(T) -> W(long) gam(T)   ultracollinear
	b_.id = a.id;  c_.id = 22;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_W0.first;  zLim.second = zLim_W0.second;
	f__a_to_b_c = &f__WT_to_Wlong_gamT;  g__a_to_b_c = &g__WT_to_Wlong_gamT;  G__a_to_b_c = &G__WT_to_Wlong_gamT;  Ginv__a_to_b_c = &Ginv__WT_to_Wlong_gamT;  
      }
      else if ((IrunningSum += I__WT_to_Wlong_ZT)  && rnd < IrunningSum) { //  W(T) -> W(long) Z(T)   ultracollinear
	b_.id = a.id;  c_.id = 23;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_WZ.first;  zLim.second = zLim_WZ.second;
	f__a_to_b_c = &f__WT_to_Wlong_ZT;  g__a_to_b_c = &g__WT_to_Wlong_ZT;  G__a_to_b_c = &G__WT_to_Wlong_ZT;  Ginv__a_to_b_c = &Ginv__WT_to_Wlong_ZT;  
      }
      else if ((IrunningSum += I__WT_to_Zlong_WT)  && rnd < IrunningSum) { //  W(T) -> Z(long) W(T)   ultracollinear
	b_.id = 23;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_ZW.first;  zLim.second = zLim_ZW.second;
	f__a_to_b_c = &f__WT_to_Zlong_WT;  g__a_to_b_c = &g__WT_to_Zlong_WT;  G__a_to_b_c = &G__WT_to_Zlong_WT;  Ginv__a_to_b_c = &Ginv__WT_to_Zlong_WT;  
      }
      else if ((IrunningSum += I__WT_to_h_WT)  && rnd < IrunningSum) {  //  W(T) -> h W(T)   ultracollinear
	b_.id = 25;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = a.helicity;
	b_.canShower = false;  // non-showering
	zLim.first = zLim_hW.first;  zLim.second = zLim_hW.second;
	f__a_to_b_c = &f__WT_to_h_WT;  g__a_to_b_c = &g__WT_to_h_WT;  G__a_to_b_c = &G__WT_to_h_WT;  Ginv__a_to_b_c = &Ginv__WT_to_h_WT;  
      }
      else if ((IrunningSum += I__WT_to_f_f)  && rnd < IrunningSum) { //  W(T) -> f f'
	int sgn = a.signid();
	int flavor = randGen.Integer(Ndoublets);
	if      (flavor>=0  && flavor<  NC)   { b_.id = sgn*( 2);  c_.id = sgn*(- 1); }  // trivial CKM...
	else if (flavor>=NC && flavor<2*NC)   { b_.id = sgn*( 4);  c_.id = sgn*(- 3); }
	else if (flavor==2*NC  )              { b_.id = sgn*(12);  c_.id = sgn*(-11); }    // if Ndoublets < 8, will preferentially try to split to quarks, then leptons
	else if (flavor==2*NC+1)              { b_.id = sgn*(14);  c_.id = sgn*(-13); }
	else if (flavor==2*NC+2)              { b_.id = sgn*(16);  c_.id = sgn*(-15); }
	else   continue;   // simply don't branch if chooses nonexistant flavor beyond the canonical ones
	b_.helicity = sgn*(-1);  c_.helicity = -b_.helicity;
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__WT_to_f_f;  g__a_to_b_c = &g__WT_to_f_f;  G__a_to_b_c = &G__WT_to_f_f;  Ginv__a_to_b_c = &Ginv__WT_to_f_f; 
      }
      else if ((IrunningSum += I__WT_to_t_b)  && rnd < IrunningSum) { //  W(T) -> t b  (chirality-preserving)
	int sgn = a.signid();
	b_.id = sgn*(6);  c_.id = sgn*(-5);
	b_.helicity = sgn*(-1);  c_.helicity = -b_.helicity;
	zLim.first = zLim_tb.first;  zLim.second = zLim_tb.second;
	f__a_to_b_c = &f__WT_to_t_b;  g__a_to_b_c = &g__WT_to_t_b;  G__a_to_b_c = &G__WT_to_t_b;  Ginv__a_to_b_c = &Ginv__WT_to_t_b; 
      }
      else if ((IrunningSum += I__WT_to_H0_Wlong)  && rnd < IrunningSum) { //  W(T) -> h/Z(long) W(long)
	b_.id = 2325;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = 0;
	if (a.id>0)  b_.densityMatrix = rhoH0star;  else  b_.densityMatrix = rhoH0;
	zLim.first = zLim_ZW.first;  zLim.second = zLim_ZW.second;
	f__a_to_b_c = &f__WT_to_H0_Wlong;  g__a_to_b_c = &g__WT_to_H0_Wlong;  G__a_to_b_c = &G__WT_to_H0_Wlong;  Ginv__a_to_b_c = &Ginv__WT_to_H0_Wlong;  
      }
    }
    //
    // Tranverse mixed photon/Z branchings
    //
    if (isGamZT(a)) {
      fill(_dPij_total.begin(), _dPij_total.end(), 0.);
      pair<double,double> zLim_00(-1,-1);  if (gamZT_to_f_f) zLim_00 = zMinMax(aTemp.v,mMassless,mMassless);
      pair<double,double> zLim_WW(-1,-1);  if (gamZT_to_WT_WT || gamZT_to_Wlong_WT || gamZT_to_Wlong_Wlong) zLim_WW = zMinMax(aTemp.v,mW,mW);
      pair<double,double> zLim_hZ(-1,-1);  if (gamZT_to_h_Zlong) zLim_hZ = zMinMax(aTemp.v,mh,mZ);
      pair<double,double> zLim_tt(-1,-1);  if (gamZT_to_t_t) zLim_tt = zMinMax(aTemp.v,mtop,mtop);
      pair<double,double> zLim_bb(-1,-1);  if (gamZT_to_b_b) zLim_bb = zMinMax(aTemp.v,mbottom,mbottom);
      double I__gamZT_to_WT_WT = 0;  double couplingPrefactor__gamZT_to_WT_WT = 0;  if (gamZT_to_WT_WT && zLim_WW.first >= 0 && zLim_WW.second <= 1) { couplingPrefactor__gamZT_to_WT_WT = CouplingPrefactor__gamZT_to_WT_WT();  I__gamZT_to_WT_WT = couplingPrefactor__gamZT_to_WT_WT * (G__gamZT_to_WT_WT(zLim_WW.second) - G__gamZT_to_WT_WT(zLim_WW.first));  I += I__gamZT_to_WT_WT;  AddTo(_dPij_total, _dPij_WT_WT,  VtoVVintegral(max(zLim_WW.first,2*mW/a.E()),min(zLim_WW.second,1-2*mW/a.E()))); }
      double I__gamZT_to_Wlong_WT = 0;  double couplingPrefactor__gamZT_to_Wlong_WT = 0;  if (gamZT_to_Wlong_WT && zLim_WW.first >= 0 && zLim_WW.second <= 1) { couplingPrefactor__gamZT_to_Wlong_WT = CouplingPrefactor__gamZT_to_Wlong_WT();  I__gamZT_to_Wlong_WT = couplingPrefactor__gamZT_to_Wlong_WT * (G__gamZT_to_Wlong_WT(zLim_WW.second) - G__gamZT_to_Wlong_WT(zLim_WW.first));  I += I__gamZT_to_Wlong_WT; }   //// Currently this is not added to the evolution matrix _dPij_total (the gauge splitting contribution is anyway dominated by WT_WT with a single-log ambiguity)
      double I__gamZT_to_f_f = 0;  double couplingPrefactor__gamZT_to_f_f = 0;  if (gamZT_to_f_f && zLim_00.first >= 0 && zLim_00.second <= 1) { couplingPrefactor__gamZT_to_f_f = CouplingPrefactor__gamZT_to_f_f();  I__gamZT_to_f_f = couplingPrefactor__gamZT_to_f_f * (G__gamZT_to_f_f(zLim_00.second) - G__gamZT_to_f_f(zLim_00.first));  I += I__gamZT_to_f_f;  AddTo(_dPij_total, _dPij_sum_f_f, 2./3*(cube(zLim_00.second)-cube(zLim_00.first))); }
      double I__gamZT_to_t_t = 0;  double couplingPrefactor__gamZT_to_t_t = 0;  if (gamZT_to_t_t && zLim_tt.first >= 0 && zLim_tt.second <= 1) { couplingPrefactor__gamZT_to_t_t = CouplingPrefactor__gamZT_to_t_t();  I__gamZT_to_t_t = couplingPrefactor__gamZT_to_t_t * (G__gamZT_to_t_t(zLim_tt.second) - G__gamZT_to_t_t(zLim_tt.first));  I += I__gamZT_to_t_t;  AddTo(_dPij_total, _dPij_t_t, 2./3*(cube(zLim_tt.second)-cube(zLim_tt.first))); }
      double I__gamZT_to_b_b = 0;  double couplingPrefactor__gamZT_to_b_b = 0;  if (gamZT_to_b_b && zLim_bb.first >= 0 && zLim_bb.second <= 1) { couplingPrefactor__gamZT_to_b_b = CouplingPrefactor__gamZT_to_b_b();  I__gamZT_to_b_b = couplingPrefactor__gamZT_to_b_b * (G__gamZT_to_b_b(zLim_bb.second) - G__gamZT_to_b_b(zLim_bb.first));  I += I__gamZT_to_b_b;  AddTo(_dPij_total, _dPij_b_b, 2./3*(cube(zLim_bb.second)-cube(zLim_bb.first))); }
      double I__gamZT_to_Wlong_Wlong = 0;  double couplingPrefactor__gamZT_to_Wlong_Wlong = 0;  if (gamZT_to_Wlong_Wlong && zLim_WW.first >= 0 && zLim_WW.second <= 1) { couplingPrefactor__gamZT_to_Wlong_Wlong = CouplingPrefactor__gamZT_to_Wlong_Wlong();  I__gamZT_to_Wlong_Wlong = couplingPrefactor__gamZT_to_Wlong_Wlong * (G__gamZT_to_Wlong_Wlong(zLim_WW.second) - G__gamZT_to_Wlong_Wlong(zLim_WW.first));  I += I__gamZT_to_Wlong_Wlong;  AddTo(_dPij_total, _dPij_Wlong_Wlong, (1./2)*(square(zLim_WW.second)-square(zLim_WW.first)) - (1./3)*(cube(zLim_WW.second)-cube(zLim_WW.first)) ); }
      double I__gamZT_to_h_Zlong = 0;  double couplingPrefactor__gamZT_to_h_Zlong = 0;  if (gamZT_to_h_Zlong && zLim_hZ.first >= 0 && zLim_hZ.second <= 1) { couplingPrefactor__gamZT_to_h_Zlong = CouplingPrefactor__gamZT_to_h_Zlong();  I__gamZT_to_h_Zlong = couplingPrefactor__gamZT_to_h_Zlong * (G__gamZT_to_h_Zlong(zLim_hZ.second) - G__gamZT_to_h_Zlong(zLim_hZ.first));  I += I__gamZT_to_h_Zlong;  AddTo(_dPij_total, _dPij_h_Zlong, (1./2)*(square(zLim_hZ.second)-square(zLim_hZ.first)) - (1./3)*(cube(zLim_hZ.second)-cube(zLim_hZ.first)) ); }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__gamZT_to_WT_WT)  && rnd < IrunningSum) { //  gamZ(T) -> W(T) W(T)
	b_.id = 24;  c_.id = -24;
	b_.helicity = randPlusMinus1();  c_.helicity = randPlusMinus1();  // provisionally assign random transverse helicities
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__gamZT_to_WT_WT;  g__a_to_b_c = &g__gamZT_to_WT_WT;  G__a_to_b_c = &G__gamZT_to_WT_WT;  Ginv__a_to_b_c = &Ginv__gamZT_to_WT_WT;  
      }
      else if ((IrunningSum += I__gamZT_to_Wlong_WT)  && rnd < IrunningSum) { //  gamZ(T) -> W(long) W(T)
	b_.id = randPlusMinus1()*24;  c_.id = -b_.id;
	b_.helicity = 0;  c_.helicity = a.helicity;  // provisionally assign random transverse helicities
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__gamZT_to_Wlong_WT;  g__a_to_b_c = &g__gamZT_to_Wlong_WT;  G__a_to_b_c = &G__gamZT_to_Wlong_WT;  Ginv__a_to_b_c = &Ginv__gamZT_to_Wlong_WT;  
      }
      else if ((IrunningSum += I__gamZT_to_f_f)  && rnd < IrunningSum) { //  gamZ(T) -> f f
	double couplingPrefactor = couplingPrefactor__gamZT_to_f_f;
	double trRho = Trace(_aVirtual->densityMatrix);
	double PuR = MultiplyTrace(_aVirtual->densityMatrix,_dPij_uR) / trRho / couplingPrefactor;
	double PuL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_uL) / trRho / couplingPrefactor;
	double PcR = PuR;
	double PcL = PuL;
	double PdR = MultiplyTrace(_aVirtual->densityMatrix,_dPij_dR) / trRho / couplingPrefactor;
	double PdL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_dL) / trRho / couplingPrefactor;
	double PsR = PdR;
	double PsL = PdL;
	double PeR = MultiplyTrace(_aVirtual->densityMatrix,_dPij_eR) / trRho / couplingPrefactor;
	double PeL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_eL) / trRho / couplingPrefactor;
	double PmuR = PeR;
	double PmuL = PeL;
	double PtauR = PeR;
	double PtauL = PeL;
	double PveL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_veL) / trRho / couplingPrefactor;
	double PvmuL = PveL;
	double PvtauL = PveL;
	double rndff = randGen.Uniform();  double PrunningSum = 0;
	b_.id = 0;
	if      ((PrunningSum += PuR   )  && rndff < PrunningSum)  { b_.id =  2;  b_.helicity =  1; }
	else if ((PrunningSum += PuL   )  && rndff < PrunningSum)  { b_.id =  2;  b_.helicity = -1; }
	else if ((PrunningSum += PcR   )  && rndff < PrunningSum)  { b_.id =  4;  b_.helicity =  1; }
	else if ((PrunningSum += PcL   )  && rndff < PrunningSum)  { b_.id =  4;  b_.helicity = -1; }
	else if ((PrunningSum += PdR   )  && rndff < PrunningSum)  { b_.id =  1;  b_.helicity =  1; }
	else if ((PrunningSum += PdL   )  && rndff < PrunningSum)  { b_.id =  1;  b_.helicity = -1; }
	else if ((PrunningSum += PsR   )  && rndff < PrunningSum)  { b_.id =  3;  b_.helicity =  1; }
	else if ((PrunningSum += PsL   )  && rndff < PrunningSum)  { b_.id =  3;  b_.helicity = -1; }
	else if ((PrunningSum += PeR   )  && rndff < PrunningSum)  { b_.id = 11;  b_.helicity =  1; }
	else if ((PrunningSum += PeL   )  && rndff < PrunningSum)  { b_.id = 11;  b_.helicity = -1; }
	else if ((PrunningSum += PmuR  )  && rndff < PrunningSum)  { b_.id = 13;  b_.helicity =  1; }
	else if ((PrunningSum += PmuL  )  && rndff < PrunningSum)  { b_.id = 13;  b_.helicity = -1; }
	else if ((PrunningSum += PtauR )  && rndff < PrunningSum)  { b_.id = 15;  b_.helicity =  1; }
	else if ((PrunningSum += PtauL )  && rndff < PrunningSum)  { b_.id = 15;  b_.helicity = -1; }
	else if ((PrunningSum += PveL  )  && rndff < PrunningSum)  { b_.id = 12;  b_.helicity = -1; }
	else if ((PrunningSum += PvmuL )  && rndff < PrunningSum)  { b_.id = 14;  b_.helicity = -1; }
	else if ((PrunningSum += PvtauL)  && rndff < PrunningSum)  { b_.id = 16;  b_.helicity = -1; }
	if (b_.id == 0) { cout << "WARNING: BAD FERMION FLAVOR IN photon/Z -> f fbar....SOMETHING IS WRONG" << endl;  continue; }
	c_.id = -b_.id;  c_.helicity = -b_.helicity;
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__gamZT_to_f_f;  g__a_to_b_c = &g__gamZT_to_f_f;  G__a_to_b_c = &G__gamZT_to_f_f;  Ginv__a_to_b_c = &Ginv__gamZT_to_f_f; 
      }
      else if ((IrunningSum += I__gamZT_to_t_t)  && rnd < IrunningSum) { //  gamZ(T) -> t t
	double couplingPrefactor = couplingPrefactor__gamZT_to_t_t;
	double trRho = Trace(_aVirtual->densityMatrix);
	double PtR = MultiplyTrace(_aVirtual->densityMatrix,_dPij_tR) / trRho / couplingPrefactor;
	double PtL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_tL) / trRho / couplingPrefactor;
	double rndff = randGen.Uniform();  double PrunningSum = 0;
	b_.id = 0;
	if      ((PrunningSum += PtR   )  && rndff < PrunningSum)  { b_.id =  6;  b_.helicity =  1; }
	else if ((PrunningSum += PtL   )  && rndff < PrunningSum)  { b_.id =  6;  b_.helicity = -1; }
	if (b_.id == 0) { cout << "WARNING: BAD FERMION FLAVOR IN photon/Z -> t tbar....SOMETHING IS WRONG" << endl;  continue; }
	c_.id = -b_.id;  c_.helicity = -b_.helicity;
	zLim.first = zLim_tt.first;  zLim.second = zLim_tt.second;
	f__a_to_b_c = &f__gamZT_to_t_t;  g__a_to_b_c = &g__gamZT_to_t_t;  G__a_to_b_c = &G__gamZT_to_t_t;  Ginv__a_to_b_c = &Ginv__gamZT_to_t_t; 
      }
      else if ((IrunningSum += I__gamZT_to_b_b)  && rnd < IrunningSum) { //  gamZ(T) -> b b
	double couplingPrefactor = couplingPrefactor__gamZT_to_b_b;
	double trRho = Trace(_aVirtual->densityMatrix);
	double PbR = MultiplyTrace(_aVirtual->densityMatrix,_dPij_bR) / trRho / couplingPrefactor;
	double PbL = MultiplyTrace(_aVirtual->densityMatrix,_dPij_bL) / trRho / couplingPrefactor;
	double rndff = randGen.Uniform();  double PrunningSum = 0;
	b_.id = 0;
	if      ((PrunningSum += PbR   )  && rndff < PrunningSum)  { b_.id =  5;  b_.helicity =  1; }
	else if ((PrunningSum += PbL   )  && rndff < PrunningSum)  { b_.id =  5;  b_.helicity = -1; }
	if (b_.id == 0) { cout << "WARNING: BAD FERMION FLAVOR IN photon/Z -> b bbar....SOMETHING IS WRONG" << endl;  continue; }
	c_.id = -b_.id;  c_.helicity = -b_.helicity;
	zLim.first = zLim_bb.first;  zLim.second = zLim_bb.second;
	f__a_to_b_c = &f__gamZT_to_b_b;  g__a_to_b_c = &g__gamZT_to_b_b;  G__a_to_b_c = &G__gamZT_to_b_b;  Ginv__a_to_b_c = &Ginv__gamZT_to_b_b; 
      }
      else if ((IrunningSum += I__gamZT_to_Wlong_Wlong)  && rnd < IrunningSum) { //  gamZ(T) -> W(long) W(long)
	b_.id = 24;  c_.id = -24;
	b_.helicity = 0;  c_.helicity = 0;
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__gamZT_to_Wlong_Wlong;  g__a_to_b_c = &g__gamZT_to_Wlong_Wlong;  G__a_to_b_c = &G__gamZT_to_Wlong_Wlong;  Ginv__a_to_b_c = &Ginv__gamZT_to_Wlong_Wlong;  
      }
      else if ((IrunningSum += I__gamZT_to_h_Zlong)  && rnd < IrunningSum) { //  gamZ(T) -> h Z(long)
	b_.id = 2325;  c_.id = 2325;
	b_.helicity = 0;  c_.helicity = 0;
	b_.densityMatrix = rhoHiggs;   // project onto pure CP-even
	c_.densityMatrix = rhoZlong;   // project onto pure CP-odd
	zLim.first = zLim_hZ.first;  zLim.second = zLim_hZ.second;
	f__a_to_b_c = &f__gamZT_to_h_Zlong;  g__a_to_b_c = &g__gamZT_to_h_Zlong;  G__a_to_b_c = &G__gamZT_to_h_Zlong;  Ginv__a_to_b_c = &Ginv__gamZT_to_h_Zlong;  
      }
    }
    //
    // Photon branchings (QED, only valid for Q < ~mZ, otherwise should do mixed evolution with Z boson)
    //
    if (isGamma(a)) {
      pair<double,double> zLim_00(-1,-1);  if (gamma_to_f_f) zLim_00 = zMinMax(aTemp.v,mMassless,mMassless);
      pair<double,double> zLim_bb(-1,-1);  if (gamma_to_b_b) zLim_bb = zMinMax(aTemp.v,mbottom,mbottom);
      double I__gamma_to_f_f = 0;  if (gamma_to_f_f && zLim_00.first >= 0 && zLim_00.second <= 1) { I__gamma_to_f_f = CouplingPrefactor__gamma_to_f_f() * (G__gamma_to_f_f(zLim_00.second) - G__gamma_to_f_f(zLim_00.first));  I += I__gamma_to_f_f; }
      double I__gamma_to_b_b = 0;  if (gamma_to_b_b && zLim_bb.first >= 0 && zLim_bb.second <= 1) { I__gamma_to_b_b = CouplingPrefactor__gamma_to_b_b() * (G__gamma_to_b_b(zLim_bb.second) - G__gamma_to_b_b(zLim_bb.first));  I += I__gamma_to_b_b; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__gamma_to_f_f)  && rnd < IrunningSum) { //  photon -> f f
	double rnd_flavor = randGen.Uniform() * (3 + 2*3*(4./9) + 2*3*(1./9));
	if      (rnd_flavor < 1)                          { b_.id = 11;  c_.id = -11; }  // electron
	else if (rnd_flavor < 2)                          { b_.id = 13;  c_.id = -13; }  // muon
	else if (rnd_flavor < 3)                          { b_.id = 15;  c_.id = -15; }  // tau
	else if (rnd_flavor < 3 +   3*(4./9))             { b_.id =  2;  c_.id = - 2; }  // up-quark
	else if (rnd_flavor < 3 + 2*3*(4./9))             { b_.id =  4;  c_.id = - 4; }  // charm-quark
	else if (rnd_flavor < 3 + 2*3*(4./9) + 3*(1./9))  { b_.id =  1;  c_.id = - 1; }  // down-quark
	else                                              { b_.id =  3;  c_.id = - 3; }  // strange-quark
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	zLim.first = zLim_00.first;  zLim.second = zLim_00.second;
	f__a_to_b_c = &f__gamma_to_f_f;  g__a_to_b_c = &g__gamma_to_f_f;  G__a_to_b_c = &G__gamma_to_f_f;  Ginv__a_to_b_c = &Ginv__gamma_to_f_f; 
      }
      else if ((IrunningSum += I__gamma_to_b_b)  && rnd < IrunningSum) { //  photon -> b b
	b_.id = 5;  c_.id = -5;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	zLim.first = zLim_bb.first;  zLim.second = zLim_bb.second;
	f__a_to_b_c = &f__gamma_to_b_b;  g__a_to_b_c = &g__gamma_to_b_b;  G__a_to_b_c = &G__gamma_to_b_b;  Ginv__a_to_b_c = &Ginv__gamma_to_b_b; 
      }
    }
    //
    // Longitudinal W / charged scalar branchings
    //
    if (isWlong(a)) {
      pair<double,double> zLim_WZ(-1,-1); if(Wlong_to_WT_H0) zLim_WZ = zMinMax(aTemp.v,mW,mZ);
      pair<double,double> zLim_ZW(-1,-1); if(Wlong_to_Zlong_Wlong) zLim_ZW = zMinMax(aTemp.v,mZ,mW);
      pair<double,double> zLim_hW(-1,-1); if(Wlong_to_h_Wlong) zLim_hW = zMinMax(aTemp.v,mh,mW);
      pair<double,double> zLim_gamZW(-1,-1); if(Wlong_to_gamZT_Wlong || Wlong_to_gamZT_WT) zLim_gamZW = zMinMax(aTemp.v,m(2223),mW);
      pair<double,double> zLim_tb(-1,-1); if(Wlong_to_tR_bR || Wlong_to_tL_bR) zLim_tb = zMinMax(aTemp.v,mtop,mbottom);
      double I__Wlong_to_WT_H0 = 0;  if (Wlong_to_WT_H0 && zLim_WZ.first >= 0 && zLim_WZ.second <= 1) { I__Wlong_to_WT_H0 = CouplingPrefactor__Wlong_to_WT_H0() * (G__Wlong_to_WT_H0(zLim_WZ.second) - G__Wlong_to_WT_H0(zLim_WZ.first));  I += I__Wlong_to_WT_H0; }
      double I__Wlong_to_gamZT_Wlong = 0;  if (Wlong_to_gamZT_Wlong && zLim_gamZW.first >= 0 && zLim_gamZW.second <= 1) { I__Wlong_to_gamZT_Wlong = CouplingPrefactor__Wlong_to_gamZT_Wlong() * (G__Wlong_to_gamZT_Wlong(zLim_gamZW.second) - G__Wlong_to_gamZT_Wlong(zLim_gamZW.first));  I += I__Wlong_to_gamZT_Wlong; }
      double I__Wlong_to_tR_bR = 0;  if (Wlong_to_tR_bR && zLim_tb.first >= 0 && zLim_tb.second <= 1) { I__Wlong_to_tR_bR = CouplingPrefactor__Wlong_to_tR_bR() * (G__Wlong_to_tR_bR(zLim_tb.second) - G__Wlong_to_tR_bR(zLim_tb.first));  I += I__Wlong_to_tR_bR; }
      double I__Wlong_to_Zlong_Wlong = 0;  if (Wlong_to_Zlong_Wlong && zLim_ZW.first >= 0 && zLim_ZW.second <= 1) { I__Wlong_to_Zlong_Wlong = CouplingPrefactor__Wlong_to_Zlong_Wlong() * (G__Wlong_to_Zlong_Wlong(zLim_ZW.second) - G__Wlong_to_Zlong_Wlong(zLim_ZW.first));  I += I__Wlong_to_Zlong_Wlong; }
      double I__Wlong_to_h_Wlong = 0;  if (Wlong_to_h_Wlong && zLim_hW.first >= 0 && zLim_hW.second <= 1) { I__Wlong_to_h_Wlong = CouplingPrefactor__Wlong_to_h_Wlong() * (G__Wlong_to_h_Wlong(zLim_hW.second) - G__Wlong_to_h_Wlong(zLim_hW.first));  I += I__Wlong_to_h_Wlong; }
      double I__Wlong_to_gamZT_WT = 0;  if (Wlong_to_gamZT_WT && zLim_gamZW.first >= 0 && zLim_gamZW.second <= 1) { I__Wlong_to_gamZT_WT = CouplingPrefactor__Wlong_to_gamZT_WT() * (G__Wlong_to_gamZT_WT(zLim_gamZW.second) - G__Wlong_to_gamZT_WT(zLim_gamZW.first));  I += I__Wlong_to_gamZT_WT; }
      double I__Wlong_to_tL_bR = 0;  if (Wlong_to_tL_bR && zLim_tb.first >= 0 && zLim_tb.second <= 1) { I__Wlong_to_tL_bR = CouplingPrefactor__Wlong_to_tL_bR() * (G__Wlong_to_tL_bR(zLim_tb.second) - G__Wlong_to_tL_bR(zLim_tb.first));  I += I__Wlong_to_tL_bR; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__Wlong_to_WT_H0)  && rnd < IrunningSum) {  //  W(long) -> W(T) h/Z(long)
	a_to_V_c = true;
	b_.id = a.id;  c_.id = 2325;
	b_.helicity = randPlusMinus1();  c_.helicity = 0;  // provisionally assign the W random helicity
	if (a.id>0)  c_.densityMatrix = rhoH0;  else  c_.densityMatrix = rhoH0star;
	zLim.first = zLim_WZ.first;  zLim.second = zLim_WZ.second;
	f__a_to_b_c = &f__Wlong_to_WT_H0;  g__a_to_b_c = &g__Wlong_to_WT_H0;  G__a_to_b_c = &G__Wlong_to_WT_H0;  Ginv__a_to_b_c = &Ginv__Wlong_to_WT_H0;
      }
      else if ((IrunningSum += I__Wlong_to_gamZT_Wlong)  && rnd < IrunningSum) {  //  W(long) -> photon/Z(T) W(long)
	a_to_V_c = true;
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = 0;  // provisionally assign the photon/Z random helicity
	b_.densityMatrix[0] = square(gY*Y(&a));  b_.densityMatrix[1] = square(gW*T3(&a));  b_.densityMatrix[2] = gY*gW*Y(&a)*T3(&a);  b_.densityMatrix[3] = 0; 
	zLim.first = zLim_gamZW.first;  zLim.second = zLim_gamZW.second;
	f__a_to_b_c = &f__Wlong_to_gamZT_Wlong;  g__a_to_b_c = &g__Wlong_to_gamZT_Wlong;  G__a_to_b_c = &G__Wlong_to_gamZT_Wlong;  Ginv__a_to_b_c = &Ginv__Wlong_to_gamZT_Wlong;
      }
      else if ((IrunningSum += I__Wlong_to_tR_bR)  && rnd < IrunningSum) {  //  W(long) -> t(R) b~(R)
	b_.id = a.signid()*6;  c_.id = a.signid()*(-5);
	b_.helicity = a.signid();  c_.helicity = a.signid();
	zLim.first = zLim_tb.first;  zLim.second = zLim_tb.second;
	f__a_to_b_c = &f__Wlong_to_tR_bR;  g__a_to_b_c = &g__Wlong_to_tR_bR;  G__a_to_b_c = &G__Wlong_to_tR_bR;  Ginv__a_to_b_c = &Ginv__Wlong_to_tR_bR;
      }
      else if ((IrunningSum += I__Wlong_to_Zlong_Wlong)  && rnd < IrunningSum) {  //  W(long) -> Z(long) W(long)   ultracollinear
	b_.id = 23;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;   // non-showering
	zLim.first = zLim_ZW.first;  zLim.second = zLim_ZW.second;
	f__a_to_b_c = &f__Wlong_to_Zlong_Wlong;  g__a_to_b_c = &g__Wlong_to_Zlong_Wlong;  G__a_to_b_c = &G__Wlong_to_Zlong_Wlong;  Ginv__a_to_b_c = &Ginv__Wlong_to_Zlong_Wlong;
      }
      else if ((IrunningSum += I__Wlong_to_h_Wlong)  && rnd < IrunningSum) {  //  W(long) -> h W(long)   ultracollinear
	b_.id = 25;  c_.id = a.id;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;   // non-showering
	zLim.first = zLim_hW.first;  zLim.second = zLim_hW.second;
	f__a_to_b_c = &f__Wlong_to_h_Wlong;  g__a_to_b_c = &g__Wlong_to_h_Wlong;  G__a_to_b_c = &G__Wlong_to_h_Wlong;  Ginv__a_to_b_c = &Ginv__Wlong_to_h_Wlong;
      }
      else if ((IrunningSum += I__Wlong_to_gamZT_WT)  && rnd < IrunningSum) {  //  W(long) -> photon/Z(T) W(long)   ultracollinear
	b_.id = 2223;  c_.id = a.id;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;  // random, opposite helicities
	b_.densityMatrix[0] = 0;  b_.densityMatrix[1] = 1;  b_.densityMatrix[2] = 0;  b_.densityMatrix[3] = 0;   // pure W0
	b_.canShower = false;  c_.canShower = false;
	zLim.first = zLim_gamZW.first;  zLim.second = zLim_gamZW.second;
	f__a_to_b_c = &f__Wlong_to_gamZT_WT;  g__a_to_b_c = &g__Wlong_to_gamZT_WT;  G__a_to_b_c = &G__Wlong_to_gamZT_WT;  Ginv__a_to_b_c = &Ginv__Wlong_to_gamZT_WT;
      }
       else if ((IrunningSum += I__Wlong_to_tL_bR)  && rnd < IrunningSum) {  //  W(long) -> t(L) b~(R)   ultracollinear
	b_.id = a.signid()*6;  c_.id = a.signid()*(-5);
	b_.helicity = -a.signid();  c_.helicity = a.signid();
        // allowing here secondary showering of the ultracollinear final-state (QCD & QED)
	zLim.first = zLim_tb.first;  zLim.second = zLim_tb.second;
	f__a_to_b_c = &f__Wlong_to_tL_bR;  g__a_to_b_c = &g__Wlong_to_tL_bR;  G__a_to_b_c = &G__Wlong_to_tL_bR;  Ginv__a_to_b_c = &Ginv__Wlong_to_tL_bR;
      }
    }
    //
    // Mixed h/Z(long) branchings
    //
    if (isHiggsZlong(a)) {
      double tr = Trace(a.densityMatrix);
      if (tr==0)  { cout << "WARNING:  TRIED TO SPLIT h/Z(long) WITH VANISHING DENSITY MATRIX...SKIPPING" << endl;  continue; }
      vector<double> weightedDensityMatrix(4,0.);  // weighted by relative propagator factors
      weightedDensityMatrix[0] = a.densityMatrix[0] * square((_Q2-m2(2325))/(_Q2-m2(25))) / tr;
      weightedDensityMatrix[1] = a.densityMatrix[1] * square((_Q2-m2(2325))/(_Q2-m2(23))) / tr;
      weightedDensityMatrix[2] = a.densityMatrix[2] * square((_Q2-m2(2325)))/(_Q2-m2(23))/(_Q2-m2(25)) / tr;
      weightedDensityMatrix[3] = a.densityMatrix[3] * square((_Q2-m2(2325)))/(_Q2-m2(23))/(_Q2-m2(25)) / tr;
      double H0prefactor = Trace(weightedDensityMatrix);
      pair<double,double> zLim_WW(-1,-1);  if(H0_to_WT_Wlong || H0_to_Wlong_Wlong || H0_to_WT_WT) zLim_WW = zMinMax(aTemp.v,mW,mW);
      pair<double,double> zLim_ZZ(-1,-1);  if(H0_to_Zlong_Zlong) zLim_ZZ = zMinMax(aTemp.v,mZ,mZ);
      pair<double,double> zLim_gamZZ(-1,-1);  if(H0_to_gamZT_H0) zLim_gamZZ = zMinMax(aTemp.v,m(2223),mZ);
      pair<double,double> zLim_tt(-1,-1);  if(H0_to_tR_tR) zLim_tt = zMinMax(aTemp.v,mtop,mtop);
      pair<double,double> zLim_hh(-1,-1);  if(H0_to_h_h) zLim_hh = zMinMax(aTemp.v,mh,mh);
      pair<double,double> zLim_hZ(-1,-1);  if(H0_to_h_Zlong) zLim_hZ = zMinMax(aTemp.v,mh,mZ);
      double I__H0_to_WT_Wlong = 0;  if (H0_to_WT_Wlong && zLim_WW.first >= 0 && zLim_WW.second <= 1) { I__H0_to_WT_Wlong = H0prefactor*CouplingPrefactor__H0_to_WT_Wlong() * (G__H0_to_WT_Wlong(zLim_WW.second) - G__H0_to_WT_Wlong(zLim_WW.first));  I += I__H0_to_WT_Wlong; }
      double I__H0_to_gamZT_H0 = 0;  if (H0_to_gamZT_H0 && _Q > mh+mZ && zLim_gamZZ.first >= 0 && zLim_gamZZ.second <= 1) { I__H0_to_gamZT_H0 = H0prefactor*CouplingPrefactor__H0_to_gamZT_H0() * (G__H0_to_gamZT_H0(zLim_gamZZ.second) - G__H0_to_gamZT_H0(zLim_gamZZ.first));  I += I__H0_to_gamZT_H0; }  // added "catch" for emissions below threshold (can't make a photon, but tries to make massless Z with crazy rate near Higgs resonance)
      double I__H0_to_tR_tR = 0;  if (H0_to_tR_tR && zLim_tt.first >= 0 && zLim_tt.second <= 1) { I__H0_to_tR_tR = H0prefactor*CouplingPrefactor__H0_to_tR_tR() * (G__H0_to_tR_tR(zLim_tt.second) - G__H0_to_tR_tR(zLim_tt.first));  I += I__H0_to_tR_tR; }
      double I__H0_to_Wlong_Wlong = 0;  if (H0_to_Wlong_Wlong && zLim_WW.first >= 0 && zLim_WW.second <= 1) { I__H0_to_Wlong_Wlong = CouplingPrefactor__H0_to_Wlong_Wlong() * (G__H0_to_Wlong_Wlong(zLim_WW.second) - G__H0_to_Wlong_Wlong(zLim_WW.first));  I += I__H0_to_Wlong_Wlong; }
      double I__H0_to_WT_WT = 0;  if (H0_to_WT_WT && zLim_WW.first >= 0 && zLim_WW.second <= 1) { I__H0_to_WT_WT = CouplingPrefactor__H0_to_WT_WT() * (G__H0_to_WT_WT(zLim_WW.second) - G__H0_to_WT_WT(zLim_WW.first));  I += I__H0_to_WT_WT; }
      double I__H0_to_Zlong_Zlong = 0;  if (H0_to_Zlong_Zlong && zLim_ZZ.first >= 0 && zLim_ZZ.second <= 1) { I__H0_to_Zlong_Zlong = weightedDensityMatrix[0]*CouplingPrefactor__H0_to_Zlong_Zlong() * (G__H0_to_Zlong_Zlong(zLim_ZZ.second) - G__H0_to_Zlong_Zlong(zLim_ZZ.first));  I += I__H0_to_Zlong_Zlong; }
      double I__H0_to_h_Zlong = 0;  if (H0_to_h_Zlong && zLim_hZ.first >= 0 && zLim_hZ.second <= 1) { I__H0_to_h_Zlong = weightedDensityMatrix[1]*CouplingPrefactor__H0_to_h_Zlong() * (G__H0_to_h_Zlong(zLim_hZ.second) - G__H0_to_h_Zlong(zLim_hZ.first));  I += I__H0_to_h_Zlong; }
      double I__H0_to_h_h = 0;  if (H0_to_h_h && zLim_hh.first >= 0 && zLim_hh.second <= 1) { I__H0_to_h_h = weightedDensityMatrix[0]*CouplingPrefactor__H0_to_h_h() * (G__H0_to_h_h(zLim_hh.second) - G__H0_to_h_h(zLim_hh.first));  I += I__H0_to_h_h; }
      double rnd = I*randGen.Uniform();  double IrunningSum = 0;
      if ((IrunningSum += I__H0_to_WT_Wlong)  && rnd < IrunningSum) {  //  h/Z(long) -> W(T) W(long)
	a_to_V_c = true;
	b_.id = -24;  c_.id = 24;
	double P_WTminus_WlongPlus = 1./2 - weightedDensityMatrix[3]/H0prefactor;  // unit probability for W(T)- W(long)+ for incoming H0, zero for H0*, 50/50 for any incoherent mix of h and Z(long)
	if (randGen.Uniform() > P_WTminus_WlongPlus)  { b_.id = 24;  c_.id = -24; } 
	b_.helicity = randPlusMinus1();  c_.helicity = 0;  // provisionally assign the W random helicity
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__H0_to_WT_Wlong;  g__a_to_b_c = &g__H0_to_WT_Wlong;  G__a_to_b_c = &G__H0_to_WT_Wlong;  Ginv__a_to_b_c = &Ginv__H0_to_WT_Wlong;
      }
      else if ((IrunningSum += I__H0_to_gamZT_H0)  && rnd < IrunningSum) {  //  h/Z(long) -> photon/Z(T) h/Z(long)
	a_to_V_c = true;
	b_.id = 2223;  c_.id = 2325;
	b_.helicity = randPlusMinus1();  c_.helicity = 0;  // provisionally assign the photon/Z random helicity
	b_.densityMatrix[0] = square(gY*Y(&a));  b_.densityMatrix[1] = square(gW*T3(&a));  b_.densityMatrix[2] = gY*gW*Y(&a)*T3(&a);  b_.densityMatrix[3] = 0;
	c_.densityMatrix[0] = weightedDensityMatrix[1];  c_.densityMatrix[1] = weightedDensityMatrix[0];  c_.densityMatrix[2] = -weightedDensityMatrix[2];  c_.densityMatrix[3] = weightedDensityMatrix[3];  // H0->H0, H0*->H0*, h->Z(long), Z(long)->h
	zLim.first = zLim_gamZZ.first;  zLim.second = zLim_gamZZ.second;
	f__a_to_b_c = &f__H0_to_gamZT_H0;  g__a_to_b_c = &g__H0_to_gamZT_H0;  G__a_to_b_c = &G__H0_to_gamZT_H0;  Ginv__a_to_b_c = &Ginv__H0_to_gamZT_H0;
      }
      else if ((IrunningSum += I__H0_to_tR_tR)  && rnd < IrunningSum) {  //  h/Z(long) -> t(R) t~(R)
	b_.id = 6;  c_.id = -6;
	double P_tR_tbarR = 1./2 - weightedDensityMatrix[3]/H0prefactor;  // unit probability for t(R) t~(R) for incoming H0, zero for H0*, 50/50 for any incoherent mix of h and Z(long)
	if (randGen.Uniform() > P_tR_tbarR)  { b_.id = -6;  c_.id = 6; } 
	b_.helicity = b_.signid();  c_.helicity = b_.helicity;
	zLim.first = zLim_tt.first;  zLim.second = zLim_tt.second;
	f__a_to_b_c = &f__H0_to_tR_tR;  g__a_to_b_c = &g__H0_to_tR_tR;  G__a_to_b_c = &G__H0_to_tR_tR;  Ginv__a_to_b_c = &Ginv__H0_to_tR_tR;
      }
      else if ((IrunningSum += I__H0_to_Wlong_Wlong)  && rnd < IrunningSum) {  //  h/Z(long) -> W(long) W(long)
	b_.id = 24;  c_.id = -24;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;  // non-showering
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__H0_to_Wlong_Wlong;  g__a_to_b_c = &g__H0_to_Wlong_Wlong;  G__a_to_b_c = &G__H0_to_Wlong_Wlong;  Ginv__a_to_b_c = &Ginv__H0_to_Wlong_Wlong;
      }
      else if ((IrunningSum += I__H0_to_WT_WT)  && rnd < IrunningSum) {  //  h/Z(long) -> W(T) W(T)
	b_.id = 24;  c_.id = -24;
	b_.helicity = randPlusMinus1();  c_.helicity = -b_.helicity;
	b_.canShower = false;  c_.canShower = false;  // non-showering
	zLim.first = zLim_WW.first;  zLim.second = zLim_WW.second;
	f__a_to_b_c = &f__H0_to_WT_WT;  g__a_to_b_c = &g__H0_to_WT_WT;  G__a_to_b_c = &G__H0_to_WT_WT;  Ginv__a_to_b_c = &Ginv__H0_to_WT_WT;
      }
      else if ((IrunningSum += I__H0_to_Zlong_Zlong)  && rnd < IrunningSum) {  //  h/Z(long) -> Z(long) Z(long)
	b_.id = 23;  c_.id = 23;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;  // non-showering
	zLim.first = zLim_ZZ.first;  zLim.second = zLim_ZZ.second;
	f__a_to_b_c = &f__H0_to_Zlong_Zlong;  g__a_to_b_c = &g__H0_to_Zlong_Zlong;  G__a_to_b_c = &G__H0_to_Zlong_Zlong;  Ginv__a_to_b_c = &Ginv__H0_to_Zlong_Zlong;
      }
      else if ((IrunningSum += I__H0_to_h_Zlong)  && rnd < IrunningSum) {  //  h/Z(long) -> h Z(long)
	b_.id = 25;  c_.id = 23;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;  // non-showering
	zLim.first = zLim_hZ.first;  zLim.second = zLim_hZ.second;
	f__a_to_b_c = &f__H0_to_h_Zlong;  g__a_to_b_c = &g__H0_to_h_Zlong;  G__a_to_b_c = &G__H0_to_h_Zlong;  Ginv__a_to_b_c = &Ginv__H0_to_h_Zlong;
      }
      else if ((IrunningSum += I__H0_to_h_h)  && rnd < IrunningSum) {  //  h/Z(long) -> h h
	b_.id = 25;  c_.id = 25;
	b_.helicity = 0;  c_.helicity = 0;
	b_.canShower = false;  c_.canShower = false;  // non-showering
	zLim.first = zLim_hh.first;  zLim.second = zLim_hh.second;
	f__a_to_b_c = &f__H0_to_h_h;  g__a_to_b_c = &g__H0_to_h_h;  G__a_to_b_c = &G__H0_to_h_h;  Ginv__a_to_b_c = &Ginv__H0_to_h_h;
      }
    }
    //
    //
    //
    double productionCorrection = 1;  if (applyProductionCorrectionsSplitting)  productionCorrection = ComputeProductionCorrection();  // account for changes to this particle's production probability now that it's gone off-shell
    double P_noBranch = exp( -tStep*I*productionCorrection );  // the Sudakov
    //
    // Evolve the density matrix if relevant (count this evolution step towards the joint wavefunction running, which may involve a rotation)
    // ...Only relevant for gamma/ZT. For h/ZL, the H0 and Hbar0 components lose amplitude identically, so the orientation of the state should not rotate (except for mismatches in ultracollinear contributions, which we'll ignore).
    if (isGamZT(a) && neutralBosonSudakovEvolution) {
      double P0 = (_dPij_total[0]+_dPij_total[1])/2;  // "trace" piece of total differential splitting matrix, proportional to sigma^0
      dPij_traceless[0] = _dPij_total[0]-P0;  dPij_traceless[1] = _dPij_total[1]-P0;  dPij_traceless[2] = _dPij_total[2];  dPij_traceless[3] = _dPij_total[3];
      double length_dPij = 0.5 * sqrt( square(_dPij_total[1]-_dPij_total[0]) + 4*(square(_dPij_total[2])+square(_dPij_total[3])) );
      if (length_dPij > 0) {
	double coshPo2 = exp(-tStep*(P0/2)*productionCorrection) *  cosh(tStep*(length_dPij/2)*productionCorrection);
	double sinhPo2 = exp(-tStep*(P0/2)*productionCorrection) *  sinh(tStep*(length_dPij/2)*productionCorrection);
	SudMatrix[0] = coshPo2 - (dPij_traceless[0]/length_dPij)*sinhPo2;  SudMatrix[1] = coshPo2 - (dPij_traceless[1]/length_dPij)*sinhPo2;  SudMatrix[2] = -(dPij_traceless[2]/length_dPij)*sinhPo2;  SudMatrix[3] = -(dPij_traceless[3]/length_dPij)*sinhPo2;
	EvolveRho(a.densityMatrix,SudMatrix);
      }
    }
    //
    // Check for splitting
    //
    if (P_noBranch < 0.7) {
      cout << "WARNING:  BRANCHING PROBABILITY (" << 1-P_noBranch << ") EXCEEDS 30% IN ONE STEP...CONSIDER A SMALLER STEP SIZE OR REDUCE OVER-ESTIMATOR FUNCTIONS" << endl;
      cout << "          momentum = " << a.v.P() << ",  Q = " << a.GetQ() << endl;
      cout << "          production correction = " << productionCorrection << endl;
    }
    if (randGen.Uniform() > P_noBranch) {
      z = Ginv__a_to_b_c( G__a_to_b_c(zLim.first) + randGen.Uniform()*(G__a_to_b_c(zLim.second) - G__a_to_b_c(zLim.first)) );  // throw random # according to over-estimator distribution
      if (z < zLim.first || zLim.second < z)  { cout << "WARNING:  z THROWN OUTSIDE OF BOUNDARIES...SOMETHING MAY BE WRONG WITH YOUR OVERESTIMATOR FUNCTIONS/INTEGRALS/INVERSES" << endl;  continue; }
      _mb = m(b_.id);  _mc = m(c_.id);  // set kinematic masses for splitting function evaluation  **Be careful that these are consistent with zLim's
      double frac = f__a_to_b_c(z)/g__a_to_b_c(z);
      if (frac > 1) {
	cout << "WARNING:  UPPER-BOUND ON SPLITING FUNCTION EXCEEDED" << endl << "    frac = " << frac << endl;
	cout << "    Q = " << a.GetQ() << ",  z = " << z << ",   f(z) = " << f__a_to_b_c(z) << ",  g(z) = " << g__a_to_b_c(z) << ",  G(z) = " << G__a_to_b_c(z) << ",  Ginv(G(z)) = " << Ginv__a_to_b_c(G__a_to_b_c(z)) << endl;
      }
      if (randGen.Uniform() < frac)  {
	if (a_to_V_c)  {  // set helicity of radiated vector boson
	  f__a_to_b_c(z);  // evaluate splitting function again, to be sure it sets kinematic quantities and computes polarization rates
	  double PR = _PR_eval;  double PL = _PL_eval;  double Plong = _Plong_eval;
	  double Ptot = PR+PL+Plong;  PR /= Ptot;  PL /= Ptot;  Plong /= Ptot;
	  double R = randGen.Uniform();
	  if      (R < PR   )  b_.helicity =  1;
	  else if (R < PR+PL)  b_.helicity = -1;
	  else                 b_.helicity =  0;
	}
	if (a_to_V_V)  {  // set helicity of radiated vector boson pair
	  f__a_to_b_c(z);  // evaluate splitting function again, to be sure it sets kinematic quantities and computes polarization rates
	  double PTT = _PTT_eval;  double PTlong = _PTlong_eval;  double PlongT = _PlongT_eval;  double Plonglong = _Plonglong_eval;
	  double Ptot = PTT+PTlong+PlongT+Plonglong;  PTT /= Ptot;  PTlong /= Ptot;  PlongT /= Ptot;  Plonglong /= Ptot;
	  double R = randGen.Uniform();
	  if      (R < PTT              )  { b_.helicity = randPlusMinus1();  c_.helicity = randPlusMinus1(); }
	  else if (R < PTT+PTlong       )  { b_.helicity = randPlusMinus1();  c_.helicity = 0; }
	  else if (R < PTT+PTlong+PlongT)  { b_.helicity = 0;  c_.helicity = randPlusMinus1(); }
	  else                             { b_.helicity = 0;  c_.helicity = 0; }
	  if (a.helicity!=0 && b_.helicity==0 && c_.helicity!=0)  b_.canShower=false;  // forbid ultracollinear longitudinals from subsequent showering
	  if (a.helicity!=0 && b_.helicity!=0 && c_.helicity==0)  c_.canShower=false;
	}
	b = b_;
	c = c_;
	Colorize(a,b,c);  // add/continue color lines
	a.f__a_to_b_c = f__a_to_b_c;  // tentatively assume that this splitting function gets assigned to a
	a.a_to_V_c = a_to_V_c;
	a.a_to_V_V = a_to_V_V;
	return true;
      }
    }
  }  // end virtuality evolution loop

  return false;  // evolved down to cutoff without branching
}



//  Evolve down a pair of particles in virtuality.  If any branchings occur, recurse.
//
bool EvolvePair(Particle & p1, Particle & p2, bool goodPair)
{
  goodPair = true;
  p1.branched = false;  p2.branched = false;
  p1.daughters.clear();  p2.daughters.clear();
  Particle p1original=p1.BasicCopy();  Particle p2original=p2.BasicCopy();  // keep copies of the originals for "recoiler" purposes
  p1.unrecoiledSister = &p2original;  p2.unrecoiledSister = &p1original;
  double m1original=p1original.M();  double m2original=p2original.M();  double z1 = -1;  double z2 = -1;
  Particle p1b,p1c,p2b,p2c;  // potential daughters 
  double m12 = (p1.v+p2.v).M();  // total 1+2 system mass

  // continue evolving until each particle either branches, reaches t=0, or hits a resonance threshold (in which case throw mass from Breit-Wigner)...restarting in cases with vetos
  bool doneEvolving1 = false;  bool doneEvolving2 = false;
  while (!doneEvolving1 || !doneEvolving2) {
    int goodEvolve1 = 1;  int goodEvolve2 = 1;   // 0: bad (veto),  1: good,  >1: special exception (e.g., gamma/ZT -> gamma projection...good and did not "branch"/decay, but continue evolving anyway)
    ///cout << eventCounter << " TRY EVOLVE  " << p1.id << " @ " << p1.GetQ() << ",  " << p2.id << " @ " << p2.GetQ() << endl;////
    ////cout << eventCounter << "    " << p1original.id << " " << p2original.id << endl;////
    if (!doneEvolving1 && !p1.branched) { p1.branched = EvolveParticle(p1,p1original,p2original,z1,p1b,p1c,goodEvolve1);  if (!p1.branched && goodEvolve1==1) doneEvolving1=true; }
    if (!doneEvolving2 && !p2.branched) { p2.branched = EvolveParticle(p2,p2original,p1original,z2,p2b,p2c,goodEvolve2);  if (!p2.branched && goodEvolve2==1) doneEvolving2=true; }
    if (p1.mother != NULL) {////
      ////if (isTopQuark(p1) && !goodEvolve1  ||  isTopQuark(p2) && !goodEvolve2)   cout << eventCounter << "      BAD TOP QUARK" << endl;
      //if (isVector(p1) && !goodEvolve1)  cout << eventCounter << "      BAD VECTOR " << p1.id << endl;
      //if (isVector(p2) && !goodEvolve2)  cout << eventCounter << "      BAD VECTOR " << p2.id << endl;
    }
    ///cout << eventCounter << " TRIED EVOLVE  " << p1.id << " @ " << p1.GetQ() << " (" << goodEvolve1 << "),  " << p2.id << " @ " << p2.GetQ() << " (" << goodEvolve2 << ")" << endl;////
    //if (p1.branched && goodEvolve1)  cout << "        " << p1.id << " -> " << p1b.id << " " << p1c.id << endl;///
    //if (p2.branched && goodEvolve2)  cout << "        " << p2.id << " -> " << p2b.id << " " << p2c.id << endl;///
    if (!goodEvolve1 || !goodEvolve2) {
      //cout << eventCounter << "  FAIL EVOLVEPAIR, VETO!" << endl;
      //cout << "         " <<  p1.id << " @ " << p1.GetQ() << " (" << goodEvolve1 << "),  " << p2.id << " @ " << p2.GetQ() << " (" << goodEvolve2 << ")" << endl;
      return false;  // one of the branchings triggered a veto (either something went wrong, or vetoed a weighted splitting)
    }
    double m1new = p1.GetQ();  // note that even if the particle isn't "branched", we may have assigned it a new virtuality (e.g., resonance mass distributed as Breit-Wigner is done as part of evolution)
    double m2new = p2.GetQ();
    if (m1new+m2new > m12) {  // violated mass/energy, veto the more off-shell branching
      //return false;  // alternately, just veto (too harsh!)
      if (m1new*m1new-m(p1.id)*m(p1.id) > m2new*m2new-m(p2.id)*m(p2.id)) { doneEvolving1=false; p1.branched=false; }
      else                                                               { doneEvolving2=false; p2.branched=false; }
      continue;
    }
    ResetMasses(p1,p2,m1new,m2new);  // reset the physical masses of the evolving particles according to their virtualities or final "on-shell" masses, adjust kinematics
    
    // set up splitting kinematics for a freshly-branched particle, accounting for possible off-shell sister
    if (p1.branched && !doneEvolving1) {  // p1 branched, check if the momentum-sharing is valid and set up the kinematics
      double mb = m(p1b.id);  double mc = m(p1c.id);
      pair<double,double> zLims = zMinMax(p1.v,mb,mc);  // recheck allowed z range with full kinematics (with p2 also possibly off-shell)
      if (z1 >= zLims.first && z1 <= zLims.second)   Split(p1,mb,mc,z1,p1b.v,p1c.v);   // looks good? then tentatively p1 is split
      else  p1.branched=false;   // otherwise, veto this branching, and later continue evolving
    }
    if (p2.branched && !doneEvolving2) {  // p2 branched, check if the momentum-sharing is valid and set up the kinematics
      double mb = m(p2b.id);  double mc = m(p2c.id);
      pair<double,double> zLims = zMinMax(p2.v,mb,mc);  // recheck allowed z range with full kinematics (with p1 also possibly off-shell)
      if (z2 >= zLims.first && z2 <= zLims.second)   Split(p2,mb,mc,z2,p2b.v,p2c.v);   // looks good? then tentatively p2 is split
      else  p2.branched=false;   // otherwise, continue its evolution
    }
    
    // boost-correct any daughters. includes cases where an entire shower/decay chain has already been developed for one of the particles from an earlier pass (with doneEvolving==true for
    // that particle), but the kinematics has to be re-adjusted due to changes/vetoes in the sister's shower.
    if (p1.branched)  BoostDaughters(p1.v,p2.v,p1b,p1c);
    if (p2.branched)  BoostDaughters(p2.v,p1.v,p2b,p2c);

    // check for angular-ordering
    //    THIS SHOULD BE UPDATED SUCH THAT THE APPROPRIATE "MOTHER" SPLITTING IS RECURSIVELY SOUGHT-OUT
    if (enforceAngularOrdering && p1.branched) {
      double thisAngle = p1b.v.Angle(p1c.Vect());
      double motherAngle = p1.v.Angle(p2.Vect());
      if (thisAngle > motherAngle)  p1.branched = false; 
    }
    if (enforceAngularOrdering && p2.branched) {
      double thisAngle = p2b.v.Angle(p2c.Vect());
      double motherAngle = p2.v.Angle(p1.Vect());
      if (thisAngle > motherAngle)  p2.branched = false; 
    }

    // attempt to develop the showers for the daughters and (recursively) granddaughters, etc
    if (p1.branched && !doneEvolving1) {
      p1b.t = min(p1.t,2*log(p1b.v.E()/Qmin));  p1c.t = min(p1.t,2*log(p1c.v.E()/Qmin));
      p1b.mother = &p1;    p1c.mother = &p1; 
      p1b.sister = &p1c;   p1c.sister = &p1b;
      p1b.aunt   = &p2;    p1c.aunt   = &p2;
      p1b.is_b   = true;   p1c.is_b   = false;
      p1b.is_c   = false;  p1c.is_c   = true;
      doneEvolving1 = EvolvePair(p1b,p1c);
      if (!doneEvolving1)  p1.branched = false;
    }
    if (p2.branched && !doneEvolving2) {
      p2b.t = min(p2.t,2*log(p2b.v.E()/Qmin));  p2c.t = min(p2.t,2*log(p2c.v.E()/Qmin));  
      p2b.mother = &p2;    p2c.mother = &p2;
      p2b.sister = &p2c;   p2c.sister = &p2b;
      p2b.aunt   = &p1;    p2c.aunt   = &p1;
      p2b.is_b   = true;   p2c.is_b   = false;
      p2b.is_c   = false;  p2c.is_c   = true; 
      doneEvolving2 = EvolvePair(p2b,p2c);
      if (!doneEvolving2)  p2.branched = false;
    }
    ////cout << eventCounter << " TRIED DAUGHTER EVOLVE  " << p1.id << " @ " << p1.GetQ() << " (" << doneEvolving1 << "),  " << p2.id << " @ " << p2.GetQ() << " (" << doneEvolving2 << ")" << endl;////

    // if we're *really* done evolving this pair, go ahead and nest their daughters inside them, or attempt to decay
    if (doneEvolving1 && doneEvolving2) {
      p1.daughters.clear();  if (p1.branched) { p1.daughters.push_back(p1b);  p1.daughters.push_back(p1c); }  else Decay(p1);
      p2.daughters.clear();  if (p2.branched) { p2.daughters.push_back(p2b);  p2.daughters.push_back(p2c); }  else Decay(p2);
    }
  }
  
  
  return goodPair;
}



// Reset particle masses in pairs, keeping their combined 4-vector fixed.
//
void ResetMasses(Particle & p1, Particle & p2, double m1, double m2)
{
  TVector3 CMboost = (p1.v+p2.v).BoostVector();
  TLorentzVector v1 = p1.v;
  TLorentzVector v2 = p2.v;
  v1.Boost(-CMboost);
  v2.Boost(-CMboost);
  double ECM = v1.E()+v2.E();
  double pStarOld = (v1.P()+v2.P())/2;
  double pStarNew = sqrt( (ECM*ECM - (m1+m2)*(m1+m2)) * (ECM*ECM - (m1-m2)*(m1-m2)) ) / 2 / ECM;
  v1.SetVectM(v1.Vect()*(pStarNew/v1.P()),m1);
  v2.SetVectM(v2.Vect()*(pStarNew/v2.P()),m2);
  v1.Boost(CMboost);
  v2.Boost(CMboost);
  p1.v = v1;
  p2.v = v2;
}


// In case a parent's kinematics has been adjusted due to a change in recoiler mass, re-synchronize the daughter boosts
//
// (Consider making this more efficient by first composing the boosts as TLorentzRotations into a single boost/rotation, and then applying only once to each particle.)
void BoostDaughters(TLorentzVector va, TLorentzVector vr, Particle & pb, Particle & pc)
{
  // go to CM frame of branching particle + its recoil partner
  TVector3 CMboost = (va+vr).BoostVector();
  va.Boost(-CMboost);
  pb.v.Boost(-CMboost);  BoostGrandDaughters(-CMboost,pb);
  pc.v.Boost(-CMboost);  BoostGrandDaughters(-CMboost,pc);
  
  // go to rest frame of branching particle
  TVector3 aBoost = va.BoostVector();
  pb.v.Boost(-aBoost);  BoostGrandDaughters(-aBoost,pb);
  pc.v.Boost(-aBoost);  BoostGrandDaughters(-aBoost,pc);
  
  // bring the daughters to rest in this frame
  TVector3 bcBoost = (pb.v+pc.v).BoostVector();
  pb.v.Boost(-bcBoost);  BoostGrandDaughters(-bcBoost,pb);
  pc.v.Boost(-bcBoost);  BoostGrandDaughters(-bcBoost,pc);
  
  // undo boosts and go back to lab
  pb.v.Boost(aBoost);  BoostGrandDaughters(aBoost,pb);
  pc.v.Boost(aBoost);  BoostGrandDaughters(aBoost,pc);
  pb.v.Boost(CMboost);  BoostGrandDaughters(CMboost,pb);
  pc.v.Boost(CMboost);  BoostGrandDaughters(CMboost,pc);
}



// Split 4-vector a->bc.  Here, "z" is a 3-momentum-sharing variable, rather than energy-sharing.
//
bool Split(Particle & a, double mb, double mc, double z, TLorentzVector & b, TLorentzVector & c)
{
  TLorentzVector va = a.v;
  double ma = va.M();
  if (mb+mc > ma)  return false;
  if (ma <= 0)  return false;
  double beta = va.Beta();
  double gamma = va.Gamma();
  double theta = va.Theta();
  double phi = va.Phi();
  
  // a-frame kinematics, except for "decay" angle
  double EbStar = (ma*ma - mc*mc + mb*mb) / 2 / ma;
  double EcStar = (ma*ma - mb*mb + mc*mc) / 2 / ma;
  double pStar = sqrt( (ma*ma - (mb+mc)*(mb+mc)) * (ma*ma - (mb-mc)*(mb-mc)) ) / 2 / ma;
  double betabStar = sqrt(1 - mb*mb/EbStar/EbStar);
  double betacStar = sqrt(1 - mc*mc/EcStar/EcStar);
  
  // solve for cosThetaStar == cStar
  double zBar = 1-z;
  double coeffA = (zBar*zBar - z*z) * (gamma*gamma - 1);
  double coeffB = 2*gamma*gamma*beta * (zBar*zBar/betabStar + z*z/betacStar);
  double coeffC = (zBar*zBar - z*z) + gamma*gamma*beta*beta*(zBar*zBar/betabStar/betabStar - z*z/betacStar/betacStar);
  double discr = coeffB*coeffB - 4*coeffA*coeffC;
  double cStar = 0;
  if (z==zBar) cStar = -coeffC/coeffB;  // when z==zBar==0.5, the equation becomes linear
  else         cStar = (-coeffB + sqrt(discr)) / 2 / coeffA;
  double sStar = sqrt(1-cStar*cStar);

  // set kinematics
  TLorentzVector vb( pStar*sStar,0, pStar*cStar,EbStar);
  TLorentzVector vc(-pStar*sStar,0,-pStar*cStar,EcStar);
  double phiStar = randGen.Uniform(0,twoPi);
  vb.RotateZ(phiStar);
  vc.RotateZ(phiStar);
  vb.Boost(TVector3(0,0,beta));
  vc.Boost(TVector3(0,0,beta));
  vb.RotateY(theta);
  vc.RotateY(theta);
  vb.RotateZ(phi);
  vc.RotateZ(phi);

  b = vb;
  c = vc;
  a.cosThetaStar = cStar;

  return true;
}



// Kinematic limits of momentum sharing, z.
//
pair<double,double> zMinMax(const TLorentzVector & a, double mb, double mc)
{
  double beta = a.Beta();
  double ma = a.M();
  if (mb+mc > ma)  return pair<double,double>(-1,-1);
  double EbStar = (ma*ma - mc*mc + mb*mb) / 2 / ma;
  double EcStar = (ma*ma - mb*mb + mc*mc) / 2 / ma;
  double betabStar = sqrt(1 - mb*mb/EbStar/EbStar);
  double betacStar = sqrt(1 - mc*mc/EcStar/EcStar);
  double yPlus  = (betacStar/betabStar) * fabs((beta+betabStar)/(beta-betacStar));
  double yMinus = (betacStar/betabStar) * fabs((beta-betabStar)/(beta+betacStar));
  double zPlus  = yPlus  / (1+yPlus );
  double zMinus = yMinus / (1+yMinus);
  double zMin = min(zPlus,zMinus);
  double zMax = max(zPlus,zMinus);
  return pair<double,double>(zMin,zMax);
}



// Compute scalar-sum of 3-momenta |pb|+|pc| for a splitting
//
double ComputePtot(double z, double Ea, double mb, double mc)
{
  double zBar = 1-z;
  double coeffA = square(z*z-zBar*zBar);
  double coeffB = -2*Ea*Ea*(z*z+zBar*zBar) + 2*(mb*mb-mc*mc)*(z*z-zBar*zBar);
  double coeffC = Ea*Ea*Ea*Ea + square(mb*mb-mc*mc) - 2*Ea*Ea*(mb*mb+mc*mc);
  if (z==zBar) { return sqrt(-coeffC/coeffB); }
  return sqrt( (-coeffB-sqrt(coeffB*coeffB-4*coeffA*coeffC)) / (2*coeffA) );
}



// Compute the opening angle as a function of splitting kinematics
//
double ComputeTheta(double z, double Q, double Ptot, double Eb, double Ec, double mb, double mc)
{
  ////return acos((mb*mb+mc*mc+2*Eb*Ec-Q*Q)/2/z/(1-z)/Ptot/Ptot);  // exact arc-cosine...more difficult to keep the splitting functions bounded by overestimator functions in some cases
  return sqrt(2 - (mb*mb+mc*mc+2*Eb*Ec-Q*Q)/z/(1-z)/Ptot/Ptot);  // small-angle approximation...tends to give somewhat smaller theta and conservative splitting function numerators;  the biggest possible theta returned is sqrt(2) instead of 3.141..., which works well e.g. if we used 1-theta^2/2 as an approximation for cos(theta) in a splitting
}



// Initialize global variables associated to the mother, for access by the splitting functions
//
void InitializeMotherVariables(Particle & a, Particle & r)
{
  _Ea = a.v.E();  _Q = a.GetQ();  _Q2 = _Q*_Q;  _ma = m(a.id);   // set global info about showering particle, for use by the splitting functions
  _Q2overPropagator = _Q2 / square(_Q2-_ma*_ma);  _Q4overPropagator = _Q2*_Q2overPropagator;
  _aVirtual = &a;  _aMother = a.mother;  _aSisterRecoiled = &r;  _aAunt = a.aunt;
}



// Account for the change in a particle's production probability when it goes off-shell to split, by recomputing its mother's differential splitting rate.
// This calculation requires a Jacobian factor, tied to our specific kinematic readjustment procedure when a daughter is set off-shell.
// Note that this is only computed one daughter splitting at a time, and does not precisely account for cases where a pair of daughters both go appreciably off-shell.
// 
// (Ideally, we could also introduce such ME & phase space corrections for particles splitting in the hard process. This would require access
//  to a complete event record, and generally the ME's used to make it. The phase space correction part would only require the kinematics.)
//
Particle bOnShellSplit,bOffShellSplit,cOnShellSplit,cOffShellSplit;
double ComputeProductionCorrection()
{
  if (_aMother==NULL || _aMother==0)  return 1;  // if couldn't find a mother, don't do anything!
  if (_aMother->f__a_to_b_c == NULL)  return 1;  // if mother process wasn't a splitting, don't do anything!

  bool corrected = false;
  double correction = 1;
  Particle *aVirtualSAVE = _aVirtual;  Particle *aSisterRecoiledSAVE = _aSisterRecoiled;
  int id = _aVirtual->id;
  double mNew = _aVirtual->M();  // kinematic mass, not virtuality (might be "off-shell" due to Breit-Wigner, instead of shower)

  bool cIsTheOffShell = false;  if (_aVirtual->is_c)  cIsTheOffShell = true; // default assumes that our aVirtual was the "b" particle in its production process, but set this flag to true if it was instead "c"
  double (*f__a_to_b_c)(double) = _aMother->f__a_to_b_c;
  bool a_to_V_c = _aMother->a_to_V_c;
  bool a_to_V_V = _aMother->a_to_V_V;

  double bHelicity,cHelicity;
  int bid,cid;
  if (!cIsTheOffShell) { // virtual "a" becomes "b" in its production
    bid = _aOnShell->id;  // this (and below) will be the original ID, e.g. before a wavefunction collapse to a mass eigenstate
    cid = _aSisterUnrecoiled->id;
    bHelicity = _aVirtual->helicity;
    cHelicity = _aSisterRecoiled->helicity;
    bOffShellSplit.NewBasicCopy(_aVirtual);         bOnShellSplit.NewBasicCopy(_aOnShell);
    cOffShellSplit.NewBasicCopy(_aSisterRecoiled);  cOnShellSplit.NewBasicCopy(_aSisterUnrecoiled);
  }
  else {  // virtual "a" becomes "c" in its production
    cid = _aOnShell->id;
    bid = _aSisterUnrecoiled->id;
    cHelicity = _aVirtual->helicity;
    bHelicity = _aSisterRecoiled->helicity;
    cOffShellSplit.NewBasicCopy(_aVirtual);         cOnShellSplit.NewBasicCopy(_aOnShell);
    bOffShellSplit.NewBasicCopy(_aSisterRecoiled);  bOnShellSplit.NewBasicCopy(_aSisterUnrecoiled);
  } 
  double zOffShellSplit = bOffShellSplit.P()/(bOffShellSplit.P()+cOffShellSplit.P());
  double zOnShellSplit  = bOnShellSplit .P()/(bOnShellSplit .P()+cOnShellSplit .P());

  // prepare global splitting variables associated to pre-split state (masses, virtualities)
  // *** BE CAREFUL:  from now on, global variables (beginning with "_") are now only associated to the parent splitting (use "SAVE" variables to access the current splitting)
  Particle * a = _aMother;  Particle * r = _aAunt;
  InitializeMotherVariables(*a,*r);
  
  // set off-shell daughter mass in splitting functions
  if (!cIsTheOffShell) { _mb = mNew;  _mc = m(cid); }
  else                 { _mc = mNew;  _mb = m(bid); }
  if (_mb+_mc >= _aVirtual->GetQ()) { InitializeMotherVariables(*aVirtualSAVE,*aSisterRecoiledSAVE);  return 0; }  // kinematically not allowed = zero probability
  SetKinematics(zOffShellSplit);  // to check if _th is good
  if (_th != _th) { InitializeMotherVariables(*aVirtualSAVE,*aSisterRecoiledSAVE);  return 0; } // also not kinematically allowed according to the cutoff for our angle computation formulas
  ////cout << _aVirtual->id << endl;
  ////cout << "GET OFF-SHELL PROBABILITY" << endl;
  double PsplitOffShell = f__a_to_b_c(zOffShellSplit);
  ////cout << "mb+mc = " << _mb+_mc << ", Qa = " << _aVirtual->GetQ() << endl;
  ////cout << "z = " << _z << ", Ptot = " << _Ptot << ", Eb = " << _Eb << ", Ec = " << _Ec << ", prefactor = " << _kinematicPrefactor << endl;
  ////cout << "th = " << _th << endl;
  if (a_to_V_c) { PsplitOffShell= (bHelicity==1)*_PR_eval + (bHelicity==0)*_Plong_eval + (bHelicity==-1)*_PL_eval; }
  if (a_to_V_V) { PsplitOffShell= (abs(bHelicity)==1 && abs(cHelicity)==1)*_PTT_eval + (abs(bHelicity)==1 && cHelicity==0)*_PTlong_eval + 
      (bHelicity==0 && abs(cHelicity)==1)*_PlongT_eval + (bHelicity==0 && cHelicity==0)*_Plonglong_eval; }
  
  // reset the splitting function daughter masses on-shell, and compute on-shell differential splitting rate
  _mb = m(bid);  _mc = m(cid);
  /////cout << "GET ON-SHELL PROBABILITY" << endl;
  /*
  SetKinematics(zOnShellSplit);//// needed here??? why might _th be bad?
  if (_th != _th) {
    cout << cIsTheOffShell << endl;
    cout << bid << " " << cid << endl;
    cout << mNew << endl;
    cout << "mb+mc = " << _mb << "+" << _mc << " = " << _mb+_mc << ", Qa = " << _aVirtual->GetQ() << endl;
    cout << "z = " << _z << ", Ptot = " << _Ptot << ", Eb = " << _Eb << ", Ec = " << _Ec << ", prefactor = " << _kinematicPrefactor << endl;
    cout << "th = " << _th << endl;
  }
  */
  double PsplitOnShell = f__a_to_b_c(zOnShellSplit);
  if (a_to_V_c) { PsplitOnShell= (bHelicity==1)*_PR_eval + (bHelicity==0)*_Plong_eval + (bHelicity==-1)*_PL_eval; }
  if (a_to_V_V) { PsplitOnShell= (abs(bHelicity)==1 && abs(cHelicity)==1)*_PTT_eval + (abs(bHelicity)==1 && cHelicity==0)*_PTlong_eval + 
      (bHelicity==0 && abs(cHelicity)==1)*_PlongT_eval + (bHelicity==0 && cHelicity==0)*_Plonglong_eval; }

  // total correction = Jacobian * (ratio of differential splitting functions)
  if (PsplitOffShell > 0 && PsplitOnShell > 0) {
    if (!cIsTheOffShell)  correction = Jacobian(*a,zOnShellSplit,zOffShellSplit,m(bid),mNew,m(cid),false)*PsplitOffShell/PsplitOnShell;
    else                  correction = Jacobian(*a,zOnShellSplit,zOffShellSplit,m(cid),mNew,m(bid),true )*PsplitOffShell/PsplitOnShell;
    /*
    //if ((_aOnShell->id==23 || _aOnShell->id==2223) && a->GetQ() > 900 && a->GetQ() < 1100) {
    if (abs(_aSisterUnrecoiled->id==24) && _aSisterUnrecoiled->helicity==0) {
      cout << eventCounter << "   compute correction for " << a->id << " -> " << id << " " << _aSisterUnrecoiled->id << "  @ Q = " << a->GetQ() << endl;
      cout << eventCounter << "   correction = " << correction*PsplitOnShell/PsplitOffShell << " * " << PsplitOffShell << "/" << PsplitOnShell << " = " << correction << endl;
      cout << eventCounter << "            z = " << zOnShellSplit << " -> " << zOffShellSplit << endl;
    }
    */
  }
  else  correction = 0;
  
  // final cleanup:  re-initialize global splitting variables
  InitializeMotherVariables(*aVirtualSAVE,*aSisterRecoiledSAVE);

  return correction;
}


//  Compute the Jacobian for the transformation of z from a->b(on-shell)c --> a->b(off-shell)c via the usual method of preserving the mother's rest-frame "decay" axis.  
//  This quantity returned is d[z(off-shell)]/d[z(on-shell)] under this transformation. (Q is left fixed.)
//  The computation by default assumes that the off-shell particle is "b", the particle that carries the momentum-sharing factor z, but we could just as well be setting "c" off-shell.
//  The last function parameter is a flag for when its the reverse case...pay attention!!
//
double Jacobian(const Particle & mother, double zOnShell, double zOffShell, double mOnShell, double mOffShell, double mSister, bool cIsTheOffShell)
{
  double ma = mother.v.M();
  double gamma = mother.v.E()/ma;
  double beta = mother.v.Beta();
  double cosThetaStar = mother.cosThetaStar;

  // "y" is a convenient intermediate variable defined as z/zBar, and y2 is the square of this.  compute its derivative wrt z.
  double dy2OffShell_dzOffShell = 2*zOffShell/TMath::Power(1-zOffShell,3.);
  double dy2OnShell_dzOnShell = 2*zOnShell/TMath::Power(1-zOnShell,3.);

  // "decay" into off-shell
  double mb = mOffShell;  
  double mc = mSister;
  if (cIsTheOffShell) { mb = mSister;  mc = mOffShell; }
  double EbStar = (ma*ma - mc*mc + mb*mb) / 2 / ma;
  double EcStar = (ma*ma - mb*mb + mc*mc) / 2 / ma;
  double pStar = sqrt( (ma*ma - (mb+mc)*(mb+mc)) * (ma*ma - (mb-mc)*(mb-mc)) ) / 2 / ma;
  double A    = beta*beta*pStar*pStar;
  double B    = 2*beta*  pStar *EbStar;
  double Bbar = 2*beta*(-pStar)*EcStar;
  double C    = pStar*pStar + beta*beta*mb*mb;
  double Cbar = pStar*pStar + beta*beta*mc*mc;
  double dy2OffShell_dcosThetaStar = TMath::Power(A*cosThetaStar*cosThetaStar + Bbar*cosThetaStar + Cbar, -2.) * 
    (A*(Bbar-B)*cosThetaStar*cosThetaStar + 2*A*(Cbar-C)*cosThetaStar + (B*Cbar-Bbar*C));

  // "decay" into on-shell
  if (!cIsTheOffShell)  mb = mOnShell;
  else                  mc = mOnShell;
  EbStar = (ma*ma - mc*mc + mb*mb) / 2 / ma;
  EcStar = (ma*ma - mb*mb + mc*mc) / 2 / ma;
  pStar = sqrt( (ma*ma - (mb+mc)*(mb+mc)) * (ma*ma - (mb-mc)*(mb-mc)) ) / 2 / ma;
  A = beta*beta*pStar*pStar;
  B = 2*beta*pStar*EbStar;
  Bbar = 2*beta*(-pStar)*EcStar;
  C = pStar*pStar+beta*beta*mb*mb;
  Cbar = pStar*pStar+beta*beta*mc*mc;  
  double dy2OnShell_dcosThetaStar = TMath::Power(A*cosThetaStar*cosThetaStar + Bbar*cosThetaStar + Cbar, -2.) * 
    (A*(Bbar-B)*cosThetaStar*cosThetaStar + 2*A*(Cbar-C)*cosThetaStar + (B*Cbar-Bbar*C));

  return (1./dy2OffShell_dzOffShell)*dy2OffShell_dcosThetaStar*(1./dy2OnShell_dcosThetaStar)*dy2OnShell_dzOnShell;
}



// Decay a polarized top quark t->Wb, accounting for W polarizations in the top's "production" frame
//
//    TO DO:
//       * Extend to fully coherent 3-body decay t->Wb->(ff')b?
// 
void DecayTop(Particle & top)
{
  if (!do_top_decay) return;

  TLorentzVector vtop = top.v;  double mThisTop = vtop.M();  double m2ThisTop = square(mThisTop);

  // get top boost vector and orientation
  double betaTop = vtop.BoostVector().Mag();  double thetaTop = vtop.Theta();  double phiTop = vtop.Phi();

  // daughter kinematics, accounting for spin effects
  double EWStar = (m2ThisTop + mW*mW - mbottom*mbottom)/(2*mThisTop);  double EbStar = mThisTop-EWStar;
  double pStar = sqrt(EbStar*EbStar - mbottom*mbottom);
  TLorentzVector vW(0,0,pStar,EWStar);  TLorentzVector vb(0,0,-pStar,EbStar);
  double analyzingPower = (m2ThisTop-2*mW*mW)/(m2ThisTop+2*mW*mW);
  double cStar = 1;
  while (1) {
    cStar = randGen.Uniform(-1,1);
    double relProb = (1+top.signid()*top.helicity*analyzingPower*cStar) / 2;
    if (randGen.Uniform() < relProb)  break;
  }
  double thetaStar = acos(cStar);
  double phiStar = randGen.Uniform(0,twoPi);
  vW.RotateY(thetaStar);  vW.RotateZ(phiStar);  vW.Boost(TVector3(0.,0.,betaTop));  double thetaW = fabs(vW.Theta());  vW.RotateY(thetaTop);  vW.RotateZ(phiTop);
  vb.RotateY(thetaStar);  vb.RotateZ(phiStar);  vb.Boost(TVector3(0.,0.,betaTop));  double thetab = fabs(vb.Theta());  vb.RotateY(thetaTop);  vb.RotateZ(phiTop);
 
  // determine relative rates of different W helicities using exact decay matrix elements (assuming massless bottom for simplicity)
  // relative W helicity probabilities computed here as for +6 top quark, but multiplied by -1 below if -6 antitop quark
  double Pplus=0; double Pminus=0; double Plong=0;
  if (top.signid()*top.helicity > 0) {
    double epsPlusDotSigmaBar11  = 1/sqrt2*(-sin(thetaW));         double epsPlusDotSigmaBar21  = 1/sqrt2*(cos(thetaW)+1);
    double epsMinusDotSigmaBar11 = 1/sqrt2*(-sin(thetaW));         double epsMinusDotSigmaBar21 = 1/sqrt2*(cos(thetaW)-1);
    double epsLongDotSigmaBar11  = 1/mW*(vW.P()+vW.E()*cos(thetaW));  double epsLongDotSigmaBar21  = 1/mW*(vW.E()*sin(thetaW));
    Pplus  = pow( sin(thetab/2)*epsPlusDotSigmaBar11  + cos(thetab/2)*epsPlusDotSigmaBar21 , 2.0);
    Pminus = pow( sin(thetab/2)*epsMinusDotSigmaBar11 + cos(thetab/2)*epsMinusDotSigmaBar21, 2.0);
    Plong  = pow( sin(thetab/2)*epsLongDotSigmaBar11  + cos(thetab/2)*epsLongDotSigmaBar21 , 2.0);
  }
  else {
    double epsPlusDotSigmaBar12  = 1/sqrt2*(cos(thetaW)-1);  double epsPlusDotSigmaBar22  = 1/sqrt2*(sin(thetaW));
    double epsMinusDotSigmaBar12 = 1/sqrt2*(cos(thetaW)+1);  double epsMinusDotSigmaBar22 = 1/sqrt2*(sin(thetaW));
    double epsLongDotSigmaBar12  = 1/mW*(vW.E()*sin(thetaW));   double epsLongDotSigmaBar22  = 1/mW*(vW.P()-vW.E()*cos(thetaW));
    Pplus  = pow( sin(thetab/2)*epsPlusDotSigmaBar12  + cos(thetab/2)*epsPlusDotSigmaBar22 , 2.0);
    Pminus = pow( sin(thetab/2)*epsMinusDotSigmaBar12 + cos(thetab/2)*epsMinusDotSigmaBar22, 2.0);
    Plong  = pow( sin(thetab/2)*epsLongDotSigmaBar12  + cos(thetab/2)*epsLongDotSigmaBar22 , 2.0);
  }
  double Whel = 0;
  double rnd = (Pplus+Pminus+Plong)*randGen.Uniform();
  if      (rnd < Pplus       )  Whel = 1;
  else if (rnd < Pplus+Pminus)  Whel = -1;
  else                          Whel =  0;

  Particle thisW,thisb;
  thisW.id = top.signid()*24;  thisW.v = vW;  thisW.helicity = Whel*top.signid();  thisW.mother = &top;
  thisb.id = top.signid()*5;   thisb.v = vb;  thisb.helicity = (-1)*top.signid();  thisb.mother = &top;
  top.daughters.clear();
  top.daughters.push_back(thisW);
  top.daughters.push_back(thisb);

  // ...maybe later run a (QCD) parton shower within the top decay? Just use W and b as recoilers of each other (careful of shower frame). 
  // W will not evolve other than kinematics. Then decay the W and shower it as well.

  Decay(thisW); 
}


// Decay a polarized W or Z boson (this gets fed from the DecayW and DecayZ routines)
// 
void DecayWZ(Particle & V)
{
  TLorentzVector vV = V.v;  double mThisV = vV.M();  double m2ThisV = square(mThisV);

  // get V boost vector and orientation
  double betaV = vV.BoostVector().Mag();  double thetaV = vV.Theta();  double phiV = vV.Phi();

  // throw random flavors (currently without QCD corrections, nor CKM for W decay)
  Particle f1,f2;
  int sgn = V.signid();
  if (abs(V.id)==24) {
    int flavor = randGen.Integer(Ndoublets);
    if      (flavor>=0  && flavor<  NC)   { f1.id = sgn*( 2);  f2.id = sgn*(- 1); }
    else if (flavor>=NC && flavor<2*NC)   { f1.id = sgn*( 4);  f2.id = sgn*(- 3); }
    else if (flavor==2*NC  )              { f1.id = sgn*(12);  f2.id = sgn*(-11); }    // if Ndoublets < 8, will preferentially try to split to quarks, then leptons
    else if (flavor==2*NC+1)              { f1.id = sgn*(14);  f2.id = sgn*(-13); }
    else if (flavor==2*NC+2)              { f1.id = sgn*(16);  f2.id = sgn*(-15); }
    else {
      cout << "WARNING:  TRIED TO DECAY W, BUT RAN OUT OF FLAVORS!!" << endl;
      return;   // simply don't decay if chooses nonexistant flavor beyond the canonical ones
    }
    f1.helicity = sgn*(-1);  f2.helicity = -f1.helicity;
    f1.mother = &V;  f2.mother = &V;
  }
  else if (V.id==23) {
    double Ptot = 0;
    double PuR = NC * square(QZ(2,+1));   Ptot += PuR;
    double PuL = NC * square(QZ(2,-1));   Ptot += PuL;
    double PcR = PuR;                     Ptot += PcR;
    double PcL = PuL;                     Ptot += PcL;
    double PdR = NC * square(QZ(1,+1));   Ptot += PdR;
    double PdL = NC * square(QZ(1,-1));   Ptot += PdL;
    double PsR = PdR;                     Ptot += PsR;
    double PsL = PdL;                     Ptot += PsL;
    double PeR = square(QZ(11,+1));       Ptot += PeR;
    double PeL = square(QZ(11,-1));       Ptot += PeL;
    double PmuR = PeR;                    Ptot += PmuR;
    double PmuL = PeL;                    Ptot += PmuL;
    double PtauR = PeR;                   Ptot += PtauR;
    double PtauL = PeL;                   Ptot += PtauL;
    double PveL = square(QZ(12,-1));      Ptot += PveL;
    double PvmuL = PveL;                  Ptot += PvmuL;
    double PvtauL = PveL;                 Ptot += PvtauL;
    double rndff = randGen.Uniform()*Ptot;  double PrunningSum = 0;
    if      ((PrunningSum += PuR   )  && rndff < PrunningSum)  { f1.id =  -2;  f1.helicity = -1; }  // will always choose f1 as the LH-helicity particle, to recycle routines for W decay
    else if ((PrunningSum += PuL   )  && rndff < PrunningSum)  { f1.id =   2;  f1.helicity = -1; }
    else if ((PrunningSum += PcR   )  && rndff < PrunningSum)  { f1.id =  -4;  f1.helicity = -1; }
    else if ((PrunningSum += PcL   )  && rndff < PrunningSum)  { f1.id =   4;  f1.helicity = -1; }
    else if ((PrunningSum += PdR   )  && rndff < PrunningSum)  { f1.id =  -1;  f1.helicity = -1; }
    else if ((PrunningSum += PdL   )  && rndff < PrunningSum)  { f1.id =   1;  f1.helicity = -1; }
    else if ((PrunningSum += PsR   )  && rndff < PrunningSum)  { f1.id =  -3;  f1.helicity = -1; }
    else if ((PrunningSum += PsL   )  && rndff < PrunningSum)  { f1.id =   3;  f1.helicity = -1; }
    else if ((PrunningSum += PeR   )  && rndff < PrunningSum)  { f1.id = -11;  f1.helicity = -1; }
    else if ((PrunningSum += PeL   )  && rndff < PrunningSum)  { f1.id =  11;  f1.helicity = -1; }
    else if ((PrunningSum += PmuR  )  && rndff < PrunningSum)  { f1.id = -13;  f1.helicity = -1; }
    else if ((PrunningSum += PmuL  )  && rndff < PrunningSum)  { f1.id =  13;  f1.helicity = -1; }
    else if ((PrunningSum += PtauR )  && rndff < PrunningSum)  { f1.id = -15;  f1.helicity = -1; }
    else if ((PrunningSum += PtauL )  && rndff < PrunningSum)  { f1.id =  15;  f1.helicity = -1; }
    else if ((PrunningSum += PveL  )  && rndff < PrunningSum)  { f1.id =  12;  f1.helicity = -1; }
    else if ((PrunningSum += PvmuL )  && rndff < PrunningSum)  { f1.id =  14;  f1.helicity = -1; }
    else if ((PrunningSum += PvtauL)  && rndff < PrunningSum)  { f1.id =  16;  f1.helicity = -1; }
    else {
      cout << "WARNING:  TRIED TO DECAY Z, BUT RAN OUT OF FLAVORS!!" << endl;
      return;  // simply don't decay if chooses nonexistant flavor beyond the canonical ones
    }
    f2.id = -f1.id;  f2.helicity = -f1.helicity;
  }
  else {
    cout << "WARNING:  TRIED TO DECAY PARTICLE ID " << V.id << " IN W/Z DECAY ROUTINE!!"  << endl;
    return;
  }

  // daughter kinematics (assuming everything is massless)
  double pStar = mThisV/2;
  TLorentzVector v1(0,0,pStar,pStar);  TLorentzVector v2(0,0,-pStar,pStar);
  double cStar = 1;
  while (1) {
    cStar = randGen.Uniform(-1,1);
    double relProb = (1-cStar*cStar);  // default longitudinal
    if (V.helicity== 1)  relProb = square((1-sgn*cStar)/2);
    if (V.helicity==-1)  relProb = square((1+sgn*cStar)/2);
    if (randGen.Uniform() < relProb)  break;
  }
  double thetaStar = acos(cStar);
  double phiStar = randGen.Uniform(0,twoPi);
  v1.RotateY(thetaStar);  v1.RotateZ(phiStar);  v1.Boost(TVector3(0.,0.,betaV));  v1.RotateY(thetaV);  v1.RotateZ(phiV);
  v2.RotateY(thetaStar);  v2.RotateZ(phiStar);  v2.Boost(TVector3(0.,0.,betaV));  v2.RotateY(thetaV);  v2.RotateZ(phiV);
  f1.v = v1;  f2.v = v2;

  Colorize(V,f1,f2);

  V.daughters.clear();
  V.daughters.push_back(f1);
  V.daughters.push_back(f2);

  // ...maybe later run a (QCD) parton shower within the decay?
}

// Decay a polarized W or Z boson
void DecayW(Particle & W)
{
  if (!do_W_decay) return;
  DecayWZ(W);
}

// Decay a polarized Z boson (* does not currently account for possible interference effects with photon exchange...could be added if we know the density matrix)
// 
void DecayZ(Particle & Z)
{
  if (!do_Z_decay) return;
  DecayWZ(Z);
}


//////////////////////////////////////////////////////////////////////////////////////////////
//
}  // close namespace weaklib


#endif
