//
//  Generate events for our weak showering machinery 
//
//
//   NOTE: I had to reset some hardcoded, overly-restrictive ranges for EW parameters, to facilitate direct comparison to MG. I only did this for the 8176 version of PYTHIA8. It is accomplished by modifying the xmldoc/StandardModelParamters.xml file (temporarily setting write permissions for the file). I modified the ranges of alphaEMmZ, 
//


//#include "Pythia8/Pythia.h"
#include "Pythia.h"
using namespace Pythia8; 

#include"weaklib.h"


string xmldir = "/Users/brock/pythia8176/xmldoc";   //////   SET TO YOUR PYTHIA8 DIRECTORY   




//------------------------------------------------------------------------
//
int main(int argc, char ** argv) {

  string runName = "";
  if (argc >= 2)   runName = argv[1]; 
  string outFileName = "data/output.pythia."+runName+".dat";
  if (runName=="")  outFileName = "data/output.pythia.dat";
  ofstream outFile(outFileName.c_str());


  int nGen = 1e5;

  // Generator
  Pythia pythia(xmldir);
  pythia.readString("PartonLevel:ISR = off"); 
  pythia.readString("PartonLevel:FSR = off"); 
  pythia.readString("PartonLevel:MPI = off"); 
  pythia.readString("HadronLevel:Hadronize = off"); 
  pythia.readString("Beams:idA = 2212");  // proton...
  pythia.readString("Beams:idB = 2212");  // ...on proton
  //pythia.readString("Beams:eCM = 100000");  // 100 TeV FCC
  pythia.readString("Beams:eCM =  14000");  // 14 TeV FCC
  //pythia.readString("PDF:pSet = 8");  // CTEQ6L1 PDF, same as MadGraph

  /*
  // Couplings and scales (set for consistency with MadGraph, validated on Vj production)
  pythia.readString("SigmaProcess:alphaSvalue = 0.13");  // same as MadGraph default at mZ (note that it gets auto-reset from param_card value in MG when using CTEQ6L1 PDF set...not obvious!)
  pythia.readString("SigmaProcess:alphaSorder = 0");  // fix at mZ to facilitate comparisons (divorced from scale-setting schemes)
  pythia.readString("SigmaProcess:alphaEMorder = -1");  // fix at mZ to facilitate comparisons (divorced from scale-setting schemes)
  pythia.readString("StandardModel:alphaEMmZ = 0.0075468");  // fix to MG5 default value 1/132.507 (this required modifying the allowed range in xmldoc/StandardModelParameters.xml)
  pythia.readString("StandardModel:sin2thetaW = 0.22225");  //  fix to MG5 default value, which is 1-mW^2/mZ^2  (ditto)
  pythia.readString("StandardModel:sin2thetaWbar = 0.22225");  //   ditto for version that appears in Zff vector couplings
  pythia.readString("24:m0 = 80.41846800");  // reset W mass to better match this sin2thetaW and MG, which might help enforce tree-level high-energy cancellations in longitudinal production (though don't trust in it!)
  pythia.readString("SigmaProcess:renormScale2 = 5");  // fix the renormalization scale
  pythia.readString("SigmaProcess:renormFixScale = 8315.3");  // renormalization scale Q^2 (in GeV^2), fixed to mZ^2
  pythia.readString("SigmaProcess:factorScale2 = 5");  // fix the factorization scale
  pythia.readString("SigmaProcess:factorFixScale = 9.e6");  // factorization scale Q^2 (in GeV^2)
  pythia.readString("SigmaProcess:Kfactor = 0.95");  // Literally a fudge factor, to account for lingering differences in MadGraph vs PYTHIA Vj rates at a few TeV. They agree at pT ~ mW, but drift away to 5-10% differences at 10's of TeV. These probably trace to some kind of residual non-cancellation in longitudinal production or generally some numerical instability in the hardcoded PYTHIA cross section formula (which is certainly a big issue for diboson production). Indeed, changing the W mass leads to large changes in the Wj cross section, even at pT >> mW. (I also thought perhaps PYTHIA is running sin2thetaW, even when I tell it to used fixed couplings, but I can't find anywhere in the source code where such running would be applied. The function sin2thetaW() just returns the constant s2tW, which gets set according to the user value.) Note that I get perfect agreement between PYTHIA and MadGraph in pure QCD and pure QED processes when using common fixed couplings, PDFs, and factorization scales. I have also verified the (rather nontrivial) MadGraph scaling behavior vs EW couplings (mZ,alphaEM,GF) for Wj processes.
  */

  // Hard event cuts
  pythia.readString("PhaseSpace:pTHatMin = 900");
  //pythia.readString("PhaseSpace:pTHatMin = 2950");
  //pythia.readString("PhaseSpace:pTHatMin = 20000");
  //pythia.readString("PhaseSpace:mHatMin =  19000");
  //pythia.readString("PhaseSpace:mHatMax = 10000");

  // W/Z/H properties
  pythia.readString("23:mayDecay = false");  // stable Z
  pythia.readString("24:mayDecay = false");  // stable W
  pythia.readString("25:mayDecay = false");  // stable Higgs
  pythia.readString("WeakZ0:gmZmode = 2");  // only Z contributions in mixed Z/gamma processes (**Careful with this for processes with Z exchange!!)

  // Splitting function switches
  weaklib::q_to_g_q          = 0;
  weaklib::g_to_g_g          = 0;
  weaklib::g_to_q_q          = 0;
  //
  weaklib::f_to_W_f          =    1;
  weaklib::f_to_gamZT_f      =    1;
  weaklib::f_to_Zlong_f      =    1;
  //
  weaklib::WT_to_gamZT_WT    =    0;
  weaklib::WT_to_Wlong_gamT  =    0;
  weaklib::WT_to_Wlong_ZT    =    0;
  weaklib::WT_to_Zlong_WT    =    0;
  weaklib::WT_to_f_f         =    0;
  weaklib::WT_to_t_b         =    0;

  // Diboson processes   
  //  **** Why do the high-pT cross sections for this look crazy compared to MadGraph (especially W+W-)?
  //       It's due to noncancellation in longitudinal production, unless the relation sin2thetaW == 1-mW^2/mZ^2 is enforced.
  //       Probably these cross sections cease to be trustworthy!
  //       It's not possible to see this directly since PYTHIA doesn't provide polarization information :-/
  //       (Interestingly, the program runs extremely slowly when dealing with this crazy cancellation.)
  //pythia.readString("WeakDoubleBoson:ffbar2gmZgmZ = on");  // ZZ (and gamma*)
  //pythia.readString("WeakDoubleBoson:ffbar2ZW = on");  // ZW
  //pythia.readString("WeakDoubleBoson:ffbar2WW = on");  // WW
  ////pythia.readString("WeakBosonAndParton:ffbar2Wgm = on");  // photon W
  //pythia.readString("PromptPhoton:ffbar2gammagamma = on");  // diphoton

  // V+jets processes
  /////pythia.readString("WeakBosonAndParton:qqbar2gmZg = on");  // qqbar -> Zg
  pythia.readString("PromptPhoton:qg2qgamma = off");  // qg -> q+photon
  pythia.readString("WeakBosonAndParton:qg2gmZq = off");  // qg -> Zq
  pythia.readString("WeakBosonAndParton:qqbar2Wg = off");  // qqbar -> Wg
  pythia.readString("WeakBosonAndParton:qg2Wq  = on");  // qg -> Wq

  // QCD processes
  pythia.readString("HardQCD:nQuarkNew=4");  // include charm as a light quark (for diagnostics against MadGraph default settings)
  ////pythia.readString("HardQCD:all = on");   // turns on all of the processes below
  //pythia.readString("HardQCD:gg2gg = on");
  //pythia.readString("HardQCD:qqbar2gg = on");
  //pythia.readString("HardQCD:gg2qqbar = on");
  //pythia.readString("HardQCD:qg2qg = on");
  //pythia.readString("HardQCD:qq2qq = on");

  // Initialize Pythia
  pythia.init();

 
  // Cross section, event counters
  double sigma = 0;
  int nEventsGenerated = 0;
  int nEventsPass = 0;
  int nEventsMultiboson = 0;
  int nEventsOneZ = 0;
  int nEventsOneGamma = 0;
  int nEventsBadShower = 0;

  // Allow for possibility of a few faulty events.
  int nAbort = 1000;
  int iAbort = 0;
  

  // Begin event loop; generate until none left in input file.     
  for (int iEvent = 0; ; ++iEvent) {

    if (iEvent >= nGen)  break;

    ///cout << iEvent << endl;/////
    //if (iEvent >= 10000)  break;

    // Generate events, and check whether generation failed.
    if (!pythia.next()) {
      
      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) break; 

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      cout << "TOO MANY ERRORS:  BAILING OUT!" << endl;
      break;
    }

    int nWZ = 0;
    int nW = 0;
    int nZ = 0;
    int nGamma = 0;

    nEventsGenerated++;
    nEventsPass++;

    Particle pythia_particle1;
    Particle pythia_particle2;
    
    // Extract particles from the hard event
    for (int i = 0; i < pythia.process.size(); ++i) { 

      Particle part = pythia.process[i];

      // pythia codes:  21: initial-state,  22: intermediate hard particle from user process,  23:  "final" hard particle from user process
      // "-" sign indicates particle has been decayed or annihilated

      if (part.status() == 23)  {
	if      (pythia_particle1.e()==0)  pythia_particle1 = part;
	else if (pythia_particle2.e()==0)  pythia_particle2 = part;
      }
    }

    // Check what category of event this is, for setting quark chiralities (Pythia doesn't provide these!)
    bool Wq=false;  bool qW=false;  //bool Zu=false;  bool Zd=false;  bool uZ=false;  bool dZ=false;
    bool gamZ_u=false;  bool gamZ_d=false;  bool u_gamZ=false;  bool d_gamZ=false;
    int absid1 = abs(pythia_particle1.id());  int absid2 = abs(pythia_particle2.id());
    int signid1 = pythia_particle1.id()/absid1;  int signid2 = pythia_particle2.id()/absid2;
    double gYuL = weaklib::gY * weaklib:: Y(2,-1);  double gYuR = weaklib::gY * weaklib:: Y(2,+1);
    double gYdL = weaklib::gY * weaklib:: Y(1,-1);  double gYdR = weaklib::gY * weaklib:: Y(1,+1);
    double gWuL = weaklib::gW * weaklib::T3(2,-1);  double gWuR = weaklib::gW * weaklib::T3(2,+1);
    double gWdL = weaklib::gW * weaklib::T3(1,-1);  double gWdR = weaklib::gW * weaklib::T3(1,+1);
    double RuL = (gYuL*gYuL + gWuL*gWuL) / (gYuL*gYuL + gWuL*gWuL + gYuR*gYuR + gWuR*gWuR);
    ////double RuL = weaklib::square(weaklib::QZ(2,-1)) / ( weaklib::square(weaklib::QZ(2,-1)) + weaklib::square(weaklib::QZ(2,+1)) );
    double RdL = (gYdL*gYdL + gWdL*gWdL) / (gYdL*gYdL + gWdL*gWdL + gYdR*gYdR + gWdR*gWdR);
    //double RdL = weaklib::square(weaklib::QZ(1,-1)) / ( weaklib::square(weaklib::QZ(1,-1)) + weaklib::square(weaklib::QZ(1,+1)) );
    if (absid1==24 && absid2<= 6)  Wq=true;
    if (absid1<= 6 && absid2==24)  qW=true;
    if ((absid1==22 || absid1==23) && absid2<=6 && absid2%2==0)  gamZ_u=true;
    if (absid1<=6 && absid1%2==0 && (absid2==22 || absid2==23))  u_gamZ=true;
    if ((absid1==22 || absid1==23) && absid2<=6 && absid2%2==1)  gamZ_d=true;
    if (absid1<=6 && absid1%2==1 && (absid2==22 || absid2==23))  d_gamZ=true;
    /*
    if (absid1==23 && absid2<=6 && absid2%2==0)  Zu=true;
    if (absid1<=6 && absid1%2==0 && absid2==23)  uZ=true;
    if (absid1==23 && absid2<=6 && absid2%2==1)  Zd=true;
    if (absid1<=6 && absid1%2==1 && absid2==23)  dZ=true;
    */

    // Translate to our shower particle convention
    //   ** vector helicities are randomized, which should hopefully be adequate. splitting rates within the gauge sector are approximately parity-symmetric, so shouldn't matter.
    weaklib::Particle particle1;
    particle1.v = TLorentzVector(pythia_particle1.px(),pythia_particle1.py(),pythia_particle1.pz(),pythia_particle1.e());
    particle1.id = pythia_particle1.id();
    particle1.helicity = weaklib::randPlusMinus1();  // nominally random helicity (and no longitudinals)
    double densityMatrix1_arr[4] = {0,0,0,0};  // default dummy density matrix
    vector<double> densityMatrix1(densityMatrix1_arr, densityMatrix1_arr + sizeof(densityMatrix1_arr) / sizeof(densityMatrix1_arr[0]));
    //
    weaklib::Particle particle2;
    particle2.v = TLorentzVector(pythia_particle2.px(),pythia_particle2.py(),pythia_particle2.pz(),pythia_particle2.e());
    particle2.id = pythia_particle2.id();
    particle2.helicity = weaklib::randPlusMinus1();  // nominally random helicity (and no longitudinals)
    double densityMatrix2_arr[4] = {0,0,0,0};  // default dummy density matrix
    vector<double> densityMatrix2(densityMatrix2_arr, densityMatrix2_arr + sizeof(densityMatrix2_arr) / sizeof(densityMatrix2_arr[0]));

    // For Vq events, set quark helicity and neutral vector density matrix accordingly
    if (qW)  particle1.helicity = (-1)*signid1;
    if (Wq)  particle2.helicity = (-1)*signid2;
    //
    if (u_gamZ) {
      if (weaklib::randGen.Uniform() < RuL) {
	particle1.helicity = (-1)*signid1;
	particle2.id = 2223;
	densityMatrix2[0] = gYuL*gYuL;  densityMatrix2[1] = gWuL*gWuL;  densityMatrix2[2] = gYuL*gWuL;
      }
      else {
	particle1.helicity = (+1)*signid1;
	particle2.id = 2223;
	densityMatrix2[0] = gYuR*gYuR;  densityMatrix2[1] = gWuR*gWuR;  densityMatrix2[2] = gYuR*gWuR;
      }
    }
    if (gamZ_u) {
      if (weaklib::randGen.Uniform() < RuL) {
	particle2.helicity = (-1)*signid2;
	particle1.id = 2223;
	densityMatrix1[0] = gYuL*gYuL;  densityMatrix1[1] = gWuL*gWuL;  densityMatrix1[2] = gYuL*gWuL;
      }
      else {
	particle2.helicity = (+1)*signid2;
	particle1.id = 2223;
	densityMatrix1[0] = gYuR*gYuR;  densityMatrix1[1] = gWuR*gWuR;  densityMatrix1[2] = gYuR*gWuR;
      }
    }
    //
    if (d_gamZ) {
      if (weaklib::randGen.Uniform() < RdL) {
	particle1.helicity = (-1)*signid1;
	particle2.id = 2223;
	densityMatrix2[0] = gYdL*gYdL;  densityMatrix2[1] = gWdL*gWdL;  densityMatrix2[2] = gYdL*gWdL;
      }
      else {
	particle1.helicity = (+1)*signid1;
	particle2.id = 2223;
	densityMatrix2[0] = gYdR*gYdR;  densityMatrix2[1] = gWdR*gWdR;  densityMatrix2[2] = gYdR*gWdR;
      }
    }
    if (gamZ_d) {
      if (weaklib::randGen.Uniform() < RdL) {
	particle2.helicity = (-1)*signid2;
	particle1.id = 2223;
	densityMatrix1[0] = gYdL*gYdL;  densityMatrix1[1] = gWdL*gWdL;  densityMatrix1[2] = gYdL*gWdL;
      }
      else {
	particle2.helicity = (+1)*signid2;
	particle1.id = 2223;
	densityMatrix1[0] = gYdR*gYdR;  densityMatrix1[1] = gWdR*gWdR;  densityMatrix1[2] = gYdR*gWdR;
      }
    }

    /*
    if (uZ) {
      if (weaklib::randGen.Uniform() < RuL)  particle1.helicity = (-1)*signid1;
      else                                   particle1.helicity = (+1)*signid1;
    }
    if (dZ) {
      if (weaklib::randGen.Uniform() < RdL)  particle1.helicity = (-1)*signid1;
      else                                   particle1.helicity = (+1)*signid1;
    }
    //
    if (Zu) {
      if (weaklib::randGen.Uniform() < RuL)  particle2.helicity = (-1)*signid2;
      else                                   particle2.helicity = (+1)*signid2;
    }
    if (Zd) {
      if (weaklib::randGen.Uniform() < RdL)  particle2.helicity = (-1)*signid2;
      else                                   particle2.helicity = (+1)*signid2;
    }
    */
    
    /*
    if(particle1.id==23) {  // if we see a Z-boson, assume it's transverse and set to showering gamma/Z state, artificially collapsed in Z direction
      particle1.id = 2223;
      densityMatrix1 = weaklib::rhoZT;
    }
    if(particle2.id==23) {  // if we see a Z-boson, assume it's transverse and set to showering gamma/Z state, artificially collapsed in Z direction
      particle2.id = 2223;
      densityMatrix2 = weaklib::rhoZT;
    }
    */

    // Set the density matrices
    particle1.densityMatrix = densityMatrix1;
    particle2.densityMatrix = densityMatrix2;

    // Reset masses of particles to correspond to their nominal ones in our shower program (especially may be important for "gamma/Z" derived from on-shell Z state)
    weaklib::ResetMasses(particle1,particle2,weaklib::m(particle1.id),weaklib::m(particle2.id));

    // Keep track of the unmodified initial partons, if needed
    weaklib::Particle original_particle1 = particle1;
    weaklib::Particle original_particle2 = particle2;
    
    // Initialize virtualities
    double Qmax = particle1.v.Pt();
    double tmax = log(Qmax*Qmax/weaklib::Qmin/weaklib::Qmin);
    particle1.t = tmax;
    if (particle2.id != 0)  particle2.t = tmax;  else  particle2.t = 0;

    ///cout << "BEGIN SHOWER" << endl;/////
    
    // Run the shower
    vector<weaklib::Particle*> finalParticles;
    bool done = 0;
    ////done = 1;/////
    while (!done) {
      particle1 = original_particle1;
      particle2 = original_particle2;
      if (particle1.id != 0)  particle1.t = tmax;  else  particle1.t = 0;
      if (particle2.id != 0)  particle2.t = tmax;  else  particle2.t = 0;
      ///cout << particle1.id << " " << particle2.id << endl;/////
      ///cout << particle1.t << " " << particle2.t << endl;/////
      done = weaklib::EvolvePair(particle1,particle2);
      if (!done) nEventsBadShower++; // cout << "BAD SHOWER:  EVENT #" << iEvent << "....RETRY" << endl;   ///// CONSIDER CHANGING THIS TO AN EVENT VETO
    }
    vector<weaklib::Particle*> primaries;
    primaries.push_back(&particle1);
    primaries.push_back(&particle2);
    weaklib::GatherFinalParticles(primaries,finalParticles);  // unpack final-state particles out of nested showering/decays
    
    ///cout << "END SHOWER" << endl;/////

    // Evaluate the final state
    for (int p = 0; p < finalParticles.size(); p++) {
      double ptmin = 0;
      if (abs(finalParticles[p]->id) == 24 && finalParticles[p]->v.Pt() > ptmin) nWZ++;
      if (    finalParticles[p]->id  == 23 && finalParticles[p]->v.Pt() > ptmin) nWZ++;

      if (abs(finalParticles[p]->id) == 24) nW++;
      if (    finalParticles[p]->id  == 23) nZ++;
      if (    finalParticles[p]->id  == 22) nGamma++;
    }

    //if (nWZ >= 2)  nEventsMultiboson++;
    if (nW >= 1 && nZ >= 1)  nEventsMultiboson++;
    if (nZ==1)  nEventsOneZ++;
    if (nGamma==1)  nEventsOneGamma++;

    // File output
    vector<weaklib::Particle> hardParticles;
    hardParticles.push_back(original_particle1);
    hardParticles.push_back(original_particle2);
    ////if (nW>=1 && nZ>=1) {   // exclusive multiboson
    if (nW==1 && nZ==0) {    // exclusive single-W
    ////if (1) {
      outFile << iEvent << " " << hardParticles.size() << endl;
      for (int p = 0; p < hardParticles.size(); p++) {
	outFile << "  " 
		<< hardParticles[p].id << "  " 
		<< hardParticles[p].v.Px() << " " 
		<< hardParticles[p].v.Py() << " " 
		<< hardParticles[p].v.Pz() << " " 
		<< hardParticles[p].v.E() << "  -2" << endl;
      }
      outFile << "0 0" << endl; // dummy...placeholder for b-hadron output
      outFile << "0 " << finalParticles.size() << endl;
      for (int p = 0; p < finalParticles.size(); p++) {
	outFile << "  " 
		<< finalParticles[p]->id << "  " 
		<< finalParticles[p]->v.Px() << " " 
		<< finalParticles[p]->v.Py() << " " 
		<< finalParticles[p]->v.Pz() << " " 
		<< finalParticles[p]->v.E() << "  "
		<< finalParticles[p]->helicity << endl;
      }
      outFile << endl;
    }
    


    //pythia.process.list();//////
    //pythia.event.list();//////// 

  // End of event loop.        
  }                                           

  sigma =  pythia.info.sigmaGen()*1e12;


  pythia.stat();

  cout << endl << "sigma = " << sigma << " fb" << endl << endl;
  cout << nEventsMultiboson << " multiboson events" << endl << endl;

  cout << endl;
  cout << "ration one-photon events to one-Z events (testing gamma/Z assignments/projections relative to PY native, only sensible with shower OFF): " << nEventsOneGamma*1./nEventsOneZ << endl;

  cout << endl;
  cout << nEventsBadShower*1./nGen << " experienced a bad/restarted shower" << endl;

  cout << endl;
  cout << "wrote " << outFileName << endl;

  // Done.                           
  return 0;
}
