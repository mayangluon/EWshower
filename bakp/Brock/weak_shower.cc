// weak_shower.cc    (BAT  spring 2014)
//
// A bare-bones weak shower simulator
//
//

#include"weaklib.h"

#include<iostream> // needed for io
#include<iomanip>
#include<fstream>  // file io
#include<sstream>  // needed for internal io
#include<vector> 


using namespace std;
using namespace weaklib;



/////////////////////////////////////////////////////////////////////
// GLOBAL PARAMETERS

int nGen = 2e5;

double E = 5000;  // in GeV (assumed "single-beam" energy that goes into making the two particles -> ~energy of each radiating particle)
double cosTheta = 0.;  // CM production angle
bool spin1resonance = false;///  // or over-ride fixed CM angle and distribute as for a spin-1 resonance decay ()

// particle #1
int   id1 = 2;     // pdg id
int   helicity1 = 0;  // for fermions -1,1; for weak bosons -1,0,1; other choices => randomized (gluon spins can be set, but are practically ignored)
double densityMatrix1_arr[4] = {0,1,0,0};  vector<double> densityMatrix1(densityMatrix1_arr, densityMatrix1_arr + sizeof(densityMatrix1_arr) / sizeof(densityMatrix1_arr[0]));  // neutral bosons only, mixed wavefunction in hypercharge/T3 gauge space or h0/phi0 complex scalar space
//double densityMatrix1_arr[4] = {square(gY*Y(11,-1)),square(gW*T3(11,-1)),gY*gW*Y(11,-1)*T3(11,-1),0};  vector<double> densityMatrix1(densityMatrix1_arr, densityMatrix1_arr + sizeof(densityMatrix1_arr) / sizeof(densityMatrix1_arr[0]));  // neutral bosons only, mixed wavefunction in hypercharge/T3 gauge space or h0/phi0 complex scalar space
//vector<double> densityMatrix1 = rhoPhoton;
//vector<double> densityMatrix1 = rhoZT;
//vector<double> densityMatrix1 = rhoH0star;
//vector<double> densityMatrix1 = rhoZlong;

// particle #2
int   id2 = 0;  // use "0" for dummy recoiler
int   helicity2 = 1;
double densityMatrix2_arr[4] = {0,0,0,0};  vector<double> densityMatrix2(densityMatrix2_arr, densityMatrix2_arr + sizeof(densityMatrix2_arr) / sizeof(densityMatrix2_arr[0]));



//////////////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS


//////////////////////////////////////////////////////////////////////////
// MAIN

int main (int argc, char ** argv)
{
  cout << "START (INITIALIZED ROOT)" << endl;

  // you can reset shower controls here if you like
  // dt *= 2;

  
  string runName = "";
  if (argc >= 2)   runName = argv[1]; 
  string outFileName = "data/output."+runName+".dat";
  if (runName=="")  outFileName = "data/output.dat";
  ofstream outFile(outFileName.c_str());
  outFile.precision(9);

  // counters, diagnostics, etc
  int nH = 0;
  int nZT = 0;
  int nZL = 0;
  int nWT = 0;
  int nWL = 0;
  int nPhoton = 0;
  int nTop = 0;
  int nBottom = 0;
  int n2W = 0;
  int n3W = 0;
  int nUnShowered = 0;
  
  // generate events
  for (int i = 0; i < nGen; i++) {
 

    int nH_thisEvent = 0;
    int nZT_thisEvent = 0;
    int nZL_thisEvent = 0;
    int nWT_thisEvent = 0;
    int nWL_thisEvent = 0;
    int nPhoton_thisEvent = 0;
    int nTop_thisEvent = 0;
    int nBottom_thisEvent = 0;
    int nLeptonNeutrino_thisEvent = 0;  // e/mu/tau or ve/vm/vt

    eventCounter++;
    if ((i+1)%1000 == 0) {
      //cout << endl;
      cout << "SIMULATE EVENT # " << i+1 << endl;
      //cout << endl;
    }

    Particle particle1;
    Particle particle2;
    
    // set ids
    particle1.id = id1;
    particle2.id = id2;
    
    // set spectators
    if (particle1.id == 0)  particle1.canShower = false;
    if (particle2.id == 0)  particle2.canShower = false;

    // set spins
    particle1.helicity = helicity1;
    if (abs(id1)<=6 && abs(helicity1)!=1)  particle1.helicity = randPlusMinus1();  // unpolarized quark
    if ((id1==21 || id1==22) && abs(helicity1)!=1)  particle1.helicity = randPlusMinus1();  // unpolarized gluon or photon
    if ((abs(id1)==24 || id1==23 || id1==2223) && abs(helicity1)>1)  particle1.helicity = randPlusMinus1();  // transverse-unpolarized gauge boson
    particle2.helicity = helicity2;
    if (abs(id2)<=6 && abs(helicity2)!=1)  particle2.helicity = randPlusMinus1();  // unpolarized quark
    if ((id2==21 || id2==22) && abs(helicity2)!=1)  particle2.helicity = randPlusMinus1();  // unpolarized gluon or photon
    if ((abs(id2)==24 || id2==23 || id2==2223) && abs(helicity2)>1)  particle2.helicity = randPlusMinus1();  // transverse-unpolarized W/Z

    // set density matrices
    particle1.densityMatrix = densityMatrix1;
    particle2.densityMatrix = densityMatrix2;

    // special: distribute angle as for a spin-1 resonance decay in its rest frame (e.g. for primitive W' studies)
    if (spin1resonance) {
       while (1) {
	 cosTheta = randGen.Uniform(-1,1);
	 double relProb = (1+cosTheta*cosTheta)/2;  // default longitudinal
	 if (randGen.Uniform() < relProb)  break;
       }
    }

    // set kinematics
    double m1 = m(id1);  double m2 = m(id2);
    double M = 2*E;  // invariant mass
    double E1 = (M*M-m2*m2+m1*m1) / (2*M);  double E2 = M-E1;
    double P = sqrt(M*M-(m1+m2)*(m1+m2)) * sqrt(M*M-(m1-m2)*(m1-m2)) / (2*M);
    double sinTheta = sqrt(1-cosTheta*cosTheta);    
    particle1.v = TLorentzVector( P*sinTheta,0, P*cosTheta,E1);
    particle2.v = TLorentzVector(-P*sinTheta,0,-P*cosTheta,E2);
    
    // check good density matrix for mixed states
    if ((id1==2223 || id1==2325) && Trace(densityMatrix1)<=0) { cout << "BAD NEUTRAL BOSON DENSITY MATRIX:  EVENT #" << i << endl;  continue; }
    if ((id2==2223 || id2==2325) && Trace(densityMatrix2)<=0) { cout << "BAD NEUTRAL BOSON DENSITY MATRIX:  EVENT #" << i << endl;  continue; }

    // keep track of the unmodified initial partons, if needed
    Particle original_particle1 = particle1;
    Particle original_particle2 = particle2;

    // run the shower
    vector<Particle*> finalParticles;
    bool done = 0;
    while (!done) {
      current_max_color = 101;
      particle1 = original_particle1;
      particle2 = original_particle2;
      double Qmax = particle1.v.Pt();
      ///Qmax = 100;//////////
      double tmax = log(Qmax*Qmax/Qmin/Qmin);
      if (particle1.id != 0)  particle1.t = tmax;  else  particle1.t = 0;
      if (particle2.id != 0)  particle2.t = tmax;  else  particle2.t = 0;
      done = EvolvePair(particle1,particle2);
      //if (!done) cout << "BAD SHOWER:  EVENT #" << i << "....RETRY" << endl;   ///// CONSIDER CHANGING THIS TO AN EVENT VETO
    }
    vector<Particle*> primaries;
    primaries.push_back(&particle1);
    primaries.push_back(&particle2);
    GatherFinalParticles(primaries,finalParticles);  // unpack final-state particles out of nested showering/decays

    /*
    TLorentzVector jet;
    for (int p = 0; p < finalParticles.size(); p++) {
      if (finalParticles[p]->id != 0)  jet += finalParticles[p]->v;
    }
    */
    //cout << "jet energy: " << jet.E() << endl;
    //cout << "jet mass  : " << jet.M() << endl;

    
    for (int p = 0; p < finalParticles.size(); p++) {
      int id = finalParticles[p]->id;
      int spin = finalParticles[p]->helicity;
      if (id == 25)  nH_thisEvent++;
      if (abs(id) == 24 && spin != 0)  nWT_thisEvent++;
      if (abs(id) == 24 && spin == 0)  nWL_thisEvent++;
      if (id == 23 && spin != 0) nZT_thisEvent++;
      if (id == 23 && spin == 0) nZL_thisEvent++;
      if (id == 22) nPhoton_thisEvent++;
      if (abs(id) == 6) nTop_thisEvent++;
      if (abs(id) == 5) nBottom_thisEvent++;
      if (abs(id) >= 11 && abs(id) <= 16) nLeptonNeutrino_thisEvent++;
    }
    nH += nH_thisEvent;
    nWT += nWT_thisEvent;
    nWL += nWL_thisEvent;
    nZT += nZT_thisEvent;
    nZL += nZL_thisEvent;
    nPhoton += nPhoton_thisEvent;
    nTop += nTop_thisEvent;
    nBottom += nBottom_thisEvent;

    if (finalParticles.size()==2)  nUnShowered++;

    vector<Particle> hardParticles;
    hardParticles.push_back(original_particle1);
    hardParticles.push_back(original_particle2);
    

    double nW_thisEvent = nWT_thisEvent+nWL_thisEvent;
    double nZ_thisEvent = nZT_thisEvent+nZL_thisEvent;

    //if (nW_thisEvent && nZ_thisEvent) {
    //if (nW_thisEvent) {
    //if (nH_thisEvent >= 2) {
    //if (nLeptonNeutrino_thisEvent) {
    //if (finalParticles.size() > 2) {
    if (1) {
  
      outFile << i << " " << hardParticles.size() << endl;
      for (int p = 0; p < hardParticles.size(); p++) {
	outFile << "  " 
		<< hardParticles[p].id << "  " 
		<< hardParticles[p].v.Px() << " " 
		<< hardParticles[p].v.Py() << " " 
		<< hardParticles[p].v.Pz() << " " 
		<< hardParticles[p].v.E() << "  " 
		<< hardParticles[p].helicity << endl;
      }
      outFile << "0 0" << endl; // dummy...placeholder for b-hadron output
      outFile << "0 " << finalParticles.size() << endl;
      for (int p = 0; p < finalParticles.size(); p++) {
	if (finalParticles[p]->id==6 && finalParticles[p]->v.M() > 200)  cout << eventCounter << "   FINAL TOP OFF-SHELL, M = " << finalParticles[p]->v.M() << ",  Q = " << finalParticles[p]->GetQ() << ",  branched = " << finalParticles[p]->branched << ",  # daughters = " << finalParticles[p]->daughters.size() << endl;//////
	outFile << "  "
		<< finalParticles[p]->id << "  " 
		<< finalParticles[p]->v.Px() << " " 
		<< finalParticles[p]->v.Py() << " " 
		<< finalParticles[p]->v.Pz() << " " 
		<< finalParticles[p]->v.E() << "  "
		<< finalParticles[p]->helicity << "  "
		<< finalParticles[p]->color << " " << finalParticles[p]->anticolor << "  "
		<< endl;
      }
      outFile << endl;
    }
    
    //////cout << endl << endl; //////////
  }

  cout << endl;
  cout << "made " << nH << "/" << nGen << " = " << nH*1./nGen << " Higgs" << endl;
  cout << "made " << nWT << "/" << nGen << " = " << nWT*1./nGen << " WT bosons" << endl;
  cout << "made " << nWL << "/" << nGen << " = " << nWL*1./nGen << " WL bosons" << endl;
  cout << "made " << nZT << "/" << nGen << " = " << nZT*1./nGen << " ZT bosons" << endl;
  cout << "made " << nZL << "/" << nGen << " = " << nZL*1./nGen << " ZL bosons" << endl;
  cout << endl;
  cout << "made " << nWT+nZT << "/" << nGen << " = " << (nWT+nZT)*1./nGen / weakCouplingRescaling << " WT+ZT bosons" << endl;
  cout << "made " << nWL+nZL << "/" << nGen << " = " << (nWL+nZL)*1./nGen / weakCouplingRescaling << " WL+ZL bosons" << endl;
  cout << endl;
  cout << "made " << nPhoton << "/" << nGen << " = " << nPhoton*1./nGen << " photons" << endl;
  cout << "made " << nTop << "/" << nGen << " = " << nTop*1./nGen << " top quarks" << endl;
  cout << "made " << nBottom << "/" << nGen << " = " << nBottom*1./nGen << " bottom quarks" << endl;
  cout << endl;
  cout << "fraction of events     showered = " << 1. - nUnShowered*1./nGen << endl;
  cout << "fraction of events not showered = " << nUnShowered*1./nGen << endl;
  cout << endl;
  cout << "wrote file: " << outFileName << endl;
  
}







///////////////////////////////////////////////////////////////////////////////////
// ADDITIONAL FUNCTION DEFINITIONS

