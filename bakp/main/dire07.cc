//==========================================================================
// PT distribution for pp-> ttbar at LHC
// Yang Ma at PITT PACC 6/21/2018
//==========================================================================


// DIRE includes.
#include "Dire/Dire.h"

// Pythia includes.
#include "Pythia8/Pythia.h"

using namespace Pythia8;

//==========================================================================

int main( int argc, char* argv[]  ){

  // Check that correct number of command-line arguments
  if (argc != 1) {
    cerr << " Unexpected number of command-line arguments ("<<argc-1<<"). \n"
         << " This example program uses no arguments, but "
         << argc-1 << " arguments provided:";
         for ( int i=1; i<argc; ++i) cerr << " " << argv[i];
         cerr << "\n Program stopped. " << endl;
    return 1;
  }


//==========================================================================
  Pythia pythia;

  // Create and initialize DIRE shower plugin.
  Dire dire;
  dire.init(pythia, "ppttb.cmnd");

  double wmax =-1e15;
  double wmin = 1e15;
  double sumwt = 0.;
  double sumwtsq = 0.;

  // Hist PT("top transverse momentum", 100, 0., 400.);
  // Hist eta("top pseudorapidity", 100, -5, 5.);
  // Hist y("top truerapidity", 100, -5, 5.);
  // Hist mass("m(ttbar) [GeV]", 1000, 350., 2000.);

//==========================================================================
  // Start generation loop
  int nEvent = pythia.settings.mode("Main:numberOfEvents");
  for( int iEvent=0; iEvent<nEvent; ++iEvent ){

    // Generate next event
    if( !pythia.next() ) {
      if( pythia.info.atEndOfFile() )
        break;
      else continue;
    }
    
    pythia.event.list(); 

    // Get event weight(s).
    double evtweight         = pythia.info.weight();

    // Do not print zero-weight events.
    if ( evtweight == 0. ) continue;

    // Retrieve the shower weight.
    dire.weightsPtr->calcWeight(0.);
    dire.weightsPtr->reset();
    double wt = dire.weightsPtr->getShowerWeight();

    if (abs(wt) > 1e3) {
      cout << scientific << setprecision(8)
      << "Warning in DIRE main program dire06.cc: Large shower weight wt="
      << wt << endl;
      if (abs(wt) > 1e4) { 
        cout << "Warning in DIRE main program dire06.cc: Shower weight larger"
        << " than 10000. Discard event with rare shower weight fluctuation."
        << endl;
        evtweight = 0.;
      }
    }
    // Do not print zero-weight events.
    if ( evtweight == 0. ) continue;

    evtweight *= wt;

    wmin = min(wmin,wt);
    wmax = max(wmax,wt);
    sumwt += wt;
    sumwtsq+=pow2(wt);


    //  int iTop = 0;
    //  int iTopbar = 0;
    //  //particle loop
    //  for (int i = 0; i < pythia.event.size(); ++i){   
    //    if (pythia.event[i].id()==6) iTop= i;
    //    else if (pythia.event[i].id()==-6) iTopbar= i;
    // }

    //Test weak shower by counting W
    // int nw = 0;
    // int iw = 0;
    // //particle loop
    //  for (int i = 0; i < pythia.event.size(); ++i) {
    //    if (pythia.event[i].idAbs()==24 && pythia.event[i].isFinal()) {
    //      iw= i; nw++;
    //    }
	  // }

  // cout << "iw=" << iw << ", id=" << pythia.event[iw].id() << endl;
  // cout << "in=" << nw << endl;
  //cout << "iTop=" <<iTop << ", id=" << pythia.event[iTop].id() << endl;

	// PT.fill ( pythia.event[iTop].pT() );
	// eta.fill ( pythia.event[iTop].eta() );
	// y.fill ( pythia.event[iTop].y() );
	// mass.fill((pythia.event[iTop].p() + pythia.event[iTopbar].p()).mCalc());

 } // end loop over events to generate

  // cout << PT << eta << mass; 
  // cout << PT ; 
  // PT.table("pp_t_PT_on");
  // eta.table("pp_t_eta_on");
  // y.table("pp_t_y_on");
  // mass.table("pp_t_m_on");

  // print cross section, errors
  pythia.stat();

  cout << endl
       << "\t Minimal shower weight=" << wmin
       << "\n\t Maximal shower weight=" << wmax
       << "\n\t Mean shower weight=" << sumwt/double(nEvent)
       << "\n\t Variance of shower weight="
       << sqrt(1/double(nEvent)*(sumwtsq - pow(sumwt,2)/double(nEvent)))
       << endl << endl;

  // Done

  return 0;

}
