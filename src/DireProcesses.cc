
#include "Dire/DireProcesses.h"

namespace Pythia8 {

//--------------------------------------------------------------------------

void DireSigmaHelper::init(Info* infoPtrIn, Settings* settingsPtrIn,
  ParticleData* particleDataPtrIn, Rndm*,
  BeamParticle* beamAPtrIn, BeamParticle* beamBPtrIn,
  Couplings* couplingsPtrIn, SusyLesHouches*, DireWeightContainer* weightsPtrIn,
  DireHooks* hooksPtrIn) {

  infoPtr         = infoPtrIn;
  settingsPtr     = settingsPtrIn;
  particleDataPtr = particleDataPtrIn;
  beamAPtr        = beamAPtrIn;
  beamBPtr        = beamBPtrIn;
  couplingsPtr    = couplingsPtrIn;
  weightsPtr      = weightsPtrIn;
  hooksPtr        = hooksPtrIn;

  // Renormalization scale rescaling.
  renormMultFac = settingsPtrIn->parm("SigmaProcess:renormMultFac");
  // Factorization scale rescaling.
  factorMultFac = settingsPtrIn->parm("SigmaProcess:factorMultFac");
  usePDFalphas  = settingsPtrIn->flag("ShowerPDF:usePDFalphas");

  hasExternalHook = (hooksPtr != 0);
  canSetMUR       = hasExternalHook && hooksPtr->canSetRenScale();
  canSetMUF       = hasExternalHook && hooksPtr->canSetFacScale();
  canSetMUQ       = hasExternalHook && hooksPtr->canSetStartScale();

  isInit        = true;
}

//--------------------------------------------------------------------------

double DireSigmaHelper::muRsq( double x1, double x2, double sH, double tH,
  double uH, bool massless, double m1sq, double m2sq, double m3sq,
  double m4sq) {
  if (false) cout << x1*x2*sH*tH*uH*massless*m1sq*m2sq*m3sq*m4sq;
  double ret = (massless) ?  tH * uH / sH : (tH * uH - m3sq * m4sq) / sH;
  // Scale setting for dijet process different from default Pythia.
  ret = -1./ (1/sH + 1/tH + 1/uH) / 2;
  ret *= renormMultFac;

  if (canSetMUR) ret = hooksPtr->doGetRenScale(x1, x2, sH, tH, uH, massless,
    m1sq, m2sq, m3sq, m4sq);

  return ret;
}

//--------------------------------------------------------------------------

double DireSigmaHelper::muFsq( double x1, double x2, double sH, double tH,
  double uH, bool massless, double m1sq, double m2sq, double m3sq,
  double m4sq) {
  if (false) cout << x1*x2*sH*tH*uH*massless*m1sq*m2sq*m3sq*m4sq;
  double ret = (massless) ?  tH * uH / sH : (tH * uH - m3sq * m4sq) / sH;
  // Scale setting for dijet process different from default Pythia.
  ret = -1./ (1/sH + 1/tH + 1/uH) / 2; 
  ret *= factorMultFac;

  if (canSetMUF) ret = hooksPtr->doGetFacScale(x1, x2, sH, tH, uH, massless,
    m1sq, m2sq, m3sq, m4sq);

  return ret;
}

//--------------------------------------------------------------------------

double DireSigmaHelper::muQsq( double x1, double x2, double sH, double tH,
  double uH, bool massless, double m1sq, double m2sq, double m3sq,
  double m4sq) {
  if (false) cout << x1*x2*sH*tH*uH*massless*m1sq*m2sq*m3sq*m4sq;
  double ret = (massless) ?  tH * uH / sH : (tH * uH - m3sq * m4sq) / sH;
  // Scale setting for dijet process different from default Pythia.
  ret = -1./ (1/sH + 1/tH + 1/uH) / 2;

  if (canSetMUQ) ret = hooksPtr->doGetStartScale(x1, x2, sH, tH, uH, massless,
    m1sq, m2sq, m3sq, m4sq);

  return ret;
}

//==========================================================================
 
void DireSigma2gg2gg::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in, double runBW4in)
  {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2qqbar2qqbarNew::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2qqbar2gg::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2qq2qq::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2qg2qg::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2gg2qqbar::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2gg2QQbar::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigma2qqbar2QQbar::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = sigmaHelperPtr->muRsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  Q2FacSave = sigmaHelperPtr->muFsq( x1in, x2in, sH, tH, uH, masslessKin, 0., 0.,
    s3, s4);
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

//==========================================================================
 
void DireSigmaSigma2ff2fft_neutral_current::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = -tH;
  Q2FacSave = -tH;
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

// Initialize process.
void DireSigmaSigma2ff2fft_neutral_current::initProc() {

  // Store Z0 mass for propagator. Common coupling factor.
  gmZmode   = settingsPtr->mode("WeakZ0:gmZmode");
  mZ        = particleDataPtr->m0(23);
  mZS       = mZ*mZ;
  thetaWRat = 1. / (16. * couplingsPtr->sin2thetaW()
            * couplingsPtr->cos2thetaW());

}

// Evaluate d(sigmaHat)/d(tHat), part independent of incoming flavour.
void DireSigmaSigma2ff2fft_neutral_current::sigmaKin() {

  // Cross section part common for all incoming flavours.
  double sigma0 = (M_PI / sH2) * pow2(alpEM);

  // Kinematical functions for gamma-gamma, gamma-Z and Z-Z parts.
  sigmagmgm = sigma0 * 2. * (sH2 + uH2) / tH2;
  sigmagmZ  = sigma0 * 4. * thetaWRat * sH2 / (tH * (tH - mZS));
  sigmaZZ   = sigma0 * 2. * pow2(thetaWRat) * sH2 / pow2(tH - mZS);
  if (gmZmode == 1) {sigmagmZ = 0.; sigmaZZ = 0.;}
  if (gmZmode == 2) {sigmagmgm = 0.; sigmagmZ = 0.;}

}

// Evaluate d(sigmaHat)/d(tHat), including incoming flavour dependence.
double DireSigmaSigma2ff2fft_neutral_current::sigmaHat() {

  // Couplings for current flavour combination.
  int id1Abs = abs(id1);
  double  e1 = couplingsPtr->ef(id1Abs);
  double  v1 = couplingsPtr->vf(id1Abs);
  double  a1 = couplingsPtr->af(id1Abs);
  int id2Abs = abs(id2);
  double  e2 = couplingsPtr->ef(id2Abs);
  double  v2 = couplingsPtr->vf(id2Abs);
  double  a2 = couplingsPtr->af(id2Abs);

  // Distinguish same-sign and opposite-sign fermions.
  double epsi = (id1 * id2 > 0) ? 1. : -1.;

  // Flavour-dependent cross section.
  double sigma = sigmagmgm * pow2(e1 * e2)
    + sigmagmZ * e1 * e2 * (v1 * v2 * (1. + uH2 / sH2)
      + a1 * a2 * epsi * (1. - uH2 / sH2))
    + sigmaZZ * ((v1*v1 + a1*a1) * (v2*v2 + a2*a2) * (1. + uH2 / sH2)
      + 4. * v1 * a1 * v2 * a2 * epsi * (1. - uH2 / sH2));

  // Spin-state extra factor 2 per incoming neutrino.
  if (id1Abs == 12 || id1Abs == 14 || id1Abs == 16) sigma *= 2.;
  if (id2Abs == 12 || id2Abs == 14 || id2Abs == 16) sigma *= 2.;

  // Answer.
  return sigma;

}

// Select identity, colour and anticolour.
void DireSigmaSigma2ff2fft_neutral_current::setIdColAcol() {

  // Trivial flavours: out = in.
  id3 = id1;
  id4 = id2;
  // Change to heavy neutrino.
  if (particleDataPtr->isLepton(id1)) id3 = 900012;
  if (particleDataPtr->isLepton(id2)) id4 = 900012;

  setId( id1, id2, id3, id4);

  // Colour flow topologies. Swap when antiquarks.
  if (abs(id1) < 9 && abs(id2) < 9 && id1*id2 > 0)
                         setColAcol( 1, 0, 2, 0, 1, 0, 2, 0);
  else if (abs(id1) < 9 && abs(id2) < 9)
                         setColAcol( 1, 0, 0, 2, 1, 0, 0, 2);
  else if (abs(id1) < 9) setColAcol( 1, 0, 0, 0, 1, 0, 0, 0);
  else if (abs(id2) < 9) setColAcol( 0, 0, 1, 0, 0, 0, 1, 0);
  else                   setColAcol( 0, 0, 0, 0, 0, 0, 0, 0);
  if ( (abs(id1) < 9 && id1 < 0) || (abs(id1) > 10 && id2 < 0) )
    swapColAcol();

}


//==========================================================================
 
void DireSigmaSigma2ff2fft_charged_current::store2Kin( double x1in, double x2in, double sHin,
  double tHin, double m3in, double m4in, double runBW3in,
  double runBW4in) {

  // Store inputs.
  // Default ordering of particles 3 and 4.
  swapTU   = false;
  // Incoming parton momentum fractions.
  x1Save   = x1in;
  x2Save   = x2in;
  // Incoming masses and their squares.
  bool masslessKin = (id3Mass() == 0) && (id4Mass() == 0);
  if (masslessKin) {
    m3     = 0.;
    m4     = 0.;
  } else {
    m3     = m3in;
    m4     = m4in;
  }
  mSave[3] = m3;
  mSave[4] = m4;
  s3       = m3 * m3;
  s4       = m4 * m4;
  // Standard Mandelstam variables and their squares.
  sH       = sHin;
  tH       = tHin;
  uH       = (masslessKin) ? -(sH + tH) : s3 + s4 - (sH + tH);
  mH       = sqrt(sH);
  sH2      = sH * sH;
  tH2      = tH * tH;
  uH2      = uH * uH;
  // The nominal Breit-Wigner factors with running width.
  runBW3   = runBW3in;
  runBW4   = runBW4in;
  // Calculate squared transverse momentum.
  pT2 = (masslessKin) ?  tH * uH / sH : (tH * uH - s3 * s4) / sH;

  // Get scales and alphaS
  Q2RenSave = -tH;
  Q2FacSave = -tH;
  alpS  = sigmaHelperPtr->alpS(Q2RenSave);

  // Evaluate alpha_EM.
  alpEM = couplingsPtr->alphaEM(Q2RenSave);

}

// Initialize process.
void DireSigmaSigma2ff2fft_charged_current::initProc() {

  // Store W+- mass for propagator. Common coupling factor.
  mW        = particleDataPtr->m0(24);
  mWS       = mW*mW;
  thetaWRat = 1. / (4. * couplingsPtr->sin2thetaW());

}

// Evaluate d(sigmaHat)/d(tHat), part independent of incoming flavour.
void DireSigmaSigma2ff2fft_charged_current::sigmaKin() {

  // Cross section part common for all incoming flavours.
  sigma0 = (M_PI / sH2) * pow2(alpEM * thetaWRat)
    * 4. * sH2 / pow2(tH - mWS);

}

// Evaluate d(sigmaHat)/d(tHat), including incoming flavour dependence.
double DireSigmaSigma2ff2fft_charged_current::sigmaHat() {

  // Some flavour combinations not possible.
  int id1Abs = abs(id1);
  int id2Abs = abs(id2);
  if ( (id1Abs%2 == id2Abs%2 && id1 * id2 > 0)
    || (id1Abs%2 != id2Abs%2 && id1 * id2 < 0) ) return 0.;

  // Basic cross section.
  double sigma = sigma0;
  if (id1 * id2 < 0) sigma *= uH2 / sH2;

  // CKM factors for final states.
  sigma *= couplingsPtr->V2CKMsum(id1Abs) *  couplingsPtr->V2CKMsum(id2Abs);

  // Spin-state extra factor 2 per incoming neutrino.
  if (id1Abs == 12 || id1Abs == 14 || id1Abs == 16) sigma *= 2.;
  if (id2Abs == 12 || id2Abs == 14 || id2Abs == 16) sigma *= 2.;

  // Answer.
  return sigma;

}

// Select identity, colour and anticolour.
void DireSigmaSigma2ff2fft_charged_current::setIdColAcol() {

  // Pick out-flavours by relative CKM weights.
  id3 = couplingsPtr->V2CKMpick(id1);
  id4 = couplingsPtr->V2CKMpick(id2);
  setId( id1, id2, id3, id4);

  // Colour flow topologies. Swap when antiquarks.
  if      (abs(id1) < 9 && abs(id2) < 9 && id1*id2 > 0)
                         setColAcol( 1, 0, 2, 0, 1, 0, 2, 0);
  else if (abs(id1) < 9 && abs(id2) < 9)
                         setColAcol( 1, 0, 0, 2, 1, 0, 0, 2);
  else if (abs(id1) < 9) setColAcol( 1, 0, 0, 0, 1, 0, 0, 0);
  else if (abs(id2) < 9) setColAcol( 0, 0, 1, 0, 0, 0, 1, 0);
  else                   setColAcol( 0, 0, 0, 0, 0, 0, 0, 0);
  if ( (abs(id1) < 9 && id1 < 0) || (abs(id1) > 10 && id2 < 0) )
    swapColAcol();

}

}
