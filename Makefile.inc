# DIRE configuration file.
# Generated on Fri Oct 19 00:00:57 EDT 2018 with the user supplied options:
# --prefix=~/work/dire
# --with-pythia8=~/work/pythia8235
# Configure string:
# ./configure --prefix=~/work/dire --with-pythia8=~/work/pythia8235

# Install directory prefixes.
PREFIX_BIN=/home/phy/work/dire/bin
PREFIX_INCLUDE=/home/phy/work/dire/include
PREFIX_LIB=/home/phy/work/dire/lib
PREFIX_SRC=/home/phy/work/EWshower/src
PREFIX_SHARE=/home/phy/work/dire/share/Dire

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=false
CXX=g++
CXX_COMMON=-O2 -std=c++98 -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname,
LIB_SUFFIX=.so

# PYTHIA8 configuration.
PYTHIA8_USE=true
PYTHIA8_BIN=/home/phy/work/pythia8235/bin/
PYTHIA8_INCLUDE=/home/phy/work/pythia8235/include
PYTHIA8_LIB=/home/phy/work/pythia8235/lib
PYTHIA8_PREFIX=/home/phy/work/pythia8235
PYTHIA8_SHARE=/home/phy/work/pythia8235/share
PYTHIA8_EXAMPLES=/home/phy/work/pythia8235/share/Pythia8/examples

# MG5MES configuration.
MG5MES_USE=false
MG5MES_BIN=
MG5MES_INCLUDE=./
MG5MES_LIB=./

# OPENMP configuration.
OPENMP_USE=false
OPENMP_BIN=
OPENMP_INCLUDE=./
OPENMP_LIB=./
